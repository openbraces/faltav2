<?php
   include("application-top.php");

   if(!isset($_SESSION["email"]))
   {
	header("location:index.php");
	exit();
   }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>FALTA SEZ - Admin</title>
    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> -->
    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.css" rel="stylesheet">
</head>

<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <?php include("includes/sidebar.php"); ?>
        <!-- End of Sidebar -->
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
                <?php include("includes/header.php"); ?>
                <!-- End of Topbar -->
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 font-weight-bold text-gray-700">Dashboard</h1>
                        <a href="add-gallery-image.php" class="d-none d-sm-inline-block btn btn-md btn-primary shadow-sm border-radius-1"><i class="fas fa-upload fa-md text-white-50"></i> Upload Image</a>
                    </div>
                    <!-- Header Cards -->
                    <div class="row">
                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-fsez shadow">
                                <div class="card-body custom-padding-4">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="h5 mb-0 font-weight-bold text-white">
                                              <?php echo total_circulars(); ?></div>
                                            <div class="text-md font-weight-bold text-primary text-gray-900 text-uppercase mb-1">
                                                Circulars</div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-file-pdf fa-2x text-blue-700"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-fsez shadow">
                                <div class="card-body custom-padding-4">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                          <div class="h5 mb-0 font-weight-bold text-white">
                                              <?php echo total_tenders(); ?></div>
                                          <div class="text-md font-weight-bold text-primary text-gray-900 text-uppercase mb-1">
                                                Tenders</div>

                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-file-invoice fa-2x text-blue-700"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-fsez shadow">
                                <div class="card-body custom-padding-4">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="h5 mb-0 font-weight-bold text-white">
                                              <?php echo total_forms(); ?></div>
                                            <div class="text-md font-weight-bold text-primary text-gray-900 text-uppercase mb-1">Forms
                                            </div>
                                            <div class="row no-gutters align-items-center">
                                                <div class="col-auto">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-file-excel fa-2x text-blue-700"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Pending Requests Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-fsez shadow">
                                <div class="card-body custom-padding-4">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="h5 mb-0 font-weight-bold text-white">
                                              <?php echo total_vacancies(); ?></div>
                                            <div class="text-md font-weight-bold text-primary text-gray-900 text-uppercase mb-1">
                                                Vacancies</div>

                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-clipboard-list fa-2x text-blue-700"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Header Cards -->

                    <!-- Start Body Elements -->
                    <div class="row">
                        <!-- Left Section -->
                        <div class="col-xl-12 col-lg-12">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">FSEZ Uploads Section</h6>
                                </div>
                                <div class="row">
                                  <div class="col-lg-4 mb-4">
                                      <!-- Card Body -->
                                      <div class="card-body inner-padding center-aligned">
                                        <a href="add-circular.php" class="btn btn-block btn-success btn-icon-split no-padding">
                                            <span class="icon text-white-85">
                                                <i class="fas fa-plus"></i>
                                            </span>
                                            <span class="text font-weight-normal text-white">Upload Circular</span>
                                        </a>
                                        <a href="add-tender.php" class="btn btn-block btn-success btn-icon-split no-padding">
                                            <span class="icon text-white-85">
                                                <i class="fas fa-plus"></i>
                                            </span>
                                            <span class="text font-weight-normal text-white">Upload Tender</span>
                                        </a>
                                        <a href="add-form.php" class="btn btn-block btn-success btn-icon-split no-padding">
                                            <span class="icon text-white-85">
                                                <i class="fas fa-plus"></i>
                                            </span>
                                            <span class="text font-weight-normal text-white">Upload Form</span>
                                        </a>
                                      </div>
                                    </div>
                                    <div class="col-lg-4 mb-4">
                                      <div class="card-body inner-padding center-aligned">
                                        <a href="add-authority-meeting.php" class="btn btn-block btn-success btn-icon-split no-padding">
                                            <span class="icon text-white-85">
                                                <i class="fas fa-plus"></i>
                                            </span>
                                            <span class="text font-weight-normal text-white">Upload Authority Minutes</span>
                                        </a>
                                        <a href="add-authority-agenda.php" class="btn btn-block btn-success btn-icon-split no-padding">
                                            <span class="icon text-white-85">
                                                <i class="fas fa-plus"></i>
                                            </span>
                                            <span class="text font-weight-normal text-white">Upload Authority Agenda</span>
                                        </a>
                                        <a href="add-vacancy.php" class="btn btn-block btn-success btn-icon-split no-padding">
                                            <span class="icon text-white-85">
                                                <i class="fas fa-plus"></i>
                                            </span>
                                            <span class="text font-weight-normal text-white">Upload Vacancy</span>
                                        </a>
                                      </div>
                                    </div>
                                    <div class="col-lg-4 mb-4">
                                        <div class="card-body inner-padding center-aligned">
                                          <a href="company-list.php" class="btn btn-block btn-success btn-icon-split no-padding">
                                              <span class="icon text-white-85">
                                                  <i class="fas fa-plus"></i>
                                              </span>
                                              <span class="text font-weight-normal text-white">Manage Units Detail</span>
                                          </a>
                                          <a href="add-rent-status.php" class="btn btn-block btn-success btn-icon-split no-padding">
                                              <span class="icon text-white-85">
                                                  <i class="fas fa-plus"></i>
                                              </span>
                                              <span class="text font-weight-normal text-white">Upload Quarterly Rent</span>
                                          </a>
                                          <a href="add-rti.php" class="btn btn-block btn-success btn-icon-split no-padding">
                                            <span class="icon text-white-85">
                                                <i class="fas fa-plus"></i>
                                            </span>
                                            <span class="text font-weight-normal text-white">Upload RTI Documents</span>
                                        </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Bottom Section -->
                        <div class="col-xl-3 col-lg-3">
                            <div class="card shadow mb-3">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">UAC Meeting Uploads</h6>
                                </div>
                                <!-- Card Body -->
                                <div class="card-body inner-padding-2">
                                  <a href="add-uac-agenda.php" class="btn btn-uac btn-block no-padding added-bottom-margin">
                                      <span class="icon text-white-85">
                                          <i class="fas fa-plus"></i>
                                      </span>
                                      <span class="text font-weight-normal text-white">Upload UAC Agenda</span>
                                  </a>
                                  <a href="add-uac-meeting.php" class="btn btn-uac btn-block no-padding added-bottom-margin">
                                      <span class="icon text-white-85">
                                          <i class="fas fa-plus"></i>
                                      </span>
                                      <span class="text font-weight-normal text-white">Upload UAC Minutes</span>
                                  </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-3 col-lg-3">
                            <div class="card shadow mb-3">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">EOU Meeting Uploads</h6>
                                </div>
                                <!-- Card Body -->
                                <div class="card-body inner-padding-2">
                                  <a href="add-eou-agenda.php" class="btn btn-eou btn-block no-padding added-bottom-margin">
                                      <span class="icon text-white-85">
                                          <i class="fas fa-plus"></i>
                                      </span>
                                      <span class="text font-weight-normal text-white">Upload EOU Agenda</span>
                                  </a>
                                  <a href="add-eou-meeting.php" class="btn btn-eou btn-block no-padding added-bottom-margin">
                                      <span class="icon text-white-85">
                                          <i class="fas fa-plus"></i>
                                      </span>
                                      <span class="text font-weight-normal text-white">Upload EOU Minutes</span>
                                  </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-3 col-lg-3">
                            <div class="card shadow mb-3">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Contact Management</h6>
                                </div>
                                <!-- Card Body -->
                                <div class="card-body inner-padding-2">
                                  <a href="designation-list.php" class="btn btn-cm btn-block no-padding added-bottom-margin">
                                      <span class="icon text-white-85">
                                          <i class="fas fa-plus"></i>
                                      </span>
                                      <span class="text font-weight-normal text-white">Add New Designation</span>
                                  </a>
                                  <a href="add-contact.php" class="btn btn-cm btn-block no-padding added-bottom-margin">
                                      <span class="icon text-white-85">
                                          <i class="fas fa-plus"></i>
                                      </span>
                                      <span class="text font-weight-normal text-white">Add New Contact</span>
                                  </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-3 col-lg-3">
                            <div class="card shadow mb-3">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Latest News Section</h6>
                                </div>
                                <!-- Card Body -->
                                <div class="card-body inner-padding-2">
                                  <a href="add-latest-news.php" class="btn btn-news btn-block no-padding added-bottom-margin">
                                      <span class="icon text-white-85">
                                          <i class="fas fa-plus"></i>
                                      </span>
                                      <span class="text font-weight-normal text-white">Add Latest News</span>
                                  </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- End Body Elements -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- End of Main Content -->
            <!-- Footer -->
            <?php include("includes/footer.php"); ?>
            <!-- End of Footer -->
        </div>
        <!-- End of Content Wrapper -->
    </div>
    <!-- End of Page Wrapper -->
    <?php include("includes/common-js.php"); ?>
</body>

</html>