 <!-- Common JS -->

 <script src="vendor/jquery/jquery.min.js"></script>
 <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
 <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
 <script src="js/jquery-ui.min.js"></script>
 <script src="js/sb-admin-2.min.js"></script>
 <link rel="stylesheet" href="css/jquery-ui.css">
 <script src="vendor/datatables/jquery.dataTables.min.js"></script>
 <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>
 <script src="js/demo/datatables-demo.js"></script>
 <script src="js/encryption.js"></script>
 <script src="js/crypto-js.min.js"></script>
 
 <!-- Select 2 library -->
 <link rel="stylesheet" href="css/select2.min.css">
 <script src="js/select2.min.js"></script>
 

 <script>
jQuery(document).ready(function($) {
    
    /* Load Date picker */
    
    if($("#date_of_issue").length > 0) {
        $("#date_of_issue").datepicker({
            dateFormat: 'dd-mm-yy',
            changeMonth: true,
            changeYear: true,
            maxDate: '+3D',
            yearRange: "-25:+0"
        });
    }
    
    /* Searchable Dropdown  */
    
    if($("#company").length > 0) {
        $("#company").select2();
    }
    
    

    $("#error").hide();

    $("#login-form").on('submit', function(e) {
        e.preventDefault();
        var email = $("#email").val();
        var password = $("#password").val();
        var captcha = $("#captcha").val();
        let encryption = new Encryption();
        var cipherValue=	$('#cryptoKey').val();

        if ($('#rememberMe').is(':checked') || $('#rememberMe').prop('checked')) {
            // do something if checked
            var rememberMe = $("#rememberMe").val();
        } else {
            // do something if unchecked
            var rememberMe = 0;
        }

           var dataStr = $('#login-form').serializeArray().reduce(function(obj, item) {
                if (item.name != "cryptoKey" && item.name != "rememberMe" && item.name != "captcha")
                {
                    obj[item.name] = encryption.encrypt(item.value,cipherValue);
                } else {
                  obj[item.name] =item.value;
                }
                return obj;
            }, {});

        if (email == "" || password == "") {
            alert(dataStr);
        } else {
            $.ajax({
                type: "POST",
                url: "verify-login.php",
                data: dataStr,
                cache: false,
                success: function(response) {
                    if (response == 1) {
                        window.location = "dashboard.php";
                    } else {
                        $("#error").show().fadeOut(10000).html(response);
                        $("form").trigger("reset");
                    }
                }
            });

        }
    });
})

/* Custom File */

$(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

/* File Type Validation */

function validateFile(id) {
    var formData = new FormData();

    var cfile = document.getElementById(id).files[0];

    formData.append("Filedata", cfile);
    let extn = cfile.type.split('/').pop().toLowerCase();
    if (extn != "jpeg" && extn != "jpg" && extn != "png" && extn != "pdf" && extn != "msword" && extn !=
        "vnd.openxmlformats-officedocument.wordprocessingml.document" && extn !=
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" && extn != "application/vnd.ms-excel") {
        $("#pdfModal").modal('show');
        document.getElementById(id).value = '';
        return false;
    }
    return true;
}

/* Image Type Validation */

function validateImage(id) {
    var formData = new FormData();

    var ufile = document.getElementById(id).files[0];

    formData.append("Filedata", ufile);
    let extn = ufile.type.split('/').pop().toLowerCase();
    if (extn != "jpeg" && extn != "jpg" && extn != "png" && extn != "bmp" && extn != "gif") {
        $("#imageModal").modal('show');
        document.getElementById(id).value = '';
        return false;
    }
    return true;
}

function validateUrl(url) {
// Below regular expression can validate input URL with or without http:// etc
var pattern = new RegExp("^((http|https)\://)*([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&%\$#\=~_\-]+))*$");
return pattern.test(url);
}

/* Scroll to Top */
function pageScrollTop() {
    jQuery("html, body").animate({
        scrollTop: 0,
    });
}

  //It restrict the non-numbers
	var specialKeys = new Array();
	specialKeys.push(8,46); //Backspace
	function IsNumeric(e) {
		var keyCode = e.which ? e.which : e.keyCode;
		console.log( keyCode );
		var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
		return ret;
	}


jQuery(document).ready(function($) {

    $.ajaxSetup({
        data: window.csrf
    });

    if($("#contact_no").length > 0) {
      $("#contact_no").attr('maxlength','14');
    } else {
     $("input[type=text]").attr('maxlength','200');
    }

    /* Circular Upload */

    $("#cSuccess").hide();

    $("#circularForm").on("submit", function(e) {
        e.preventDefault();

        var circular_name = $("#circular_name").val();
        var date_of_issue = $("#date_of_issue").val();

        var formData = new FormData($('#circularForm')[0]);
        formData.append("circular_file_name", $('input[type=file]')[0].files[0]);
        formData.append("circular_name", circular_name);
        formData.append("date_of_issue", date_of_issue);

        $.ajax({
            type: 'POST',
            url: 'ajax/add-new-circular.php',
            data: formData,
            dataType: "html",
            cache: false,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response == 0) {
                    circularForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    $("#cSuccess").append("Circular uploaded Succesfully...").show();
                    setTimeout(function() {
                        location.href = "circular-list.php"
                    }, 3000);
                }  else if (response == 3) {
                    circularForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    alert("Invalid file type, please choose another file.");
                } else {
                    circularForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    alert("Something went wrong");
                }
            }
        });
    });

    /* Tender Upload */

    $("#tSuccess").hide();

    $("#tenderForm").on("submit", function(e) {
        e.preventDefault();

        var tender_name = $("#tender_name").val();
        var date_of_issue = $("#date_of_issue").val();

        var formData = new FormData($('#tenderForm')[0]);
        formData.append("tender_file_name", $('input[type=file]')[0].files[0]);
        formData.append("tender_name", tender_name);
        formData.append("date_of_issue", date_of_issue);

        $.ajax({
            type: 'POST',
            url: 'ajax/add-new-tender.php',
            data: formData,
            dataType: "html",
            cache: false,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response == 0) {
                    tenderForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    $("#tSuccess").append("Tender uploaded Succesfully...").show();
                    setTimeout(function() {
                        location.href = "tender-list.php"
                    }, 3000);
                }  else if (response == 3) {
                    tenderForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    alert("Invalid file type, please choose another file.");
                } else {
                    tenderForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    alert("Something went wrong");
                }
            }
        });
    });

    /* Form Upload */

    $("#fSuccess").hide();

    $("#uploadForm").on("submit", function(e) {
        e.preventDefault();

        var form_name = $("#form_name").val();
        var date_of_issue = $("#date_of_issue").val();

        var formData = new FormData($('#uploadForm')[0]);
        formData.append("form_file_name", $('input[type=file]')[0].files[0]);
        formData.append("form_name", form_name);
        formData.append("date_of_issue", date_of_issue);

        $.ajax({
            type: 'POST',
            url: 'ajax/add-new-form.php',
            data: formData,
            dataType: "html",
            cache: false,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response == 0) {
                    uploadForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    $("#fSuccess").append("Form uploaded Succesfully...").show();
                    setTimeout(function() {
                        location.href = "form-list.php"
                    }, 3000);
                } else if (response == 3) {
                    uploadForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    alert("Invalid file type, please choose another file.");
                } else {
                    uploadForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    alert("Something went wrong");
                }
            }
        });

    });

    /* Vacancy Upload */

    $("#vSuccess").hide();

    $("#vacancyForm").on("submit", function(e) {
        e.preventDefault();

        var vacancy_name = $("#vacancy_name").val();
        var date_of_issue = $("#date_of_issue").val();

        var formData = new FormData($('#vacancyForm')[0]);
        formData.append("vacancy_file_name", $('input[type=file]')[0].files[0]);
        formData.append("vacancy_name", vacancy_name);
        formData.append("date_of_issue", date_of_issue);

        $.ajax({
            type: 'POST',
            url: 'ajax/add-new-vacancy.php',
            data: formData,
            dataType: "html",
            cache: false,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response == 0) {
                    vacancyForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    $("#vSuccess").append("Vacancy added Succesfully...").show();
                    setTimeout(function() {
                        location.href = "vacancy-list.php"
                    }, 3000);
                }  else if (response == 3) {
                    vacancyForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    alert("Invalid file type, please choose another file.");
                } else {
                    vacancyForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    alert("Something went wrong");
                }
            }
        });

    });

    /* UAC Meeting Upload */

    $("#uSuccess").hide();

    $("#uacForm").on("submit", function(e) {
        e.preventDefault();

        var uac_meeting_title = $("#uac_meeting_title").val();
        var date_of_issue = $("#date_of_issue").val();

        var formData = new FormData($('#uacForm')[0]);
        formData.append("uac_meeting_file", $('input[type=file]')[0].files[0]);
        formData.append("uac_meeting_title", uac_meeting_title);
        formData.append("date_of_issue", date_of_issue);

        $.ajax({
            type: 'POST',
            url: 'ajax/add-new-uac.php',
            data: formData,
            dataType: "html",
            cache: false,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response == 0) {
                    uacForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    $("#uSuccess").append("UAC meeting added Succesfully...").show();
                    setTimeout(function() {
                        location.href = "uac-meeting-list.php"
                    }, 3000);
                } else if (response == 3) {
                    uacForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    alert("Invalid file type, please choose another file.");
                } else {
                    uacForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    alert("Something went wrong");
                }
            }
        });

    });


    /* UAC Agenda Upload */

    $("#agSuccess").hide();

    $("#uacAgForm").on("submit", function(e) {
        e.preventDefault();

        var uac_agenda_title = $("#uac_agenda_title").val();
        var date_of_issue = $("#date_of_issue").val();

        var formData = new FormData($('#uacAgForm')[0]);
        formData.append("uac_agenda_file", $('input[type=file]')[0].files[0]);
        formData.append("uac_agenda_title", uac_agenda_title);
        formData.append("date_of_issue", date_of_issue);

        $.ajax({
            type: 'POST',
            url: 'ajax/add-new-agenda.php',
            data: formData,
            dataType: "html",
            cache: false,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response == 0) {
                    uacAgForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    $("#agSuccess").append("UAC agenda added Succesfully...").show();
                    setTimeout(function() {
                        location.href = "uac-agenda-list.php"
                    }, 3000);
                } else if (response == 3) {
                    uacAgForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    alert("Invalid file type, please choose another file.");
                } else {
                    uacAgForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    alert("Something went wrong");
                }
            }
        });

    });

    /* EOU Agenda Upload */

    $("#egSuccess").hide();

    $("#eouAgForm").on("submit", function(e) {
        e.preventDefault();

        var eou_agenda_title = $("#eou_agenda_title").val();
        var date_of_issue = $("#date_of_issue").val();

        var formData = new FormData($('#eouAgForm')[0]);
        formData.append("eou_agenda_file", $('input[type=file]')[0].files[0]);
        formData.append("eou_agenda_title", eou_agenda_title);
        formData.append("date_of_issue", date_of_issue);

        $.ajax({
            type: 'POST',
            url: 'ajax/add-new-eou-agenda.php',
            data: formData,
            dataType: "html",
            cache: false,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response == 0) {
                    eouAgForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    $("#egSuccess").append("EOU agenda added Succesfully...").show();
                    setTimeout(function() {
                        location.href = "eou-agenda-list.php"
                    }, 3000);
                } else if (response == 3) {
                    eouAgForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    alert("Invalid file type, please choose another file.");
                } else {
                    eouAgForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    alert("Something went wrong");
                }
            }
        });

    });

        /* UAC Meeting Upload */

    $("#eMSuccess").hide();

    $("#eouForm").on("submit", function(e) {
        e.preventDefault();

        var eou_meeting_title = $("#eou_meeting_title").val();
        var date_of_issue = $("#date_of_issue").val();

        var formData = new FormData($('#eouForm')[0]);
        formData.append("eou_meeting_file", $('input[type=file]')[0].files[0]);
        formData.append("eou_meeting_title", eou_meeting_title);
        formData.append("date_of_issue", date_of_issue);

        $.ajax({
            type: 'POST',
            url: 'ajax/add-new-eou.php',
            data: formData,
            dataType: "html",
            cache: false,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response == 0) {
                    eouForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    $("#eMSuccess").append("EOU meeting added Succesfully...").show();
                    setTimeout(function() {
                        location.href = "eou-meeting-list.php"
                    }, 3000);
                } else if (response == 3) {
                    eouForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    alert("Invalid file type, please choose another file.");
                } else {
                    eouForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    alert("Something went wrong");
                }
            }
        });

    });


    /* Authority Meeting Upload */

    $("#aSuccess").hide();

    $("#authForm").on("submit", function(e) {
        e.preventDefault();

        var auth_meeting_title = $("#auth_meeting_title").val();
        var date_of_issue = $("#date_of_issue").val();

        var formData = new FormData($('#authForm')[0]);
        formData.append("auth_meeting_file", $('input[type=file]')[0].files[0]);
        formData.append("auth_meeting_title", auth_meeting_title);
        formData.append("date_of_issue", date_of_issue);

        $.ajax({
            type: 'POST',
            url: 'ajax/add-new-auth.php',
            data: formData,
            dataType: "html",
            cache: false,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response == 0) {
                    authForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    $("#aSuccess").append("Authority meeting added Succesfully...").show();
                    setTimeout(function() {
                        location.href = "authority-meeting-list.php"
                    }, 3000);
                } else if (response == 3) {
                    authForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    alert("Invalid file type, please choose another file.");
                }  else {
                    authForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    alert("Something went wrong");
                }
            }
        });

    });

     /* Authority Agenda Upload */

     $("#aGSuccess").hide();

    $("#authAgForm").on("submit", function(e) {
        e.preventDefault();

        var auth_agenda_title = $("#auth_agenda_title").val();
        var date_of_issue = $("#date_of_issue").val();

        var formData = new FormData($('#authAgForm')[0]);
        formData.append("auth_agenda_file", $('input[type=file]')[0].files[0]);
        formData.append("auth_agenda_title", auth_agenda_title);
        formData.append("date_of_issue", date_of_issue);

        $.ajax({
            type: 'POST',
            url: 'ajax/add-new-auth-agenda.php',
            data: formData,
            dataType: "html",
            cache: false,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response == 0) {
                    authAgForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    $("#aGSuccess").append("Authority agenda added Succesfully...").show();
                    setTimeout(function() {
                        location.href = "authority-agenda-list.php"
                    }, 3000);
                } else if (response == 3) {
                    authAgForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    alert("Invalid file type, please choose another file.");
                }  else {
                    authAgForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    alert("Something went wrong");
                }
            }
        });

    });


    /* RTI Upload */

    $("#rSuccess").hide();

    $("#rtiForm").on("submit", function(e) {
        e.preventDefault();

        var rti_title = $("#rti_title").val();

        var formData = new FormData($('#rtiForm')[0]);
        formData.append("rti_file_name", $('input[type=file]')[0].files[0]);
        formData.append("rti_title", rti_title);

        $.ajax({
            type: 'POST',
            url: 'ajax/add-new-rti.php',
            data: formData,
            dataType: "html",
            cache: false,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response == 0) {
                    rtiForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    $("#rSuccess").append("RTI added Succesfully...").show();
                    setTimeout(function() {
                        location.href = "rti-list.php"
                    }, 3000);
                } else if (response == 3) {
                    rtiForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    alert("Invalid file type, please choose another file.");
                } else {
                    rtiForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    alert("Something went wrong");
                }
            }
        });

    });

    /* Export Upload */

    $("#eSuccess").hide();

    $("#exportForm").on("submit", function(e) {
        e.preventDefault();

        var export_title = $("#export_title").val();
        var date_of_issue = $("#date_of_issue").val();

        var formData = new FormData($('#exportForm')[0]);
        formData.append("export_file_name", $('input[type=file]')[0].files[0]);
        formData.append("export_title", export_title);
        formData.append("date_of_issue", date_of_issue);

        $.ajax({
            type: 'POST',
            url: 'ajax/add-new-export.php',
            data: formData,
            dataType: "html",
            cache: false,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response == 0) {
                    exportForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    $("#eSuccess").append("Export data added Succesfully...").show();
                    setTimeout(function() {
                        location.href = "export-list.php"
                    }, 3000);
                } else if (response == 3) {
                    exportForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    alert("Invalid file type, please choose another file.");
                } else {
                    exportForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    alert("Something went wrong");
                }
            }
        });

    });



    /* Latest News */

    $("#nSuccess").hide();

    $("#elink").css('display','none');

    $("input[name='is_link']").change(function () {
    if ($("input[name='is_link']:checked").val() == "Yes") {
        $("#elink").slideDown(1000).show();
         $("#news_link").attr("required", true);
        $("#efile").hide();
    } else {
        $("#elink").hide("slow");
        $("#news_link").attr("required", false);
         $("#efile").show();
    }
    });

    $("#newsForm").on("submit", function(e) {
        e.preventDefault();

        var news_description = $("#news_description").val();

        if ($("#news_link").is(":visible")) {
          var news_link = $("#news_link").val();
        }

        if ($("#news_link").val().length > 0 &&  !validateUrl(news_link)){
             $('#news_link').focus();
            alert("Please enter a valid URL");
            return false;
        }

        var formData = new FormData($('#newsForm')[0]);
        formData.append("news_file_name", $('input[type=file]')[0].files[0]);
        formData.append("news_description", news_description);

         if ($("#news_link").val().length > 0) {
           formData.append("news_link", news_link);
           }

        $.ajax({
            type: 'POST',
            url: "ajax/add-news.php",
            data: formData,
            dataType: "html",
            cache: false,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response == 0) {
                    newsForm.reset();
                    $("#nSuccess").append("News Added Succesfully...").show();
                    setTimeout(function() {
                        location.href = "latest-news.php"
                    }, 3000);
                } else if (response == 3) {
                    newsForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    alert("Invalid file type, please choose another file.");
                }  else {
                    newsForm.reset();
                    alert("Something went wrong");
                }
            }
        });
    });

    /* Adding Categories */

    $("#ctSuccess").hide();

    $("#categoryForm").on("submit", function(e) {
        e.preventDefault();

        var category_name = $("#category_name").val();
        var dataStr = "category_name=" + category_name;

        $.ajax({
            type: "POST",
            url: "ajax/add-new-category.php",
            data: dataStr,
            cache: false,
            success: function(response) {
                if (response == 0) {
                    categoryForm.reset();
                    $("#ctSuccess").append("Category Added Succesfully...").show();
                    setTimeout(function() {
                        location.href = "category-list.php"
                    }, 3000);
                } else {
                    categoryForm.reset();
                    alert("Something went wrong");
                }
            }
        });
    });

    /* Image Upload */

    $("#gSuccess").hide();

    $("#galleryForm").on("submit", function(e) {
        e.preventDefault();

        var category_id = $("#category_id").val();

        var formData = new FormData($('#galleryForm')[0]);
        formData.append("gallery_image", $('input[type=file]')[0].files[0]);
        formData.append("category_id", category_id);

        $.ajax({
            type: 'POST',
            url: 'ajax/add-new-image.php',
            data: formData,
            dataType: "html",
            cache: false,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response == 0) {
                    galleryForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    $("#gSuccess").append("Image Uploaded Succesfully...").show();
                } else {
                    galleryForm.reset();
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    alert("Something went wrong");
                }
            }
        });

    });

    /* Rent Status Upload */

    $("#rnSuccess").hide();

    $("#rentForm").on("submit", function(e) {
    e.preventDefault();
    
    var company = $("#company").val();
    var rent_paid_of_year = $("#rent_paid_of_year").val();
    var rent_paid_of_quarter = $("#rent_paid_of_quarter").val();
    var rent_amount = $("#rent_amount").val();
    
    if (rent_amount == "" || rent_amount == undefined || rent_amount == 0 || rent_amount == "0.00") {
            $('#rent_amount').focus();
            alert("Please enter a valid rent amount");
            return false;
        }

    var formData = new FormData($('#rentForm')[0]);
    formData.append("rent_status_file_name", $('input[type=file]')[0].files[0]);
    formData.append("company", company);
    formData.append("rent_paid_of_year", rent_paid_of_year);
    formData.append("rent_paid_of_quarter", rent_paid_of_quarter);
    formData.append("rent_amount", rent_amount);
    
  
    
    $.ajax({
        type: 'POST',
        url: 'ajax/add-new-rent.php',
        data: formData,
        dataType: "html",
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
            if (response == 0) {
                rentForm.reset();
                $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                $("#rnSuccess").append("Rent status added Succesfully...").show();
                setTimeout(function() {
                    location.href = "rent-status-list.php"
                }, 3000);
            } else if (response == 3) {
                rentForm.reset();
                $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                alert("Invalid file type, please choose another file.");
            } else {
                rentForm.reset();
                $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                alert("Something went wrong");
            }
        }
      });

    });
	
	/* Contact Upload */

    $("#cnSuccess").hide();

    $("#cntForm").on("submit", function(e) {
    e.preventDefault();
    
    var contact_name = $("#contact_name").val();
    var designation = $("#designation").val();
    var email_id = $("#email_id").val();
    var contact_no = $("#contact_no").val();

    var formData = new FormData($('#cntForm')[0]);
    formData.append("contact_name", contact_name);
    formData.append("designation", designation);
    formData.append("email_id", email_id);
    formData.append("contact_no", contact_no);

    $.ajax({
        type: 'POST',
        url: 'ajax/add-new-contact.php',
        data: formData,
        dataType: "html",
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
            if (response == 0) {
                cntForm.reset();
                $("#cnSuccess").append("Contact added Succesfully...").show();
                setTimeout(function() {
                    location.href = "contact-list.php"
                }, 3000);
            } else {
                cntForm.reset();
                alert("Something went wrong");
            }
        }
      });

    });
	
	
	/* Add Company */

    $("#cmpSuccess").hide();

    $("#cmpForm").on("submit", function(e) {
    e.preventDefault();
    
    var company_name = $("#company_name").val();
    var company_email = $("#company_email").val();

    var formData = new FormData($('#cmpForm')[0]);
    formData.append("company_name", company_name);
    formData.append("company_email", company_email);

    $.ajax({
        type: 'POST',
        url: 'ajax/add-new-company.php',
        data: formData,
        dataType: "html",
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
            if (response == 0) {
                cmpForm.reset();
                $("#cmpSuccess").append("Company added Succesfully...").show();
                setTimeout(function() {
                    location.href = "company-list.php"
                }, 3000);
            } else if (response == 2)  {
                cmpForm.reset();
                alert("Company name already exist, please check your records...");
            } else {
                cmpForm.reset();
                alert("Something went wrong");
            }
        }
      });

    });
	
	/* Add Designation */

    $("#dsgSuccess").hide();

    $("#dnForm").on("submit", function(e) {
    e.preventDefault();
    
    var designation_name = $("#designation_name").val();
    var sequence =  $("#sequence").val();

    var formData = new FormData($('#dnForm')[0]);
    formData.append("designation_name", designation_name);
    formData.append("sequence", sequence);

    $.ajax({
        type: 'POST',
        url: 'ajax/add-new-designation.php',
        data: formData,
        dataType: "html",
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
            if (response == 0) {
                dnForm.reset();
                $("#dsgSuccess").append("Designation added Succesfully...").show();
                setTimeout(function() {
                    location.href = "designation-list.php"
                }, 3000);
            } else {
                dnForm.reset();
                alert("Something went wrong");
            }
        }
      });

    });

     /* Input box validation */

        $("input[type=text],[type=email]").on('paste  keypress' , function (e) {

            var keyCode = e.keyCode || e.which;
            $(".errcode").html("");
            $(".errcopy").html("");

            if(e.type === "paste") {
              $(".errcopy").html("*Note: Sorry copy and paste option is not available due to security reason. Please type in.");
            }

            //Regex for Valid Characters
            var regex = /^[A-Za-z0-9@/\s#,"&()-.]+$/;

            //Validate TextBox value against the Regex.
            var isValid = regex.test(String.fromCharCode(e.which));
            if (!isValid && e.type !== "paste") {
                $(".errcode").html("*Note: Sorry this character is not allowed.");
            }
            return isValid;
        });

        //It restrict the non-numbers
        var specialKeys = new Array();
        specialKeys.push(8,46); //Backspace
        function IsNumeric(e) {
            var keyCode = e.which ? e.which : e.keyCode;
            console.log( keyCode );
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
            return ret;
        } 
 
        /* Hide Add Category Button */

        var currUrl = $(location).attr('href');
        if (currUrl.includes('add-gallery-image')){
            $("#imgCategory").show();
        } else {
            $("#imgCategory").hide();
        }
});
 </script>
