<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>&copy;2022 Falta SEZ | Developed by <a href="https://openbraces.in/" rel="noopener noreferrer" target="_blank">OpenBraces Ventures Pvt Ltd</a></span>
        </div>
    </div>
</footer>

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>
<!-- Scroll to Top Button-->

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="logoutModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="logoutModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body text-center">Select "Logout" if you are ready to end your current session.</div>
            <div class="modal-footer justify-content-between">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="logout.php">Logout</a>
            </div>
        </div>
    </div>
</div>
<!-- Logout Modal-->

<!-- File Upload Validation Modal -->
<div class="modal fade" id="pdfModal" tabindex="-1" role="dialog">
    <div class="modal-dialog text-center" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h5 class="modal-title text-white">System Alert :: Invalid file type</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="text-danger">Please upload files having extensions : <strong>.pdf, .doc, .docx, .xls, .jpg, .jpeg, .png </strong> only.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- File Upload Validation Modal -->

<!-- Image Upload Validation Modal -->
<div class="modal fade" id="imageModal" tabindex="-1" role="dialog">
    <div class="modal-dialog text-center" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h5 class="modal-title text-white">System Alert :: Invalid file type</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="text-danger">Please upload files having extensions : <strong> .jpg, .jpeg, .png, .bpm, .gif
                    </strong> only. </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Image Upload Validation Modal -->

<!-- Company List Modal-->
<div class="modal fade" id="compModal" tabindex="-1" role="dialog" aria-labelledby="compModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <h5 class="modal-title text-white" id="compModalLabel">Error Occured</h5>
                <button class="close text-white" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body text-center">There seems to be an issue with the company email and the unique code.</div>
            <div class="modal-footer">
                <button class="btn btn-danger" type="button" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>
<!-- Company List Modal-->
