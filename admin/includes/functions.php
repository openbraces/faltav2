<?php
function total_tenders()
{
    global $con;
    $sql = "select count(*) as total from fsez_tenders";
    $res = mysqli_query($con, $sql);
    $row = mysqli_fetch_array($res);

    return $row["total"];
}

function total_circulars()
{
    global $con;
    $sql = "select count(*) as total from fsez_circulars";
    $res = mysqli_query($con, $sql);
    $row = mysqli_fetch_array($res);

    return $row["total"];
}

function total_forms()
{
    global $con;
    $sql = "select count(*) as total from fsez_forms";
    $res = mysqli_query($con, $sql);
    $row = mysqli_fetch_array($res);

    return $row["total"];
}

function total_vacancies()
{
    global $con;
    $sql = "select count(*) as total from fsez_vacancies";
    $res = mysqli_query($con, $sql);
    $row = mysqli_fetch_array($res);

    return $row["total"];
}

function total_categories()
{
    global $con;
    $sql = "select count(*) as total from categories where is_delete = 0";
    $res = mysqli_query($con, $sql);
    $row = mysqli_fetch_array($res);

    return $row["total"];
}

function check_sanity($data)
{
    $data = trim($data);
    $data = strip_tags($data);
    $data = htmlspecialchars($data);
    return $data;
}

function get_sanity($str)
{
    return htmlspecialchars($str, ENT_QUOTES);
}

function generateCompCode($str)
{
$cpname = preg_replace('/[^a-zA-Z0-9-_\.]/','', $str);
$chname = str_replace("-","h",$cpname); // Remove Hyphen and replace with h
$cpnewnm = strtolower(substr($chname , 0 , 4));
$chars = '0123456789abcdefghijklmnopqrstuvwxyz';
$charLength = strlen($chars);
$compCode = '';

for ($i = 0; $i < 6; $i++) {
    $compCode .= $chars[rand(0, $charLength - 1)];
}   

return $cpnewnm.strtoupper($compCode);
}

function generatePassword()
{
$passchars = '0123456789abcdefghijklmnopqrstuvwxyz';
$charLength = strlen($passchars);
$randPass = '';

for ($i = 0; $i < 10; $i++) {
    $randPass .= $passchars[rand(0, $charLength - 1)];
}   

return $randPass;
}

function getUserIpAddr(){
    if(!empty($_SERVER['HTTP_CLIENT_IP'])){
        //ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        //ip pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}
