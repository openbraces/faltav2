<ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar">
   <!-- Sidebar - Brand -->
   <a class="sidebar-brand d-flex align-items-center justify-content-center" href="dashboard.php">
      <div class="sidebar-brand-icon">
        <img src="img/favicon.png" alt="" class="img img-responsive">
      </div>
      <div class="sidebar-brand-text mx-3">FSEZ Admin</div>
   </a>
   <!-- Divider -->
   <hr class="sidebar-divider my-0">
   <!-- Nav Item - Dashboard -->
   <li class="nav-item active">
      <a class="nav-link" href="dashboard.php">
      <i class="fas fa-fw fa-cog"></i>
      <span>Dashboard</span></a>
   </li>
   <!-- Divider -->
   <hr class="sidebar-divider">
   <!-- Heading -->
   <div class="sidebar-heading">FSEZ Section</div>

   <!-- Nav Item - Pages Collapse Menu -->
   <li class="nav-item">
       <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
           aria-expanded="true" aria-controls="collapseTwo">
           <i class="fas fa-fw fa-cloud-upload-alt"></i>
           <span>Circulars & Tenders</span>
       </a>
       <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
           <div class="bg-grey py-2 collapse-inner rounded">
               <a class="collapse-item" href="circular-list.php">Circulars Section</a>
               <a class="collapse-item" href="tender-list.php">Tenders Section</a>
           </div>
       </div>
   </li>
   <li class="nav-item">
       <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseThree"
           aria-expanded="true" aria-controls="collapseTwo">
           <i class="fas fa-fw fa-cloud-upload-alt"></i>
           <span>Forms & Vacancies</span>
       </a>
       <div id="collapseThree" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
           <div class="bg-grey py-2 collapse-inner rounded">
               <a class="collapse-item" href="form-list.php">Forms Section</a>
               <a class="collapse-item" href="vacancy-list.php">Vacancies Section</a>
           </div>
       </div>
   </li>
   <li class="nav-item">
       <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFive"
           aria-expanded="true" aria-controls="collapseTwo">
           <i class="fas fa-fw fa-cloud-upload-alt"></i>
           <span>Manage Rent & Units</span>
       </a>
       <div id="collapseFive" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
           <div class="bg-grey py-2 collapse-inner rounded">
              <a class="collapse-item" href="company-list.php">Manage Units</a>
              <a class="collapse-item" href="rent-status-list.php">Manage Rent Status</a>
           </div>
       </div>
   </li>

   <hr class="sidebar-divider">
   <!-- Heading -->
   <div class="sidebar-heading">Authority Section </div>

   <!-- Nav Item - Pages Collapse Menu -->
   <li class="nav-item">
       <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseEight"
           aria-expanded="true" aria-controls="collapseTwo">
           <i class="fas fa-fw fa-cloud-upload-alt"></i>
           <span>Auth Agenda & Minutes</span>
       </a>
       <div id="collapseEight" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
           <div class="bg-grey py-2 collapse-inner rounded">
               <a class="collapse-item" href="authority-agenda-list.php">Authority Agenda Section</a>
               <a class="collapse-item" href="authority-meeting-list.php">Authority Minutes Section</a>
           </div>
       </div>
   </li>

   <hr class="sidebar-divider">
   <!-- Heading -->
   <div class="sidebar-heading">UAC Section</div>

   <!-- Nav Item - Pages Collapse Menu -->
   <li class="nav-item">
       <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFour"
           aria-expanded="true" aria-controls="collapseTwo">
           <i class="fas fa-fw fa-cloud-upload-alt"></i>
           <span>UAC Agenda & Minutes</span>
       </a>
       <div id="collapseFour" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
           <div class="bg-grey py-2 collapse-inner rounded">
               <a class="collapse-item" href="uac-agenda-list.php">UAC Agenda Section</a>
               <a class="collapse-item" href="uac-meeting-list.php">UAC Minutes Section</a>
           </div>
       </div>
   </li>

   <hr class="sidebar-divider">
   <!-- Heading -->
   <div class="sidebar-heading">EOU Section</div>

   <!-- Nav Item - Pages Collapse Menu -->
   <li class="nav-item">
       <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSeven"
           aria-expanded="true" aria-controls="collapseTwo">
           <i class="fas fa-fw fa-cloud-upload-alt"></i>
           <span>EOU Agenda & Minutes</span>
       </a>
       <div id="collapseSeven" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
           <div class="bg-grey py-2 collapse-inner rounded">
               <a class="collapse-item" href="eou-agenda-list.php">EOU Agenda Section</a>
               <a class="collapse-item" href="eou-meeting-list.php">EOU Minutes Section</a>
           </div>
       </div>
   </li>

   <hr class="sidebar-divider">
   <!-- Heading -->
   <div class="sidebar-heading">Contact & Designation</div>

   <!-- Nav Item - Pages Collapse Menu -->

   <li class="nav-item">
       <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSix"
           aria-expanded="true" aria-controls="collapseTwo">
           <i class="fas fa-fw fa-cloud-upload-alt"></i>
           <span>Contacts & Designation</span>
       </a>
       <div id="collapseSix" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
           <div class="bg-grey py-2 collapse-inner rounded">
               <a class="collapse-item" href="designation-list.php">Manage Designations</a>
               <a class="collapse-item" href="contact-list.php">Manage Contacts</a>
           </div>
       </div>
   </li>

   <hr class="sidebar-divider">
   <div class="sidebar-heading">Miscellaneous</div>
   <li class="nav-item">
      <a class="nav-link" href="latest-news.php">
      <i class="fas fa-fw fa-cloud-upload-alt"></i>
      <span>Latest Updates Section</span>
      </a>
   </li>
   <li class="nav-item">
      <a class="nav-link" href="rti-list.php">
      <i class="fas fa-fw fa-cloud-upload-alt"></i>
      <span>RTI Section</span>
      </a>
   </li>
   
   <!-- <li class="nav-item">
      <a class="nav-link" href="export-list.php">
      <i class="fas fa-fw fa-chart-area"></i>
      <span> Export Performance </span>
      </a>
   </li> -->

   <!-- Sidebar Toggler (Sidebar) -->
   <hr class="sidebar-divider">
   <div class="text-center d-none d-md-inline">
      <button class="rounded-circle border-0" id="sidebarToggle"></button>
   </div>
</ul>
