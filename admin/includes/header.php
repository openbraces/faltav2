<?php if (!empty($_SESSION['csrf_token '])) {
    ?>
<script>
window.csrf = {
    csrf_token: '<?php echo $_SESSION['csrf_token ']; ?>'
};
</script>
<?php
}
?>
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
    </button>

    <div class="navbar-img">
        <div class = "container">
            <div class="row">
                <div class="col col-sm-4">
                    <img src="./img/admin_header.jpg" class="img-responsive" alt="Falta SEZ">
                </div>
                <div class="col col-sm-8">
                    <h3>Falta Special Economic Zone</h3>
                </div>
            </div>
        </div>
    </div>

    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto">
        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
        <li class="nav-item dropdown no-arrow d-sm-none">
            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
            </a>
            <!-- Dropdown - Messages -->
            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                    <div class="input-group">
                        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..."
                            aria-label="Search" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="button">
                                <i class="fas fa-search fa-sm"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </li>

        <li class="nav-item">
            <a data-toggle="collapse" href="#collapseCategory" id="imgCategory" role="button" aria-expanded="false"
                aria-controls="collapseCategory" class="btn btn-sm btn-danger mt-3"><i
                                        class="fa fa-plus" aria-hidden="true"></i> Add New Category</a>
        </li>

        <div class="topbar-divider d-none d-sm-block"></div>

        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600">Hi, <?php echo $_SESSION["username"]; ?></span>
                <img class="img-profile rounded-circle" src="upload_images/<?php echo $_SESSION["profile_image"]; ?>">
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="profile-settings.php">
                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                    Profile Settings
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="global-settings.php">
                    <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                    Global Settings
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    Logout
                </a>
            </div>
        </li>
    </ul>
</nav>

<div class="collapse" id="collapseCategory">
    <div class="card card-body">
        <form class="form-inline" id="categoryForm">
            <div class="form-group mx-sm-3 col-6">
                <label for="category_name" class="sr-only">Category Name:</label>
                <input type="text" required class="form-control full-input-width" id="category_name" name="category_name"
                    placeholder="Enter Category name..." value="">
             
                    <div class="text-danger errcode mt-2"></div>
                    <div class="text-danger errcopy mt-2"></div>
            </div>
            <input type="submit" class="btn btn-sm btn-warning mr-5" name="submit" value="Add Category">
            <a href="category-list.php" class="btn btn-sm btn-info btn-right"><i class="fa fa-list" aria-hidden="true"></i> View Categories</a>
        </form>
        
        <div class="alert alert-success alert-dismissible mt-3" id="ctSuccess">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        </div>
    </div>
</div>
