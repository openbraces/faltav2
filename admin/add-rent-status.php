<?php
include "application-top.php";

if (!isset($_SESSION["email"])) {
    header("location:index.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>FALTA SEZ - Rent Status Upload</title>
    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> -->
    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.css" rel="stylesheet">
</head>

<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <?php include "includes/sidebar.php";?>
        <!-- End of Sidebar -->
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
                <?php include "includes/header.php";?>
                <!-- End of Topbar -->
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Outer Row -->
                    <div class="nav-container">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i></a>
                            </li>
                            <li class="breadcrumb-item active">Add New Rent Status</li>
                            <li class="ml-auto"><a href="rent-status-list.php" class="btn btn-sm btn-eou add-btn"> <i
                                        class="fa fa-list" aria-hidden="true"></i> View Rent Status</a></li>
                        </ol>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-xl-10 col-lg-12 col-md-9">
                            <div class="card o-hidden border-0 shadow-lg my-5">
                                <div class="card-body p-0">
                                    <!-- Nested Row within Card Body -->
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="p-5">
                                                <div class="text-center">
                                                    <h1 class="h4 text-gray-900 mb-4">Upload New Rent Status</h1>
                                                </div>
                                                <form class="user" id="rentForm" enctype="multipart/form-data">
												
												<?php 
												$cmsql = "select * from fsez_companies where status = 1";
												$cmres = mysqli_query($con, $cmsql);
												?>
                                                    <div class="form-group">
                                                        <select required name="company"
                                                            class="form-control custom-field" id="company">
                                                            <option selected="selected" disabled="true">Click to Select Company</option>
															<?php 
															if($cmres) 
															{
															 while ($cmrow = mysqli_fetch_array($cmres))
															 {
															?>
                                                            <option value="<?php echo $cmrow["company_id"]; ?>"><?php echo $cmrow["company_name"]; ?></option>
															<?php 
															 }
															}
															?>
                                                       </select>    
                                                    </div>
                                                    <div class="form-group">
                                                    <select required name="rent_paid_of_year"
                                                            class="form-control" id="rent_paid_of_year">
                                                            <option selected="selected" disabled="true">Select Year</option>
                                                            <option value="2021">2021</option>
                                                            <option value="2022">2022</option>
                                                            <option value="2023">2023</option>
                                                       </select>
                                                    </div>
                                                    <div class="form-group">
                                                    <select required name="rent_paid_of_quarter"
                                                            class="form-control" id="rent_paid_of_quarter">
                                                            <option selected="selected" disabled="true">Select Quarter</option>
                                                            <option value="1">First</option>
                                                            <option value="2">Second</option>
                                                            <option value="3">Third</option>
                                                            <option value="4">Fourth</option>
                                                       </select>
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <input type="text" required name="rent_amount"
                                                            class="form-control form-control-user" id="rent_amount" autocomplete="off"
                                                           onkeypress="return IsNumeric(event);" placeholder="Enter Rent Amount">
                                                    </div>

                                                    <input type="hidden" name="csrf_token"
                                                        value="<?php echo $_SESSION['csrf_token']; ?>" />
                                                    <div class="form-group">
                                                        <div class="custom-file">
                                                            <input type="file" required name="rent_status_file_name"
                                                                class="custom-file-input form-control-user"
                                                                accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,.pdf,.doc,.docx"
                                                                onchange="validateFile(id)" id="rent_status_file_name">
                                                            <label class="custom-file-label" for="customFile">Choose
                                                                file</label>
                                                        </div>
                                                    </div>
                                                    <input type="submit" class="btn btn-primary btn-user btn-block"
                                                        value="Upload Rent Status">
                                                </form>
                                                <hr>
                                                <p>
                                                  <div class="errcode text-danger mb-2"></div>
                                                  <div class="errcopy text-danger mb-2"></div>
                                                </p>
                                                <div class="alert alert-success alert-dismissible" id="rnSuccess">
                                                    <a href="#" class="close" data-dismiss="alert"
                                                        aria-label="close">&times;</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <!-- End of Main Content -->
            <!-- Footer -->
            <?php include "includes/footer.php";?>
            <!-- End of Footer -->
        </div>
        <!-- End of Content Wrapper -->
    </div>
    <!-- End of Page Wrapper -->
    <!-- Bootstrap and core JavaScript-->
    <?php include "includes/common-js.php";?>
</body>

</html>