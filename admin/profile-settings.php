<?php
include "application-top.php";

if (!isset($_SESSION["email"])) {
    header("location:index.php");
    exit();
}

$psql = "select * from admin_user where id =" . $_SESSION["id"];
$pres = mysqli_query($con, $psql);
$prow = mysqli_fetch_array($pres);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>FALTA SEZ - Profile Settings</title>
    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> -->
    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.css" rel="stylesheet">
</head>

<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <?php include "includes/sidebar.php";?>
        <!-- End of Sidebar -->
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
                <?php include "includes/header.php";?>
                <!-- End of Topbar -->
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Outer Row -->
                    <div class="nav-container">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i></a>
                            </li>
                            <li class="breadcrumb-item active">Profile Settings</li>
                            <li class="ml-auto"><a href="dashboard.php" class="btn btn-sm btn-success add-btn"> <i
                                        class="fa fa-home" aria-hidden="true"></i> Home</a></li>
                        </ol>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-xl-10 col-lg-12 col-md-9">
                            <div class="card o-hidden border-0 shadow-lg my-5">
                                <div class="card-body p-0">
                                    <!-- Nested Row within Card Body -->
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="p-5">
                                                <div class="text-center">
                                                    <h1 class="h4 text-gray-900 mb-4">Profile Settings</h1>
                                                </div>
                                                <p>
                                                  <div class="errcode text-danger mb-2"></div>
                                                  <div class="errcopy text-danger mb-2"></div>
                                                </p>
                                                <form class="user" id="pForm" enctype="multipart/form-data">
                                                    <div class="form-group">
                                                        <input type="text" required name="username"
                                                            class="form-control form-control-user"
                                                            value="<?php echo $prow["username"]; ?>" id="username"
                                                            placeholder="Enter Username">
                                                    </div>

                                                    <input type="hidden" name="csrf_token"
                                                        value="<?php echo $_SESSION['csrf_token']; ?>" />
                                                    <div class="form-group">
                                                        <input type="email" required name="email"
                                                            value="<?php echo $prow["email"]; ?>"
                                                            class="form-control form-control-user" id="email"
                                                            placeholder="Enter Email Address">
                                                    </div>

                                                    <div class="form-group">
                                                        <input type="text" required name="contact_no"
                                                            value="<?php echo $prow["contact_no"]; ?>"
                                                            class="form-control form-control-user" id="contact_no"
                                                            placeholder="Enter Contact Number">
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="custom-file">
                                                            <input type="file"
                                                                <?php if ($prow["profile_image"] === "") {?> required
                                                                <?php }?> name="profile_image"
                                                                class="custom-file-input form-control-user"
                                                                accept="image/*" onchange="validateImage(id)"
                                                                id="profile_image">
                                                            <label class="custom-file-label" for="customFile">Choose
                                                                file</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <img class="img-fluid rounded mx-auto d-block"
                                                            src="upload_images/<?php echo $prow["profile_image"]; ?>">
                                                    </div>
                                                    <input type="submit" class="btn btn-primary btn-user btn-block"
                                                        value="Update Settings">
                                                </form>
                                                <hr>
                                                <p>
                                                  <div class="errcode text-danger mb-2"></div>
                                                  <div class="errcopy text-danger mb-2"></div>
                                                </p>
                                                <div class="alert alert-success alert-dismissible" id="pSuccess">
                                                    <a href="#" class="close" data-dismiss="alert"
                                                        aria-label="close">&times;</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <!-- End of Main Content -->
            <!-- Footer -->
            <?php include "includes/footer.php";?>
            <!-- End of Footer -->
        </div>
        <!-- End of Content Wrapper -->
    </div>
    <!-- End of Page Wrapper -->
    <!-- Bootstrap and core JavaScript-->
    <?php include "includes/common-js.php";?>

    <script>
    /* Update Settings */

    jQuery(document).ready(function($) {

        $("#pSuccess").hide();

        $("#pForm").on("submit", function(e) {
            e.preventDefault();

            var uid = <?php echo $_SESSION["id"]; ?>;
            var username = $("#username").val();
            var email = $("#email").val();
            var contact_no = $("#contact_no").val();


            var formData = new FormData($('#pForm')[0]);
            formData.append("profile_image", $('input[type=file]')[0].files[0]);
            formData.append("uid", uid);
            formData.append("username", username);
            formData.append("email", email);
            formData.append("contact_no", contact_no);

            $.ajax({
                type: "POST",
                url: "ajax/update-profile.php?uid=" + uid,
                data: formData,
                dataType: "html",
                cache: false,
                contentType: false,
                processData: false,
                success: function(response) {
                    if (response == 0) {
                        $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                        $("#pSuccess").append("Profile Updated Succesfully...").show();
                        setTimeout(function() {
                            location.href = "profile-settings.php"
                        }, 3000);
                    } else if (response == 3) {
                    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                    alert("Invalid file type, please choose another file.");
                    } else {
                        $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                        alert("Something went wrong");
                    }
                }
            });

        });
    });
    </script>
</body>

</html>