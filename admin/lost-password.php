<?php 
include("application-top.php");	
require("class.phpmailer.php");

if($_POST["email"] != "")
{

  $email = mysqli_real_escape_string($con, check_sanity($_POST["email"])); 
   
   $sql = "select * from admin_user where email = '$email' and status = 1";
   $res = mysqli_query($con, $sql) or die("Query Fail");
   
   $numrow = mysqli_num_rows($res);
   
   if($numrow > 0 )
   {
    date_default_timezone_set("Asia/Calcutta");  
    $passtoken = md5($email).rand(10,99999);
    $expFormat = mktime(date("H"), date("i")+15, date("s"), date("m") ,date("d"), date("Y"));
    $expiryTime = date("Y-m-d H:i:s",$expFormat);

	 $row = mysqli_fetch_array($res);
   
    $pwdLink = SITE_URL."reset-password.php?id=".base64_encode($row["id"])."&token=".$passtoken;
	
    $mail = new PHPMailer();
    
    $mail->IsSMTP();
    $mail->CharSet = 'UTF-8';
    $mail->Host = "smtp.gmail.com";
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = "tls";
    $mail->Priority = 1;
    $mail->Port = 587;
    $mail->Username = "avikdatta2020@gmail.com";
    $mail->Password = "gbfqkroidgysntuh";
    
    $mail->From = "avikdatta2020@gmail.com";
    $mail->FromName = "Admin Reset Password";
    $mail->AddAddress($row["email"]);
    //$mail->AddReplyTo("mail@mail.com");
    
    $mail->IsHTML(true);
    $mail->Subject = "Password recovery email";
    $mail->Body = "<html>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge' />
    <title>Password Reset Email</title>
    <style type='text/css'>
       @media screen {
       @font-face {
       font-family: 'Lato';
       font-style: normal;
       font-weight: 400;
       src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
       }
       @font-face {
       font-family: 'Lato';
       font-style: normal;
       font-weight: 700;
       src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
       }
       @font-face {
       font-family: 'Lato';
       font-style: italic;
       font-weight: 400;
       src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
       }
       @font-face {
       font-family: 'Lato';
       font-style: italic;
       font-weight: 700;
       src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
       }
   
       /* concert-one-regular - latin */
       @font-face {
       font-family: 'Concert One';
       font-style: normal;
       font-weight: 400;
       src: local('Concert One'),
            url('https://openbraces.in/test_fsez/fonts/fonts/concert-one-v16-latin-regular.woff2') format('woff2'), /* Super Modern Browsers */
            url('https://openbraces.in/test_fsez/fonts/fonts/concert-one-v16-latin-regular.woff') format('woff'), /* Modern Browsers */
       }
       }
       /* CLIENT-SPECIFIC STYLES */
       body,
       table,
       td,
       a {
       -webkit-text-size-adjust: 100%;
       -ms-text-size-adjust: 100%;
       }
       table,
       td {
       mso-table-lspace: 0pt;
       mso-table-rspace: 0pt;
       }
       img {
       -ms-interpolation-mode: bicubic;
       }
       /* RESET STYLES */
       img {
       border: 0;
       height: auto;
       line-height: 100%;
       outline: none;
       text-decoration: none;
       }
       table {
       border-collapse: collapse !important;
       }
       body {
       height: 100% !important;
       margin: 0 !important;
       padding: 0 !important;
       width: 100% !important;
       font-size: 16px;
       }
       /* iOS BLUE LINKS */
       a[x-apple-data-detectors] {
       color: inherit !important;
       text-decoration: none !important;
       font-size: inherit !important;
       font-family: inherit !important;
       font-weight: inherit !important;
       line-height: inherit !important;
       }
       /* MOBILE STYLES */
       @media screen and (max-width:680px) {
       h1 {
       font-size: 32px !important;
       line-height: 32px !important;
       }
       p {
         font-size: 14px !important;
       }
       }
       /* ANDROID CENTER FIX */
       div[style*='margin: 16px 0;'] {
       margin: 0 !important;
       }
    </style>
    </head>
    <body style='background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;'>
       <table border='0' cellpadding='0' cellspacing='0' width='100%'>
          <!-- LOGO -->
          <tr>
             <td bgcolor='#FFA73B' align='center'>
                <table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 680px;'>
                   <tr>
                      <td align='center' valign='top' style='padding: 40px 10px 40px 10px;'></td>
                   </tr>
                </table>
             </td>
          </tr>
          <tr>
             <td bgcolor='#FFA73B' align='center' style='padding: 0px 10px 0px 10px;'>
                <table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 680px;'>
                   <tr>
                      <td bgcolor='#ffffff' align='center' valign='top' style='padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 4px; line-height: 48px;'>
                         <img src='".ABS_PATH."/images/email_banner.jpg' alt='banner' width='100%' height='auto' style='display: block; border: 0px; margin:0;' />
                         <h1 style='font-size: 42px; font-weight: 400; margin: 1em 0 0.2em 0; font-family:'Concert One', Helvetica, Arial, sans-serif;'>Reset Your Password</h1>
                      </td>
                   </tr>
                </table>
             </td>
          </tr>
          <tr>
             <td bgcolor='#f4f4f4' align='center' style='padding: 0px 10px 0px 10px;'>
                <table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 680px;'>
                   <tr>
                      <td bgcolor='#ffffff' align='left' style='padding: 20px 30px 0 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;'>
                         <p style='margin: 14px; padding: 0 20px;font-size: 16px;'>Hello, FSEZ Admin!</p>
                      </td>
                   </tr>
                   <tr>
                      <td bgcolor='#ffffff' align='left' style='padding: 20px 30px 10px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;'>
                         <p style='margin: 14px; padding: 0 20px;font-size: 16px;'> You are receiving this email as you requested to reset your password for <a href='$gbl_row[site_url]' target='_blank'>Falta SEZ Admin</a>. 
                         Resetting your password is easy. Just click the button below and follow the instructions. We'll have you up and running in no time.</p>
                         </p>
                      </td>
                   </tr>
                   <tr>
                      <td bgcolor='#ffffff' align='left'>
                         <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                            <tr>
                               <td bgcolor='#ffffff' align='center' style='padding: 20px 30px 30px 30px;'>
                                  <table border='0' cellspacing='0' cellpadding='0'>
                                     <tr>
                                        <td align='center'><a style='border-radius: 3px; font-size: 20px; font-family: Helvetica, Arial, sans-serif; padding: 15px 25px; border: 1px solid #01c257; background:#01c257; color:#ffffff; text-decoration:none; display: inline-block;' href='".$pwdLink."'>Reset Password</a></td>
                                     </tr>
                                   </table>
                               </td>
                            </tr>
                         </table>
                      </td>
                   </tr>
                   <!-- COPY -->
                   <tr>
                      <td bgcolor='#ffffff' align='left' style='padding: 0px 30px 0px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;'>
                         <p style='margin: 14px; padding: 0 20px;font-size: 16px;'> We recommend that you keep your password secure and not share it with anyone. In case you did not initiate this request, please <a href='".SITE_PATH."/contact.php' target='_blank'>contact us</a> immediately.</p>
                         <br>
                         <p style='margin: 14px; padding: 0 20px;font-size: 16px; color:#15c;'> 
                          <span style='color:red;'>*</span>
                          <em> Please note the link is valid only for next 15 minutes, beyond that it will expire.</em>
                         </p>
                      </td>
                   </tr>
                   <!-- COPY -->
                   <tr>
                   <table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 680px;'>
                    <tr>
                       <td bgcolor='#ffffff' align='left' style='padding: 50px 30px 40px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;'>
                         <p style='margin: 14px; padding: 0 20px;font-size: 16px;'>Regards,<br>Falta SEZ Admin</p>
                         <p style='margin: 14px; padding: 0 20px;font-size: 16px;'>Social Connect: <a href='https://www.facebook.com/Falta-Special-Economic-Zone-221156656563672' target='_blank'><img src='".ABS_PATH."/images/facebook_logo.png' width='26px' height='26px' style='vertical-align:top; padding:0px 5px;
                         ' alt='facebook'></a> <a href='https://twitter.com/FaltaZone' target='_blank'><img src='".ABS_PATH."/images/twitter_logo.png' style='vertical-align:top; padding:0px 5px;' width='26px' height='26px' alt='twitter'></a></p>
                        </td>
                        <td bgcolor='#ffffff' align='right' style='padding: 50px 30px 40px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;'>
                          <img src='".ABS_PATH."/images/fsez_logo.jpg' alt='falta logo' width='80px' height='auto' style='display: block; border: 0px; margin:40px 40px 40px 0px;' />
                       </td>
                      <br>
                      <tr>
                    </table>
                   </tr>
                </table>
             </td>
          </tr>
          <tr>
             <td bgcolor='#f4f4f4' align='center' style='padding: 0px 10px 0px 10px;'>
                <table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 680px;'>
                   <tr>
                      <td bgcolor='#ffaf4c' align='center' style='padding: 10px 30px 30px 30px; border-radius: 4px 4px 4px 4px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;'>
                         <h2 style='font-size: 20px; font-weight: 400; color: #111111; margin: 9px;'>Should you require any support</h2>
                         <p style='margin: 6px 10px 10px 10px;font-size: 14px;'>Feel free to reach out to us <a href='mailto:fsez@nic.in' style='color: #435acf; text-decoration: none;'>fsez@nic.in</a> We will respond shortly!</p>
                      </td>
                   </tr>
                </table>
             </td>
          </tr>
          <tr>
             <td bgcolor='#f4f4f4' align='center' style='padding: 0px 10px 0px 10px;'>
                <table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 680px;'>
                   <tr>
                      <td bgcolor='#f4f4f4' align='center' style='padding: 0px 30px 30px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 400; line-height: 18px;'>
                         <br>
                         <p style='margin: 0 0 20px 0;font-size: 12px;'>&#169; 2022 All Rights Reserved. Falta Special Economic Zone.</p>
                      </td>
                   </tr>
                </table>
             </td>
          </tr>
       </table>
    </body>
    </html>";

    if(!$mail->Send())
    {
    echo "Message could not be sent. <p>";
    echo "Mailer Error: " . $mail->ErrorInfo;
    exit;
    }
    
    $fg_sql = "update admin_user set passtoken = '$passtoken', link_expiryTime = '$expiryTime', link_sentOn = NOW() where id=".$row["id"];
    $fg_res = mysqli_query($con, $fg_sql);

    if($fg_res) {
      echo "1";
    }
   
    }
    else
   {
    echo "<span> Email address does not exist ! </span>";
   }	
   } 
   
 
?>
