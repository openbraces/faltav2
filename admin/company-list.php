<?php
   include("application-top.php");

   if(!isset($_SESSION["email"]))
   {
	header("location:index.php");
	exit();
   }
   
   $cn_sql = "select * from fsez_companies order by added_on desc";
   $cn_res = mysqli_query($con, $cn_sql);
   
   if(isset($_GET["mode"]))
   {
   if ($_GET["mode"] == "publish")
   {
   if($_GET["val"] == "pub")
   {
   $utsql = "update fsez_companies set status = 0 where company_id=".$_GET["company_id"];
   $uures = mysqli_query($con, $utsql);
   
   if($uures)
   {
   header("location:company-list.php");
   exit();
   }
   }
   else 	
   {
   $utsql = "update fsez_companies set status = 1 where company_id=".$_GET["company_id"];
   $uures = mysqli_query($con, $utsql);
   
   if($uures)
   {
   header("location:company-list.php");
   exit();
   }
   }
   }
   }
   
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>FALTA SEZ - Unit List</title>
    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> -->
    <!-- Custom styles for this template -->
    <link href="css/sb-admin-2.css" rel="stylesheet">
    <!-- Custom styles for this page -->
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
</head>

<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <?php include("includes/sidebar.php"); ?>
        <!-- End of Sidebar -->
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
                <?php include("includes/header.php"); ?>
                <!-- End of Topbar -->
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800"></h1>
                    <!-- DataTales Example -->
                    <div class="nav-container">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i></a>
                            </li>
                            <li class="breadcrumb-item active">Units under FSEZ</li>
                            <li class="ml-auto"><a href="add-rent-status.php" class="btn btn-sm btn-uac add-btn"> <i
                                        class="fa fa-plus" aria-hidden="true"></i> Upload Rent Status</a></li>
                        </ol>
                    </div>

                    <div class="col-md-12" id="confirm" style="display:none">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                          Unit has been deleted successfully !!!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>


                 <div class="col-md-12" id="ntfySuccess" style="display:none">
                    <div class="alert alert-success alert-dismissible mt-3" role="alert">   
                      Email sent successfully...                 
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                    </div>
                </div>

                    <?php 
            		if(isset($_GET["msg"]))
            		{
            		?>
            		<div class="alert alert-success alert-dismissible fade show" role="alert">
            			 <?php echo $_GET["msg"];?>
            			<button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="cngurlparam()">
            			<span aria-hidden="true">&times;</span>
            			</button>
                   </div>
            		<?php
            		}
            		?>
					
				   <div class="card shadow mb-4">
					<div class="card-header py-3">
						<h6 class="m-0 font-weight-bold text-primary">Add New Unit</h6>
					</div>
					<div class="card-body">
					  <form class="form-inline" id="cmpForm">							
						 <div class="form-group mr-4 input-width-40">
							<input type="text" required name="company_name"
							 class="form-control full-input-width" id="company_name"
									placeholder="Enter Unit Name">
						  </div>
						  
						   <div class="form-group mr-4 input-width-40">
							<input type="email" required name="company_email"
							 class="form-control full-input-width" id="company_email" autocomplete="off"
									placeholder="Enter Unit Email">
						  </div>
																						  
						  <input type="hidden" name="csrf_token"
								value="<?php echo $_SESSION['csrf_token']; ?>" />
						   
						   <div id="errcode" class="small text-danger mb-2"></div>
						   <div id="errcopy" class="small text-info mb-2"></div>

							<input type="submit" class="btn btn-success btn-right"
								value="Save Unit Detail">
						</form>

                        <p>
                            <div class="errcode text-danger"></div>
                            <div class="errcopy text-danger"></div>
                        </p>

                      <div class="alert alert-success alert-dismissible mt-3" id="cmpSuccess">
							<a href="#" class="close" data-dismiss="alert"
								aria-label="close">&times;</a>
						</div>						
					 </div>
					</div>

                    <div id="loader" class="overlay" style="display: none;">
                        <div class="overlay__inner">
                            <div class="overlay__content">
                                <img src='img/mail-loader.gif'>
                            </div>   	 
                        </div>
                    </div>
					
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Company List</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered ttable" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>SL No.</th>
                                            <th>Unit Name</th>
                                            <th>Unit Code</th>
                                            <th>Unit Email</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>SL No.</th>
                                            <th>Unit Name</th>
                                            <th>Unit Code</th>
                                            <th>Unit Email</th>
                                            <th>Actions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php
                                    $i = 0;
                                    if($cn_res)
                                    {
                                    while($cn_row = mysqli_fetch_array($cn_res))
                                    {
                                    $i++;
                                    ?>
                                        <tr>
                                            <td><?php echo $i; ?>.</td>
                                            <td><?php echo $cn_row["company_name"]; ?></td>
                                            <td>
                                                <div class="input-group">
                                                  <input type="password" class="form-control" disabled value="<?php echo $cn_row["company_code"]; ?>">
                                                   <button class="btn btn-eou input-group-addon reveal custom-border" type="button"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                                  </div>
                                            </td>
                                            <td><?php echo $cn_row["company_email"]; ?></td>
                                            <td>
                                                <a href="#" data-id="<?php echo $cn_row["company_id"];?>"
                                                   data-toggle="modal" data-target="#edModal" 
                                                    class="btn btn-info btn-sm edit">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                                <?php
                                          if(obtools::is_company_published($cn_row["company_id"]))
                                          { 
                                          ?>
                                                <a href="company-list.php?company_id=<?php echo $cn_row["company_id"];?>&mode=publish&val=pub"
                                                    class="btn btn-success btn-sm">
                                                    <i class="fas fa-check"></i>
                                                </a>
                                                <?php 
                                          }
                                          else
                                          {
                                          ?>
                                                <a href="company-list.php?company_id=<?php echo $cn_row["company_id"];?>&mode=publish&val=unpub"
                                                    class="btn btn-warning btn-sm">
                                                    <i class="fas fa-times"></i>
                                                </a>
                                                <?php
                                          }
                                          ?>
                                                <a href="#" data-id="<?php echo $cn_row["company_id"];?>"
                                                    data-toggle="modal" data-target="#cModal"
                                                    class="btn btn-danger btn-sm delete">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                             
                                               <button data-id="<?php echo $cn_row["company_id"];?>" class="btn btn-primary btn-sm notify"    
                                                 <?php if ($cn_row["is_notification_sent"] == 1 || $cn_row["status"] == 0 ) {  ?>    disabled="disabled"   <?php } ?>>
                                                    <i class="fas fa-envelope"></i>
                                               </button>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- End of Main Content -->
            <!-- Footer -->
            <?php include("includes/footer.php"); ?>
            <!-- End of Footer -->
        </div>
        <!-- End of Content Wrapper -->
    </div>
    <!-- End of Page Wrapper -->

    <!-- Delete Modal -->
    <div class="modal fade" id="cModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header bg-danger">
                    <h4 class="modal-title text-white">Delete Unit</h4>
                    <button type="button" class="close text-white" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body text-center mt-4">
                    Are you sure want to delete this record ???
                </div>
                <hr>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="trash" data-dismiss="modal">Yes</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                </div>

            </div>
        </div>
    </div>
    <!-- Delete Modal -->
    
    <!-- Edit Modal -->
    <div class="modal fade" id="edModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header bg-primary">
                    <h4 class="modal-title text-white">Update Unit</h4>
                    <button type="button" class="close text-white" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div id="cpRes"></div>

            </div>
        </div>
    </div>
    <!-- Edit Modal -->

    <?php include("includes/common-js.php"); ?>

    <script>
    jQuery(document).ready(function($) {
           
        $("#confirm").hide();
        var cnid = '';
        $(".ttable").on('click', '.delete', function() {
            cnid = $(this).attr("data-id");
        });

        $("#trash").on('click', function() {
            if (cnid) {
                $.ajax({
                    type: "GET",
                    url: "ajax/delete-company.php?cnid=" + cnid,
                    cache: false,
                    success: function(response) {
                        if (response == 0) {
                            $('html, body').animate({
                                scrollTop: 0
                            }, 'slow');
                            $("#confirm").css("display", "block");
                            setTimeout(function() {
                                window.location = 'company-list.php';
                            }, 3000);
                        }
                    }
                });
            }
        });
        
       // $("#ntfySuccess").hide();
        $(".ttable").on('click', '.notify', function(){
            var cmpId = $(this).attr("data-id");
            var row = $(this).closest("tr")[0];
            var compEmail = row.cells[3].innerHTML;
            if ((cmpId == "" || cmpId == "0" || cmpId == null || cmpId == undefined) || (compEmail == "" || compEmail == null || compEmail == undefined)) {
              $("#compModal").modal('show');;
            } else {
                $.ajax({
                type : "GET",
                url  : "ajax/send-notification.php?company_id="+cmpId,
                cache: false,
                beforeSend: function() {
                    $("#loader").show();
                },
                success: function(response)
                {
                    if(response == 1) {
                        $("#loader").hide();   
                        pageScrollTop();     
                        $("#ntfySuccess").css("display", "block");
                        setTimeout(function() {
                            location.href = "company-list.php"
                        }, 3000);
                    } else {
                        alert(response);
                    }         
                },
                complete:function(data){
                    $("#loader").hide();
                },
                error:function(err){
                    alert(err);
                    $("#loader").hide();
                    pageScrollTop();
                }
                });
                }
        });

        $(".ttable").on('click','.reveal', function() {
            var cmCode = $(this).parent().find('input').attr('type');
            if (cmCode === 'password') {
                $(this).parent().find('input').attr('type', 'text');
            } else {
                $(this).parent().find('input').attr('type', 'password');
            }
        });  

        $(".ttable").on('click', '.edit', function(){
            var cpId = $(this).attr("data-id");
            var ctoken = '<?php echo $_SESSION['csrf_token']; ?>';
            if (cpId) {
                $.ajax({
                type : "GET",
                url  : "ajax/update-company.php?ctoken="+ctoken+"&company_id="+cpId,
                cache: false,
                success: function(data)
                {
                    $("#cpRes").html(data);
                    pageScrollTop();
                }
                });
                }
        });
    });
    
    function cngurlparam() {
        var cnurl = window.location.href;
        var newUrl = cnurl.split("?");
        location.href = location.href.replace(window.location.href, newUrl[0]); 
    }
    
    
</script>
</body>

</html>