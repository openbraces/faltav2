<?php
include "application-top.php";

$sql = "select * from fsez_uac_meetings where uac_id=" . $_GET["uac_id"];
$res = mysqli_query($con, $sql);

if ($res) {
    $row = mysqli_fetch_array($res);
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>FALTA SEZ - UAC Minutes Update</title>
    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> -->
    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.css" rel="stylesheet">
</head>

<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <?php include "includes/sidebar.php";?>
        <!-- End of Sidebar -->
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
                <?php include "includes/header.php";?>
                <!-- End of Topbar -->
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Outer Row -->
                    <div class="nav-container">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i></a>
                            </li>
                            <li class="breadcrumb-item active">Edit UAC Minutes</li>
                            <li class="ml-auto"><a href="uac-meeting-list.php" class="btn btn-sm btn-eou add-btn"> <i
                                        class="fa fa-list" aria-hidden="true"></i> View UAC Minutes</a></li>
                        </ol>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-xl-10 col-lg-12 col-md-9">
                            <div class="card o-hidden border-0 shadow-lg my-5">
                                <div class="card-body p-0">
                                    <!-- Nested Row within Card Body -->
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="p-5">
                                                <div class="text-center">
                                                    <h1 class="h4 text-gray-900 mb-4">Update UAC Minutes</h1>
                                                </div>
                                                <form class="user" id="euacForm" enctype="multipart/form-data">
                                                    <div class="form-group">
                                                        <input type="text" required name="uac_meeting_title"
                                                            class="form-control form-control-user"
                                                            value="<?php echo $row["uac_meeting_title"]; ?>" id="uac_meeting_title"
                                                            placeholder="Enter UAC Minutes Title">
                                                    </div>

                                                    <div class="form-group">
                                                        <input type="text" required name="date_of_issue"
                                                            class="form-control form-control-user" value="<?php echo $row["date_of_issue"]; ?>"
                                                            autocomplete="off" id="date_of_issue" placeholder="Date Of Issue">
                                                    </div>

                                                    <input type="hidden" name="csrf_token"
                                                        value="<?php echo $_SESSION['csrf_token']; ?>" />
                                                    <div class="form-group">
                                                        <iframe
                                                            src="upload_uac_meeting_documents/<?php echo $row["uac_meeting_file"]; ?>"
                                                            width="100%" height="400px"
                                                            alt="<?php echo $row["uac_meeting_title"]; ?>"></iframe>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="custom-file">
                                                            <input type="file" name="uac_meeting_file"
                                                                class="custom-file-input form-control-user"
                                                                accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,.pdf,.doc,.docx,image/*"
                                                                onchange="validateFile(id)" id="uac_meeting_file">
                                                            <label class="custom-file-label" for="customFile">Choose
                                                                file</label>
                                                        </div>
                                                    </div>
                                                    <input type="submit" class="btn btn-primary btn-user btn-block"
                                                        value="Update UAC Minutes">
                                                </form>
                                                <hr>
                                                <p>
                                                  <div class="errcode text-danger mb-2"></div>
                                                  <div class="errcopy text-danger mb-2"></div>
                                                </p>
                                                <div class="alert alert-success alert-dismissible" id="euSuccess">
                                                    <a href="#" class="close" data-dismiss="alert"
                                                        aria-label="close">&times;</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <!-- End of Main Content -->
            <!-- Footer -->
            <?php include "includes/footer.php";?>
            <!-- End of Footer -->
        </div>
        <!-- End of Content Wrapper -->
    </div>
    <!-- End of Page Wrapper -->
    <!-- Bootstrap and core JavaScript-->
    <?php include "includes/common-js.php";?>

    <script>
    /* Update Tender */

    jQuery(document).ready(function($) {

        $("#euSuccess").hide();

        $("#euacForm").on("submit", function(e) {
            e.preventDefault();

            var uac_id = <?php echo $_GET["uac_id"]; ?>;
            var uac_meeting_title = $("#uac_meeting_title").val();
            var date_of_issue = $("#date_of_issue").val();

            var formData = new FormData($('#euacForm')[0]);
            formData.append("uac_meeting_file", $('input[type=file]')[0].files[0]);
            formData.append("uac_meeting_title", uac_meeting_title);
            formData.append("date_of_issue", date_of_issue);
            formData.append("uac_id", uac_id);


            $.ajax({
                type: "POST",
                url: "ajax/update-uac.php?uac_id=" + uac_id,
                data: formData,
                dataType: "html",
                cache: false,
                contentType: false,
                processData: false,
                success: function(response) {
                    if (response == 0) {
                        euacForm.reset();
                        $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                        $("#euSuccess").append("UAC Meeting Updated Succesfully...").show();
                        setTimeout(function() {
                            location.href = "uac-meeting-list.php"
                        }, 3000);
                    } else if (response == 3) {
                        euacForm.reset();
                        $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                        alert("Invalid file type, please choose another file.");
                        }  else {
                        euacForm.reset();
                        $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                        alert("Something went wrong");
                    }
                }
            });

        });
    });
    </script>
</body>

</html>