<?php
   include("application-top.php");

   if(!isset($_SESSION["email"]))
   {
	header("location:index.php");
	exit();
   }
   
   $c_sql = "select * from fsez_gallery_categories order by added_on desc";
   $c_res = mysqli_query($con, $c_sql);
   
   if(isset($_GET["mode"]))
   {
   if ($_GET["mode"] == "publish")
   {
   if($_GET["val"] == "pub")
   {
   $utsql = "update fsez_gallery_categories set status = 0 where category_id=".$_GET["category_id"];
   $uures = mysqli_query($con, $utsql);
   
   if($uures)
   {
   header("location:category-list.php");
   exit();
   }
   }
   else 	
   {
   $utsql = "update fsez_gallery_categories set status = 1 where category_id=".$_GET["category_id"];
   $uures = mysqli_query($con, $utsql);
   
   if($uures)
   {
   header("location:category-list.php");
   exit();
   }
   }
   }
   }
   
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>FALTA SEZ - Categories</title>
    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> -->
    <!-- Custom styles for this template -->
    <link href="css/sb-admin-2.css" rel="stylesheet">
    <!-- Custom styles for this page -->
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
</head>

<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <?php include("includes/sidebar.php"); ?>
        <!-- End of Sidebar -->
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
                <?php include("includes/header.php"); ?>
                <!-- End of Topbar -->
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800"></h1>
                    <!-- DataTales Example -->
                    <div class="nav-container">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i></a>
                            </li>
                            <li class="breadcrumb-item active">Categories</li>
                            <li class="ml-auto"><a href="dashboard.php" class="btn btn-sm btn-success add-btn"> <i
                                        class="fa fa-arrow-left" aria-hidden="true"></i> Back</a></li>
                        </ol>

                    </div>
                    
                    <?php 
                    if(isset($_GET["msg"]))
                    {
                    ?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <?php echo $_GET["msg"];?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <?php
                    }
                    ?>
                    
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Category List</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered cttable" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>SL No.</th>
                                            <th>Category Name</th>
                                            <th>Added On</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>SL No.</th>
                                            <th>Category Name</th>
                                            <th>Added On</th>
                                            <th>Actions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php
                                    $i = 0;
                                    if($c_res)
                                    {
                                    while($c_row = mysqli_fetch_array($c_res))
                                    {
                                    $i++;
                                    ?>
                                        <tr>
                                            <td><?php echo $i; ?>.</td>
                                            <td><?php echo $c_row["category_name"]; ?></td>
                                            <td><?php echo $c_row["added_on"]; ?></td>
                                            <td>
                                                 <a data-id="<?php echo $c_row["category_id"];?>" href="#"
                                                    data-toggle="modal" data-target="#ectModal"
                                                    class="btn btn-info btn-sm edit">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                                <?php
                                          if(obtools::is_category_published($c_row["category_id"]))
                                          { 
                                          ?>
                                                <a href="category-list.php?category_id=<?php echo $c_row["category_id"];?>&mode=publish&val=pub"
                                                    class="btn btn-success btn-sm">
                                                    <i class="fas fa-check"></i>
                                                </a>
                                                <?php 
                                          }
                                          else
                                          {
                                          ?>
                                                <a href="category-list.php?category_id=<?php echo $c_row["category_id"];?>&mode=publish&val=unpub"
                                                    class="btn btn-warning btn-sm">
                                                    <i class="fas fa-times"></i>
                                                </a>
                                                <?php
                                          }
                                          ?>
                                               <!-- <a href="#" class="btn btn-danger btn-sm">
                                                    <i class="fas fa-trash"></i>
                                                </a> -->
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- End of Main Content -->
            <!-- Footer -->
            <?php include("includes/footer.php"); ?>
            <!-- End of Footer -->
        </div>
        <!-- End of Content Wrapper -->
        
          <!-- Edit Modal-->
          
            <div class="modal fade" id="ectModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-warning">
                            <h5 class="modal-title text-white">Edit Category</h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
        
                        <div id="res"></div>
        
                    </div>
                </div>
            </div>
          <!-- Edit Modal-->
        
    </div>
    <!-- End of Page Wrapper -->
    <?php include("includes/common-js.php"); ?>
    
    <script>
    jQuery(document).ready(function($) {

        var ctid = '';
    
          $(".cttable").on('click', '.edit', function() {
            ctid = $(this).attr("data-id");
            if (ctid) {
                $.ajax({
                    type: "GET",
                    url: "ajax/update-category.php?ctid=" + ctid,
                    cache: false,
                    success: function(data) {
                        $("#res").html(data);
                    }
                });
            }
        });
    });
    </script>
</body>

</html>