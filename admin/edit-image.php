<?php 
   include("application-top.php");	

   $sql = "select * from fsez_gallery_images where id=".$_GET["id"];
   $res = mysqli_query($con, $sql);
   
   if($res)
   {
      $row = mysqli_fetch_array($res);
   }

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>FALTA SEZ - Gallery Update</title>
    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> -->
    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.css" rel="stylesheet">
</head>

<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <?php include("includes/sidebar.php"); ?>
        <!-- End of Sidebar -->
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
                <?php include("includes/header.php"); ?>
                <!-- End of Topbar -->
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Outer Row -->
                    <div class="nav-container">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i></a>
                            </li>
                            <li class="breadcrumb-item active">Edit Image</li>
                            <li class="ml-auto"><a href="image-list.php" class="btn btn-sm btn-success add-btn"> <i
                                        class="fa fa-image" aria-hidden="true"></i> View Images</a></li>
                        </ol>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-xl-10 col-lg-12 col-md-9">
                            <div class="card o-hidden border-0 shadow-lg my-5">
                                <div class="card-body p-0">
                                    <!-- Nested Row within Card Body -->
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="p-5">
                                                <div class="text-center">
                                                    <h1 class="h4 text-gray-900 mb-4">Update Image</h1>
                                                </div>
                                                <form class="user" id="egalleryForm" enctype="multipart/form-data">
                                                    <input type="hidden" name="csrf_token"
                                                        value="<?php echo $_SESSION['csrf_token']; ?>" />
                                                    <div class="form-group">
                                                        <?php
                                                    $gsql = "select * from fsez_gallery_categories where status=1";
                                                    $gres = mysqli_query($con, $gsql); 
                                                    ?>
                                                        <select name="category_id" id="category_id" required
                                                            class="form-control form-control-user">
                                                            <option selected="selected" disabled="true">Please Select the Category
                                                            </option>
                                                            <?php 
                                                        if($gres)
                                                        {
                                                            while($grow = mysqli_fetch_array($gres))
                                                            {
                                                                if($grow["category_id"] == $row["category_id"])
                                                                {
                                                                    $selected = "SELECTED";
                                                                }
                                                                else
                                                                {
                                                                    $selected = NULL;
                                                                }
                                                            ?>
                                                            <option value="<?php echo $grow["category_id"]; ?>"
                                                                <?php echo $selected; ?>>
                                                                <?php echo $grow["category_name"]; ?></option>
                                                            <?php
                                                            }
                                                        }
                                                        ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <img src="upload_gallery_images/<?php echo $row["gallery_image"]; ?>"
                                                            class="img-fluid rounded mx-auto d-block"
                                                            alt="Gallery Image"></iframe>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="custom-file">
                                                            <input type="file" name="gallery_image"
                                                                class="custom-file-input form-control-user"
                                                                accept="image/*" onchange="validateImage(id)"
                                                                id="gallery_image">
                                                            <label class="custom-file-label" for="customFile">Choose
                                                                file</label>
                                                        </div>
                                                    </div>
                                                    <input type="submit" class="btn btn-primary btn-user btn-block"
                                                        value="Upload Image">
                                                </form>
                                                <hr>
                                                <div class="alert alert-success alert-dismissible" id="egSuccess">
                                                    <a href="#" class="close" data-dismiss="alert"
                                                        aria-label="close">&times;</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <!-- End of Main Content -->
            <!-- Footer -->
            <?php include("includes/footer.php"); ?>
            <!-- End of Footer -->
        </div>
        <!-- End of Content Wrapper -->
    </div>
    <!-- End of Page Wrapper -->
    <!-- Bootstrap and core JavaScript-->
    <?php include("includes/common-js.php"); ?>
    <script>

    /* Update Image */

    jQuery(document).ready(function($) {

        $("#egSuccess").hide();

        $("#egalleryForm").on("submit", function(e) {
            e.preventDefault();

            var id = <?php echo $_GET["id"]; ?>;
            var category_id = $("#category_id").val();

            var formData = new FormData($('#egalleryForm')[0]);
            formData.append("gallery_image", $('input[type=file]')[0].files[0]);
            formData.append("category_id", category_id);
            formData.append("id", id);


            $.ajax({
                type: "POST",
                url: "ajax/update-image.php?id=" + id,
                data: formData,
                dataType: "html",
                cache: false,
                contentType: false,
                processData: false,
                success: function(response) {
                    if (response == 0) {
                        egalleryForm.reset();
                        $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                        $("#egSuccess").append("Image Updated Succesfully...").show();
                        setTimeout(function() {
                            location.href = "image-list.php"
                        }, 3000);
                    } else {
                        egalleryForm.reset();
                        $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                        alert("Something went wrong");
                    }
                }
            });

        });
    });
    </script>
</body>

</html>