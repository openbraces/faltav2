<?php
   include("application-top.php");

   if(!isset($_SESSION["email"]))
   {
	header("location:index.php");
	exit();
   }
   
   $um_sql = "select * from fsez_uac_meetings order by added_on desc";
   $um_res = mysqli_query($con, $um_sql);
   
   if(isset($_GET["mode"]))
   {
   if ($_GET["mode"] == "publish")
   {
   if($_GET["val"] == "pub")
   {
   $utsql = "update fsez_uac_meetings set status = 0 where uac_id=".$_GET["uac_id"];
   $uures = mysqli_query($con, $utsql);
   
   if($uures)
   {
   header("location:uac-meeting-list.php");
   exit();
   }
   }
   else 	
   {
   $utsql = "update fsez_uac_meetings set status = 1 where uac_id=".$_GET["uac_id"];
   $uures = mysqli_query($con, $utsql);
   
   if($uures)
   {
   header("location:uac-meeting-list.php");
   exit();
   }
   }
   }
   }
   
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>FALTA SEZ - UAC Minutes</title>
    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> -->
    <!-- Custom styles for this template -->
    <link href="css/sb-admin-2.css" rel="stylesheet">
    <!-- Custom styles for this page -->
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
</head>

<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <?php include("includes/sidebar.php"); ?>
        <!-- End of Sidebar -->
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
                <?php include("includes/header.php"); ?>
                <!-- End of Topbar -->
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800"></h1>
                    <!-- DataTales Example -->
                    <div class="nav-container">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i></a>
                            </li>
                            <li class="breadcrumb-item active">UAC Minutes List</li>
                            <li class="ml-auto"><a href="add-uac-meeting.php" class="btn btn-sm btn-uac add-btn"> <i
                                        class="fa fa-plus" aria-hidden="true"></i> Add UAC Minutes</a></li>
                        </ol>
                    </div>

                    <div class="col-md-12" id="confirm" style="display:none">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            UAC Minutes has been deleted successfully !!!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">UAC Meeting Minutes</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered utable" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>SL No.</th>
                                            <th>Minutes Title</th>
                                            <th>View File</th>
                                            <th>Added On</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>SL No.</th>
                                            <th>Minutes Title</th>
                                            <th>View File</th>
                                            <th>Added On</th>
                                            <th>Actions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php
                                    $i = 0;
                                    if($um_res)
                                    {
                                    while($um_row = mysqli_fetch_array($um_res))
                                    {
                                    $i++;
                                    ?>
                                        <tr>
                                            <td><?php echo $i; ?>.</td>
                                            <td><?php echo $um_row["uac_meeting_title"]; ?></td>
                                            <td><a href="upload_uac_meeting_documents/<?php echo $um_row["uac_meeting_file"]; ?>"
                                                    target="_blank" class="btn btn-primary btn-sm">
                                                    <i class="fas fa-file"></i> Click Here
                                                </a></td>
                                            <td><?php echo $um_row["added_on"]; ?></td>
                                            <td>
                                                <a href="edit-uac-meeting.php?uac_id=<?php echo $um_row["uac_id"]; ?>"
                                                    class="btn btn-info btn-sm">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                                <?php
                                          if(obtools::is_uac_published($um_row["uac_id"]))
                                          { 
                                          ?>
                                                <a href="uac-meeting-list.php?uac_id=<?php echo $um_row["uac_id"];?>&mode=publish&val=pub"
                                                    class="btn btn-success btn-sm">
                                                    <i class="fas fa-check"></i>
                                                </a>
                                                <?php 
                                          }
                                          else
                                          {
                                          ?>
                                                <a href="uac-meeting-list.php?uac_id=<?php echo $um_row["uac_id"];?>&mode=publish&val=unpub"
                                                    class="btn btn-warning btn-sm">
                                                    <i class="fas fa-times"></i>
                                                </a>
                                                <?php
                                          }
                                          ?>
                                                <a href="#" data-id="<?php echo $um_row["uac_id"];?>"
                                                    data-toggle="modal" data-target="#uModal"
                                                    class="btn btn-danger btn-sm delete">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- End of Main Content -->
            <!-- Footer -->
            <?php include("includes/footer.php"); ?>
            <!-- End of Footer -->
        </div>
        <!-- End of Content Wrapper -->
    </div>
    <!-- End of Page Wrapper -->

    <!-- Delete Modal -->
    <div class="modal fade" id="uModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header bg-danger">
                    <h4 class="modal-title text-white">Delete UAC Minutes</h4>
                    <button type="button" class="close text-white" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body text-center mt-4">
                    Are you sure want to delete this record ???
                </div>
                <hr>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="trash" data-dismiss="modal">Yes</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                </div>

            </div>
        </div>
    </div>
    <!-- Delete Modal -->


    <?php include("includes/common-js.php"); ?>

    <script>
    jQuery(document).ready(function($) {

        $("#confirm").hide();
        var uid = '';
        $(".utable").on('click', '.delete', function() {
            uid = $(this).attr("data-id");
        });

        $("#trash").on('click', function() {
            if (uid) {
                $.ajax({
                    type: "GET",
                    url: "ajax/delete-uac.php?uid=" + uid,
                    cache: false,
                    success: function(response) {
                        if (response == 0) {
                            $('html, body').animate({
                                scrollTop: 0
                            }, 'slow');
                            $("#confirm").css("display", "block");
                            setTimeout(function() {
                                window.location = 'uac-meeting-list.php';
                            }, 3000);
                        }
                    }
                });
            }
        });
    });
    </script>
</body>

</html>