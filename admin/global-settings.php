<?php
include "application-top.php";

if (!isset($_SESSION["email"])) {
    header("location:index.php");
    exit();
}

$gsql = "select * from fsez_global_settings where setting_id = 1";
$gres = mysqli_query($con, $gsql);
$grow = mysqli_fetch_array($gres);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>FALTA SEZ - Global Settings</title>
    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> -->
    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.css" rel="stylesheet">
</head>

<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <?php include "includes/sidebar.php";?>
        <!-- End of Sidebar -->
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
                <?php include "includes/header.php";?>
                <!-- End of Topbar -->
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Outer Row -->
                    <div class="nav-container">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i></a>
                            </li>
                            <li class="breadcrumb-item active">Global Settings</li>
                            <li class="ml-auto"><a href="dashboard.php" class="btn btn-sm btn-success add-btn"> <i
                                        class="fa fa-home" aria-hidden="true"></i> Home</a></li>
                        </ol>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-xl-10 col-lg-12 col-md-9">
                            <div class="card o-hidden border-0 shadow-lg my-5">
                                <div class="card-body p-0">
                                    <!-- Nested Row within Card Body -->
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="p-5">
                                                <div class="text-center">
                                                    <h1 class="h4 text-gray-900 mb-4">Global Settings</h1>
                                                </div>
                                                <p>
                                                  <div class="errcode text-danger mb-2"></div>
                                                  <div class="errcopy text-danger mb-2"></div>
                                                </p>
                                                <form class="user" id="gForm" enctype="multipart/form-data">
                                                    <div class="form-group">
                                                        <input type="text" required name="org_name"
                                                            class="form-control form-control-user"
                                                            value="<?php echo $grow["org_name"]; ?>" id="org_name"
                                                            placeholder="Enter Organisation Name">
                                                    </div>

                                                    <input type="hidden" name="csrf_token"
                                                        value="<?php echo $_SESSION['csrf_token']; ?>" />
                                                    <div class="form-group">
                                                        <input type="text" required name="org_info"
                                                            value="<?php echo $grow["org_info"]; ?>"
                                                            class="form-control form-control-user" id="org_info"
                                                            placeholder="Enter Organisation Information">
                                                    </div>


                                                    <div class="form-group">
                                                        <input type="url" required name="site_url"
                                                            value="<?php echo $grow["site_url"]; ?>"
                                                            class="form-control form-control-user" id="site_url"
                                                            placeholder="Enter Site Url">
                                                    </div>


                                                    <div class="form-group">
                                                        <input type="text" required name="admin_email"
                                                            value="<?php echo $grow["admin_email"]; ?>"
                                                            class="form-control form-control-user" id="admin_email"
                                                            placeholder="Enter Admin Email">
                                                    </div>


                                                    <div class="form-group">
                                                        <input type="text" required name="address"
                                                            value="<?php echo $grow["address"]; ?>"
                                                            class="form-control form-control-user" id="address"
                                                            placeholder="Enter Address">
                                                    </div>


                                                    <div class="form-group">
                                                        <input type="text" required name="contact_no"
                                                            value="<?php echo $grow["contact_no"]; ?>"
                                                            class="form-control form-control-user" id="contact_no"
                                                            placeholder="Enter Contact Number">
                                                    </div>


                                                    <div class="form-group">
                                                        <input type="text" required name="fb_link"
                                                            value="<?php echo $grow["fb_link"]; ?>"
                                                            class="form-control form-control-user" id="fb_link"
                                                            placeholder="Enter Facebook Page Url">
                                                    </div>

                                                    <div class="form-group">
                                                        <input type="text" required name="tw_link"
                                                            value="<?php echo $grow["tw_link"]; ?>"
                                                            class="form-control form-control-user" id="tw_link"
                                                            placeholder="Enter Twitter Page Url">
                                                    </div>

                                                    <div class="form-group">
                                                        <input type="text" required name="ln_link"
                                                            value="<?php echo $grow["ln_link"]; ?>"
                                                            class="form-control form-control-user" id="ln_link"
                                                            placeholder="Enter Twitter Page Url">
                                                    </div>

                                                   <div class="form-group">
                                                        <select name="is_maintenance" id="is_maintenance" required class="form-control">
                                                            <option disabled="true"> Please Select </option>
                                                            <option value="0" <?php echo ($grow["is_maintenance"]) == '0' ? 'selected' : ''; ?>> Maintenance Off </option>
                                                            <option value="1" <?php echo ($grow["is_maintenance"]) == '1' ? 'selected' : ''; ?>> Maintenance On</option>                                                 
                                                        </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="custom-file">
                                                            <input type="file"
                                                                <?php if ($grow["company_logo"] === "") {?> required
                                                                <?php }?> name="company_logo"
                                                                class="custom-file-input form-control-user"
                                                                accept="image/*" onchange="validateImage(id)"
                                                                id="company_logo">
                                                            <label class="custom-file-label" for="customFile">Choose
                                                                file</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <img class="img-fluid rounded mx-auto d-block"
                                                            src="upload_images/<?php echo $grow["company_logo"]; ?>">
                                                    </div>
                                                    <input type="submit" class="btn btn-primary btn-user btn-block"
                                                        value="Update Settings">
                                                </form>
                                                <hr>
                                                <p>
                                                  <div class="errcode text-danger mb-2"></div>
                                                  <div class="errcopy text-danger mb-2"></div>
                                                </p>
                                                <div class="alert alert-success alert-dismissible" id="gSuccess">
                                                    <a href="#" class="close" data-dismiss="alert"
                                                        aria-label="close">&times;</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <!-- End of Main Content -->
            <!-- Footer -->
            <?php include "includes/footer.php";?>
            <!-- End of Footer -->
        </div>
        <!-- End of Content Wrapper -->
    </div>
    <!-- End of Page Wrapper -->
    <!-- Bootstrap and core JavaScript-->
    <?php include "includes/common-js.php";?>

    <script>
    /* Update Settings */

    jQuery(document).ready(function($) {

        $("#gSuccess").hide();

        $("#gForm").on("submit", function(e) {
            e.preventDefault();

            var sid = 1;
            var org_name = $("#org_name").val();
            var org_info = $("#org_info").val();
            var site_url = $("#site_url").val();
            var admin_email = $("#admin_email").val();
            var address = $("#address").val();
            var contact_no = $("#contact_no").val();
            var fb_link = $("#fb_link").val();
            var tw_link = $("#tw_link").val();
            var ln_link = $("#ln_link").val();
            var is_maintenance = $("#is_maintenance").val();

            var formData = new FormData($('#gForm')[0]);
            formData.append("company_logo", $('input[type=file]')[0].files[0]);
            formData.append("sid", sid);
            formData.append("org_name", org_name);
            formData.append("org_info", org_info);
            formData.append("site_url", site_url);
            formData.append("admin_email", admin_email);
            formData.append("address", address);
            formData.append("contact_no", contact_no);
            formData.append("fb_link", fb_link);
            formData.append("tw_link", tw_link);
            formData.append("ln_link", ln_link);
            formData.append("is_maintenance", is_maintenance);

            $.ajax({
                type: "POST",
                url: "ajax/update-settings.php?sid=" + sid,
                data: formData,
                dataType: "html",
                cache: false,
                contentType: false,
                processData: false,
                success: function(response) {
                    if (response == 0) {
                        $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                        $("#gSuccess").append("Settings Updated Succesfully...").show();
                        setTimeout(function() {
                            location.href = "global-settings.php"
                        }, 3000);
                    } else {
                        $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                        alert("Something went wrong");
                    }
                }
            });

        });

        $("#is_maintenance").on('change', function(e) {
              var mode = $(this).val();
              if(mode == 1) {
                alert('By selecting this option, you are agreeing to put the site in maintenance.');
              } else {
                alert('Are you sure to remove the maintenance mode from the site.');
              }
        });
    });
    </script>
</body>

</html>