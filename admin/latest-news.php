<?php
   include("application-top.php");

   if(!isset($_SESSION["email"]))
   {
	header("location:index.php");
	exit();
   }
   
   $n_sql = "select * from fsez_news order by added_on desc";
   $n_res = mysqli_query($con, $n_sql);
   
   if(isset($_GET["mode"]))
   {
   if ($_GET["mode"] == "publish")
   {
   if($_GET["val"] == "pub")
   {
   $utsql = "update fsez_news set status = 0 where news_id=".$_GET["news_id"];
   $uures = mysqli_query($con, $utsql);
   
   if($uures)
   {
   header("location:latest-news.php");
   exit();
   }
   }
   else 	
   {
   $utsql = "update fsez_news set status = 1 where news_id=".$_GET["news_id"];
   $uures = mysqli_query($con, $utsql);
   
   if($uures)
   {
   header("location:latest-news.php");
   exit();
   }
   }
   }
   }
   
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>FALTA SEZ - Latest News</title>
    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> -->
    <!-- Custom styles for this template -->
    <link href="css/sb-admin-2.css" rel="stylesheet">
    <!-- Custom styles for this page -->
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
</head>

<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <?php include("includes/sidebar.php"); ?>
        <!-- End of Sidebar -->
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
                <?php include("includes/header.php"); ?>
                <!-- End of Topbar -->
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800"></h1>
                    <!-- DataTales Example -->
                    <div class="nav-container">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i></a>
                            </li>
                            <li class="breadcrumb-item active">Latest News</li>
                            <li class="ml-auto"><a href="add-latest-news.php" class="btn btn-sm btn-danger add-btn"> <i
                                        class="fa fa-plus" aria-hidden="true"></i> Add Latest News</a></li>
                        </ol>
                    </div>
                    
                    <div class="col-md-12" id="confirm" style="display:none">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            News has been deleted successfully !!!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Latest News</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered ntable" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>SL No.</th>
                                            <th>News Title</th>
                                            <th>Added Data</th>
                                            <th>Added On</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>SL No.</th>
                                            <th>News Title</th>
                                            <th>Added Data</th>
                                            <th>Added On</th>
                                            <th>Actions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php
                                    $i = 0;
                                    if($n_res)
                                    {
                                    while($n_row = mysqli_fetch_array($n_res))
                                    {
                                    $i++;
                                    ?>
                                        <tr>
                                            <td><?php echo $i; ?>.</td>
                                            <td><?php echo $n_row["news_description"]; ?></td>
                                            <?php if ($n_row["news_link"] !== "") { ?>
                                             <td><a href="<?php echo $n_row["news_link"]; ?>" title="<?php echo $n_row["news_description"]; ?>" target="_blank" class="btn btn-success btn-sm">External Link</a></td>
                                            <?php  }   else if ($n_row["news_file_name"] !== "noimage.gif") { ?>
                                            <td><a href="upload_news_documents/<?php echo $n_row["news_file_name"]; ?>" title="<?php echo $n_row["news_description"]; ?>" target="_blank" class="btn btn-warning btn-sm">Attached File</a></td>
                                            <?php } else { ?>
                                            <td><a href="javascript:void(0)" class="btn btn-sm btn-secondary">N/A</a></td>
                                            <?php } ?>
                                            <td><?php echo $n_row["added_on"]; ?></td>
                                            <td>
                                                <a href="edit-latest-news.php?news_id=<?php echo $n_row["news_id"]; ?>"
                                                    class="btn btn-info btn-sm">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                                <?php
                                          if(obtools::is_news_published($n_row["news_id"]))
                                          { 
                                          ?>
                                                <a href="latest-news.php?news_id=<?php echo $n_row["news_id"];?>&mode=publish&val=pub"
                                                    class="btn btn-success btn-sm">
                                                    <i class="fas fa-check"></i>
                                                </a>
                                                <?php 
                                          }
                                          else
                                          {
                                          ?>
                                                <a href="latest-news.php?news_id=<?php echo $n_row["news_id"];?>&mode=publish&val=unpub"
                                                    class="btn btn-warning btn-sm">
                                                    <i class="fas fa-times"></i>
                                                </a>
                                                <?php
                                          }
                                          ?>
                                                <a href="#" data-id="<?php echo $n_row["news_id"];?>"
                                                    data-toggle="modal" data-target="#nModal"
                                                    class="btn btn-danger btn-sm delete">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- End of Main Content -->
            <!-- Footer -->
            <?php include("includes/footer.php"); ?>
            <!-- End of Footer -->
        </div>
        <!-- End of Content Wrapper -->
    </div>
    <!-- End of Page Wrapper -->

    <!-- Delete Modal -->
    <div class="modal fade" id="nModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header bg-danger">
                    <h4 class="modal-title text-white">Delete News</h4>
                    <button type="button" class="close text-white" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body text-center mt-4">
                    Are you sure want to delete this record ???
                </div>
                <hr>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="trash" data-dismiss="modal">Yes</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                </div>

            </div>
        </div>
    </div>
    <!-- Delete Modal -->

    <?php include("includes/common-js.php"); ?>

    <script>
    jQuery(document).ready(function($) {

        $("#confirm").hide();
        var nid = '';
        $(".ntable").on('click', '.delete', function() {
            nid = $(this).attr("data-id");
        });

        $("#trash").on('click', function() {
            if (nid) {
                $.ajax({
                    type: "GET",
                    url: "ajax/delete-news.php?nid=" + nid,
                    cache: false,
                    success: function(response) {
                        if (response == 0) {
                            $('html, body').animate({
                                scrollTop: 0
                            }, 'slow');
                            $("#confirm").css("display", "block");
                            setTimeout(function() {
                                window.location = 'latest-news.php';
                            }, 3000);
                        }
                    }
                });
            }
        });
    });
    </script>
</body>

</html>