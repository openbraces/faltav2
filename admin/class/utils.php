<?php
class obtools
{
    public static function is_user_published($uid)
    {
        global $con;
        $usql = "select * from admin_user where id='$uid'";
        $ures = mysqli_query($con, $usql);
        $urow = mysqli_fetch_object($ures);

        if ($urow->status == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function is_circular_published($cid)
    {
        global $con;
        $csql = "select * from fsez_circulars where circular_id='$cid'";
        $cres = mysqli_query($con, $csql);
        $crow = mysqli_fetch_object($cres);

        if ($crow->status == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function is_post_published($pid)
    {
        global $con;
        $psql = "select * from posts where post_id='$pid'";
        $pres = mysqli_query($con, $psql);
        $prow = mysqli_fetch_object($pres);

        if ($prow->post_status == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function is_tender_published($tid)
    {
        global $con;
        $tsql = "select * from fsez_tenders where tender_id='$tid'";
        $tres = mysqli_query($con, $tsql);
        $trow = mysqli_fetch_object($tres);

        if ($trow->status == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function is_form_published($fid)
    {
        global $con;
        $fsql = "select * from fsez_forms where form_id='$fid'";
        $fres = mysqli_query($con, $fsql);
        $frow = mysqli_fetch_object($fres);

        if ($frow->status == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function is_vacancy_published($vid)
    {
        global $con;
        $vsql = "select * from fsez_vacancies where vacancy_id='$vid'";
        $vres = mysqli_query($con, $vsql);
        $vrow = mysqli_fetch_object($vres);

        if ($vrow->status == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function is_uac_published($uacid)
    {
        global $con;
        $uasql = "select * from fsez_uac_meetings where uac_id='$uacid'";
        $uares = mysqli_query($con, $uasql);
        $uarow = mysqli_fetch_object($uares);

        if ($uarow->status == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function is_agenda_published($uacagid)
    {
        global $con;
        $uasql = "select * from fsez_uac_agendas where agenda_id='$uacagid'";
        $uares = mysqli_query($con, $uasql);
        $uarow = mysqli_fetch_object($uares);

        if ($uarow->status == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function is_eou_agenda_published($eouagid)
    {
        global $con;
        $uasql = "select * from fsez_eou_agendas where agenda_id='$eouagid'";
        $uares = mysqli_query($con, $uasql);
        $uarow = mysqli_fetch_object($uares);

        if ($uarow->status == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function is_authorityAgenda_published($eouagid)
    {
        global $con;
        $uasql = "select * from fsez_auth_agendas where agenda_id='$eouagid'";
        $uares = mysqli_query($con, $uasql);
        $uarow = mysqli_fetch_object($uares);

        if ($uarow->status == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function is_eou_published($eouid)
    {
        global $con;
        $uasql = "select * from fsez_eou_meetings where eou_id='$eouid'";
        $uares = mysqli_query($con, $uasql);
        $uarow = mysqli_fetch_object($uares);

        if ($uarow->status == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function is_authorityMeeting_published($amid)
    {
        global $con;
        $amsql = "select * from fsez_auth_meetings where auth_id='$amid'";
        $amres = mysqli_query($con, $amsql);
        $amrow = mysqli_fetch_object($amres);

        if ($amrow->status == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function is_rti_published($rid)
    {
        global $con;
        $rsql = "select * from fsez_rti where rti_id='$rid'";
        $rres = mysqli_query($con, $rsql);
        $rrow = mysqli_fetch_object($rres);

        if ($rrow->status == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function is_export_published($eid)
    {
        global $con;
        $esql = "select * from fsez_exports where export_id='$eid'";
        $eres = mysqli_query($con, $esql);
        $erow = mysqli_fetch_object($eres);

        if ($erow->status == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function is_news_published($nid)
    {
        global $con;
        $nsql = "select * from fsez_news where news_id='$nid'";
        $nres = mysqli_query($con, $nsql);
        $nrow = mysqli_fetch_object($nres);

        if ($nrow->status == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function is_category_published($cid)
    {
        global $con;
        $csql = "select * from fsez_gallery_categories where category_id='$cid'";
        $cres = mysqli_query($con, $csql);
        $crow = mysqli_fetch_object($cres);

        if ($crow->status == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function is_image_published($mid)
    {
        global $con;
        $gsql = "select * from fsez_gallery_images where id='$mid'";
        $gres = mysqli_query($con, $gsql);
        $grow = mysqli_fetch_object($gres);

        if ($grow->status == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function is_contact_published($ctid)
    {
        global $con;
        $gsql = "select * from fsez_contacts where contact_id='$ctid'";
        $gres = mysqli_query($con, $gsql);
        $grow = mysqli_fetch_object($gres);

        if ($grow->status == 1) {
            return true;
        } else {
            return false;
        }
    }

    
    public static function is_company_published($cmid)
    {
        global $con;
        $gsql = "select * from fsez_companies where company_id='$cmid'";
        $gres = mysqli_query($con, $gsql);
        $grow = mysqli_fetch_object($gres);

        if ($grow->status == 1) {
            return true;
        } else {
            return false;
        }
    }
	
	public static function is_role_published($cmid)
    {
        global $con;
        $gsql = "select * from fsez_designations where designation_id='$cmid'";
        $gres = mysqli_query($con, $gsql);
        $grow = mysqli_fetch_object($gres);

        if ($grow->status == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function is_rent_published($rid)
    {
        global $con;
        $gsql = "select * from fsez_rent_status where rent_status_id='$rid'";
        $gres = mysqli_query($con, $gsql);
        $grow = mysqli_fetch_object($gres);

        if ($grow->status == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function get_user_name($un)
    {
        global $con;
        $unsql = "select * from admin_user where id='$un'";
        $unres = mysqli_query($con, $unsql);
        $unrow = mysqli_fetch_object($unres);

        echo $unrow->username;

    }

    public static function get_company_name($cn)
    {
        global $con;
        $cnsql = "select * from fsez_companies where company_id='$cn'";
        $cnres = mysqli_query($con, $cnsql);
        $cnrow = mysqli_fetch_object($cnres);

        echo $cnrow->company_name;

    }

    public static function get_contact_name($ctn)
    {
        global $con;
        $ctnsql = "select * from fsez_contacts where company_id='$ctn'";
        $ctnres = mysqli_query($con, $ctnsql);
        $ctnrow = mysqli_fetch_object($ctnres);

        echo $ctnrow->contact_name;

    }
	
	public static function get_designation_name($ctn)
    {
        global $con;
        $ctnsql = "select * from fsez_designations where designation_id='$ctn'";
        $ctnres = mysqli_query($con, $ctnsql);
        $ctnrow = mysqli_fetch_object($ctnres);

        echo $ctnrow->designation_name;

    }

    public static function get_category_name($cn)
    {
        global $con;
        $mtsql = "select * from fsez_gallery_categories where category_id='$cn'";
        $mtres = mysqli_query($con, $mtsql);
        $mtrow = mysqli_fetch_object($mtres);

        echo $mtrow->category_name;

    }

    public static function get_last_loggedin_time($id)
    {
        global $con;
        $lsql = "select * from  admin_user where id='$id'";
        $lres = mysqli_query($con, $lsql);
        $lrow = mysqli_fetch_object($lres);

        echo $lrow->last_loggedin;

    }

    public static function get_total_users()
    {
        global $con;
        $usql = "select count(*) as total_user from admin_user where status =1";
        $ures = mysqli_query($con, $usql);
        $urow = mysqli_fetch_object($ures);

        echo $urow->total_user;

    }

}
