<?php
   include("application-top.php");

   if(!isset($_SESSION["email"]))
   {
	header("location:index.php");
	exit();
   }
   
   $rn_sql = "select * from fsez_rent_status order by added_on desc";
   $rn_res = mysqli_query($con, $rn_sql);
   
   if(isset($_GET["mode"]))
   {
   if ($_GET["mode"] == "publish")
   {
   if($_GET["val"] == "pub")
   {
   $utsql = "update fsez_rent_status set status = 0 where rent_status_id=".$_GET["rent_status_id"];
   $uures = mysqli_query($con, $utsql);
   
   if($uures)
   {
   header("location:rent-status-list.php");
   exit();
   }
   }
   else 	
   {
   $utsql = "update fsez_rent_status set status = 1 where rent_status_id=".$_GET["rent_status_id"];
   $uures = mysqli_query($con, $utsql);
   
   if($uures)
   {
   header("location:rent-status-list.php");
   exit();
   }
   }
   }
   }
   
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>FALTA SEZ - Rent Status List</title>
    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> -->
    <!-- Custom styles for this template -->
    <link href="css/sb-admin-2.css" rel="stylesheet">
    <!-- Custom styles for this page -->
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
</head>

<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <?php include("includes/sidebar.php"); ?>
        <!-- End of Sidebar -->
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
                <?php include("includes/header.php"); ?>
                <!-- End of Topbar -->
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800"></h1>
                    <!-- DataTales Example -->
                    <div class="nav-container">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i></a>
                            </li>
                            <li class="breadcrumb-item active">Rents</li>
                            <li class="ml-auto"><a href="add-rent-status.php" class="btn btn-sm btn-uac add-btn"> <i
                                        class="fa fa-plus" aria-hidden="true"></i> Upload Rent Staus</a></li>
                        </ol>
                    </div>

                    <div class="col-md-12" id="confirm" style="display:none">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            Rent status has been deleted successfully !!!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Rent Receipt List</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered ttable" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>SL No.</th>
                                            <th>Unit Name</th>
											<th>Quarter</th>
                                            <th>Rent Receipt</th>
                                            <th>Added On</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>SL No.</th>
                                            <th>Unit Name</th>
											<th>Quarter</th>
                                            <th>Rent Receipt</th>
                                            <th>Added On</th>
                                            <th>Actions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php
                                    $i = 0;
                                    if($rn_res)
                                    {
                                    while($rn_row = mysqli_fetch_array($rn_res))
                                    {
                                    $i++;
                                    ?>
                                        <tr>
                                            <td><?php echo $i; ?>.</td>
                                            <td><?php echo (obtools::get_company_name($rn_row["company"])); ?></td>
											<td><?php echo $rn_row["rent_paid_of_year"]; ?> - <?php echo $rn_row["rent_paid_of_quarter"]; ?>Qr</td>
                                            <td><a href="upload_rent_documents/<?php echo $rn_row["rent_status_file_name"]; ?>"
                                                    target="_blank" class="btn btn-primary btn-sm">
                                                    <i class="fas fa-file"></i> Click Here
                                                </a>
                                            </td>
                                            <td><?php echo $rn_row["added_on"]; ?></td>
                                            <td>
                                                <a href="edit-rent-status.php?rent_status_id=<?php echo $rn_row["rent_status_id"]; ?>"
                                                    class="btn btn-info btn-sm">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                                <?php
                                          if(obtools::is_rent_published($rn_row["rent_status_id"]))
                                          { 
                                          ?>
                                                <a href="rent-status-list.php?rent_status_id=<?php echo $rn_row["rent_status_id"];?>&mode=publish&val=pub"
                                                    class="btn btn-success btn-sm">
                                                    <i class="fas fa-check"></i>
                                                </a>
                                                <?php 
                                          }
                                          else
                                          {
                                          ?>
                                                <a href="rent-status-list.php?rent_status_id=<?php echo $rn_row["rent_status_id"];?>&mode=publish&val=unpub"
                                                    class="btn btn-warning btn-sm">
                                                    <i class="fas fa-times"></i>
                                                </a>
                                                <?php
                                          }
                                          ?>
                                                <a href="#" data-id="<?php echo $rn_row["rent_status_id"];?>"
                                                    data-toggle="modal" data-target="#rModal"
                                                    class="btn btn-danger btn-sm delete">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- End of Main Content -->
            <!-- Footer -->
            <?php include("includes/footer.php"); ?>
            <!-- End of Footer -->
        </div>
        <!-- End of Content Wrapper -->
    </div>
    <!-- End of Page Wrapper -->

    <!-- Delete Modal -->
    <div class="modal fade" id="rModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header bg-danger">
                    <h4 class="modal-title text-white">Delete Rent</h4>
                    <button type="button" class="close text-white" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body text-center mt-4">
                    Are you sure want to delete this record ???
                </div>
                <hr>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="trash" data-dismiss="modal">Yes</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                </div>

            </div>
        </div>
    </div>
    <!-- Delete Modal -->

    <?php include("includes/common-js.php"); ?>

    <script>
    jQuery(document).ready(function($) {

        $("#confirm").hide();
        var rid = '';
        $(".ttable").on('click', '.delete', function() {
            rid = $(this).attr("data-id");
        });

        $("#trash").on('click', function() {
            if (rid) {
                $.ajax({
                    type: "GET",
                    url: "ajax/delete-rent.php?rid=" + rid,
                    cache: false,
                    success: function(response) {
                        if (response == 0) {
                            $('html, body').animate({
                                scrollTop: 0
                            }, 'slow');
                            $("#confirm").css("display", "block");
                            setTimeout(function() {
                                window.location = 'rent-status-list.php';
                            }, 3000);
                        }
                    }
                });
            }
        });
    });
    </script>
</body>

</html>