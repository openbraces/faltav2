<?php
include("application-top.php");	

$expTime = "";
$currentTime = "";

if(isset($_GET["id"]) && isset($_GET["token"])) {
  
  $uid = base64_decode($_GET["id"]);
  $token = $_GET["token"];

  $sql = "select * from admin_user where passtoken = '$token' and id = '$uid'";
  $res = mysqli_query($con, $sql);
  $numrow = mysqli_num_rows($res);
  $row = mysqli_fetch_array($res);
  date_default_timezone_set("Asia/Calcutta");  
  if($numrow > 0) {
   $expTime = $row["link_expiryTime"];
  } else {
  $expTime = "";
  }
  $currentTime = date("Y-m-d H:i:s");

if($expTime >= $currentTime) 
{	
if(isset($_POST["submit"]))
{
   $current_password = mysqli_real_escape_string($con, sha1(sha1(sha1($_POST["current_password"]))));  
   
   $chsql = "select * from admin_user where id = '".$uid."' and passtoken = '".$token."'";
   $chres = mysqli_query($con, $chsql);
   $chrow = mysqli_fetch_array($chres);
   $curPwd = $chrow["password"];

   if($current_password === $curPwd)
   {
   $password = mysqli_real_escape_string($con, sha1(sha1(sha1($_POST["password"]))));
   
   $sql = "update admin_user set password = '$password' where id = '".$uid."' and passtoken = '".$token."'";
   $res = mysqli_query($con, $sql) or die("Query Fail");
   
   
   if($res)
   {
    $msg = "Password changed successfully!";
	header("location:reset-password.php?msg=".$msg);
	exit();
   } else {
    die("Sorry...Your link has expired. Please contact the administrator, or initiate the password recovery process.");
   }
  } else {
     die("Your current password is invalid, please check.");
  }
}
} 
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>FALTA SEZ - Reset Password</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> -->

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary">
    <div class="container">
        <div class="card o-hidden border-0 shadow-lg extra-top-margin">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-6 d-none d-lg-block bg-resetpwd-image"></div>
                    <div class="col-lg-6">
                        <?php
                          if($expTime >= $currentTime) {
                        ?>
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Reset Password</h1>
                            </div>
                            <form class="user" method="POST" action="" onsubmit="return validation();">
                                <div class="form-group">
                                    <input type="password" name="current_password" required id="current_password"
                                        class="form-control form-control-user" placeholder="Current Password">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" required id="password"
                                        class="form-control form-control-user" placeholder="New Password">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="con_password" required id="con_password"
                                        class="form-control form-control-user" placeholder="Confirm Password">
                                </div>

                                <div class="form-group">
                                    <button type="submit" name="submit"
                                        class="btn btn-success btn-user btn-block">Change
                                        Password</button>
                                </div>
                            </form>
                            <hr>
                            <?php 
                            if(isset($_GET["msg"]))
                            {
                            ?>   
                                <div class="alert alert-success text-center alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                   <strong> <i class="fa fa-check" style="float: left; padding-top: 5px;"></i></strong>  
                                   <?=$_GET["msg"]?>
                                </div>
                             <?php
                            }
                            ?>
                            <div class="text-center">
                                <a class="small" href="index.php">Already have an account? Login!</a>
                            </div>
                        </div>
                        <?php } else { ?>
                            <div class="p-5" style="min-height:400px;">
                            <div class="text-center">
                                <h4 class="text-gray-900 mb-4 h4-line">Sorry, the link you are trying to access has <b>expired!</b> Please contact the administrator, or initiate the password recovery process again. <p>Thank you!<p></h4>
                            </div>
                        </div>
                       <?php } ?>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <?php include("includes/common-js.php"); ?>


    <script>
    function validation() {
        var password = $("#password").val();
        var confirm_password = $("#con_password").val();

        if (password !== confirm_password) {
            alert("Password does not match !");
            return false;
        }
        return true;
    };
    </script>
</body>

</html>
