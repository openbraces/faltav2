<?php
include "application-top.php";

if (!isset($_SESSION["email"])) {
    header("location:index.php");
    exit();
}

$sql = "select * from fsez_exports where export_id=" . $_GET["export_id"];
$res = mysqli_query($con, $sql);

if ($res) {
    $row = mysqli_fetch_array($res);
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>FALTA SEZ - Export Data Update</title>
    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> -->
    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.css" rel="stylesheet">
</head>

<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <?php include "includes/sidebar.php";?>
        <!-- End of Sidebar -->
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
                <?php include "includes/header.php";?>
                <!-- End of Topbar -->
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Outer Row -->
                    <div class="nav-container">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i></a>
                            </li>
                            <li class="breadcrumb-item active">Edit Export Data</li>
                            <li class="ml-auto"><a href="export-list.php" class="btn btn-sm btn-success add-btn"> <i
                                        class="fa fa-list" aria-hidden="true"></i> View Export Data</a></li>
                        </ol>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-xl-10 col-lg-12 col-md-9">
                            <div class="card o-hidden border-0 shadow-lg my-5">
                                <div class="card-body p-0">
                                    <!-- Nested Row within Card Body -->
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="p-5">
                                                <div class="text-center">
                                                    <h1 class="h4 text-gray-900 mb-4">Update Export Data</h1>
                                                </div>
                                                <form class="user" id="uexportForm" enctype="multipart/form-data">
                                                    <div class="form-group">
                                                        <input type="text" required name="export_title"
                                                            class="form-control form-control-user"
                                                            value="<?php echo $row["export_title"]; ?>"
                                                            id="export_title" placeholder="Enter The Title">
                                                    </div>

                                                    <div class="form-group">
                                                        <input type="text" required name="date_of_issue"
                                                            class="form-control form-control-user" value="<?php echo $row["date_of_issue"]; ?>"
                                                            id="date_of_issue" autocomplete="off" placeholder="Date Of Issue">
                                                    </div>

                                                    <input type="hidden" name="csrf_token"
                                                        value="<?php echo $_SESSION['csrf_token']; ?>" />
                                                    <div class="form-group">
                                                        <iframe
                                                            src="upload_export_documents/<?php echo $row["export_file_name"]; ?>"
                                                            width="100%" height="400px"
                                                            alt="<?php echo $row["export_title"]; ?>"></iframe>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="custom-file">
                                                            <input type="file" name="export_file_name"
                                                                class="custom-file-input form-control-user"
                                                                accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,.pdf,.doc,.docx,image/*"
                                                                onchange="validateFile(id)" id="export_file_name">
                                                            <label class="custom-file-label" for="customFile">Choose
                                                                file</label>
                                                        </div>
                                                    </div>
                                                    <input type="submit" class="btn btn-primary btn-user btn-block"
                                                        value="Update Export Data">
                                                </form>
                                                <hr>
                                                <p>
                                                  <div class="errcode text-danger mb-2"></div>
                                                  <div class="errcopy text-danger mb-2"></div>
                                                </p>
                                                <div class="alert alert-success alert-dismissible" id="exSuccess">
                                                    <a href="#" class="close" data-dismiss="alert"
                                                        aria-label="close">&times;</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <!-- End of Main Content -->
            <!-- Footer -->
            <?php include "includes/footer.php";?>
            <!-- End of Footer -->
        </div>
        <!-- End of Content Wrapper -->
    </div>
    <!-- End of Page Wrapper -->
    <!-- Bootstrap and core JavaScript-->
    <?php include "includes/common-js.php";?>

    <script>

    /* Update Export */

    jQuery(document).ready(function($) {

        $("#exSuccess").hide();

        $("#uexportForm").on("submit", function(e) {
            e.preventDefault();

            var export_id = <?php echo $_GET["export_id"]; ?>;
            var export_title = $("#export_title").val();
            var date_of_issue = $("#date_of_issue").val();

            var formData = new FormData($('#uexportForm')[0]);
            formData.append("export_file_name", $('input[type=file]')[0].files[0]);
            formData.append("export_title", export_title);
            formData.append("date_of_issue", date_of_issue);
            formData.append("export_id", export_id);


            $.ajax({
                type: "POST",
                url: "ajax/update-export.php?export_id=" + export_id,
                data: formData,
                dataType: "html",
                cache: false,
                contentType: false,
                processData: false,
                success: function(response) {
                    if (response == 0) {
                        uexportForm.reset();
                        $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                        $("#exSuccess").append("Export Data Updated Succesfully...").show();
                        setTimeout(function() {
                            location.href = "export-list.php"
                        }, 3000);
                    } else if (response == 3) {
                       uexportForm.reset();
                       $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                       alert("Invalid file type, please choose another file.");
                    } else {
                        uexportForm.reset();
                        $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                        alert("Something went wrong");
                    }
                }
            });

        });
    });
    </script>
</body>

</html>