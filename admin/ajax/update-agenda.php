<?php
include "../application-top.php";

if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) {

    $agenda_id = $_GET["agenda_id"];

    $uasql = "select * from  fsez_uac_agendas where agenda_id =" . $_GET["agenda_id"];
    $uares = mysqli_query($con, $uasql);

    if ($uares) {
        $uarow = mysqli_fetch_array($uares);
    }

    $uac_agenda_title = mysqli_real_escape_string($con, check_sanity($_POST["uac_agenda_title"]));
    $date_of_issue = mysqli_real_escape_string($con, check_sanity($_POST["date_of_issue"]));

    if (!empty($_FILES["uac_agenda_file"]["name"])) {
        $uac_agenda_file = time() . $_FILES["uac_agenda_file"]["name"];

        $allowedMimeTypes = array(
            'application/msword',
            'application/pdf',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.ms-excel',
            'application/x-pdf',
            'application/vnd.pdf',
            'text/pdf',
            'image/jpg',
            'image/png',
            'image/jpeg',
        );

        $mimeType = mime_content_type($_FILES["uac_agenda_file"]["tmp_name"]);

        if (!in_array($mimeType, $allowedMimeTypes)) {
            die('3');
        } else {
            move_uploaded_file($_FILES["uac_agenda_file"]["tmp_name"], "../upload_uac_agenda_documents/" . $uac_agenda_file);
        }
    } else {
        $uac_agenda_file = $uarow["uac_agenda_file"];
    }

    $sql = "update fsez_uac_agendas set uac_agenda_title = '$uac_agenda_title' , date_of_issue = '$date_of_issue', uac_agenda_file = '$uac_agenda_file' where agenda_id =" . $uarow["agenda_id"];
    $res = mysqli_query($con, $sql);

    if ($res) {
        echo "0";
    } else {
        echo "1";
    }
} else {
    echo "Invalid Request...";
}
