<?php
include "../application-top.php";

if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) {

    $auth_id = $_GET["auth_id"];

    $amsql = "select * from fsez_auth_meetings where auth_id =" . $_GET["auth_id"];
    $amres = mysqli_query($con, $amsql);

    if ($amres) {
        $amrow = mysqli_fetch_array($amres);
    }

    $auth_meeting_title = mysqli_real_escape_string($con, check_sanity($_POST["auth_meeting_title"]));
    $date_of_issue = mysqli_real_escape_string($con, check_sanity($_POST["date_of_issue"]));

    if (!empty($_FILES["auth_meeting_file"]["name"])) {
        $auth_meeting_file = time() . $_FILES["auth_meeting_file"]["name"];

        $allowedMimeTypes = array(
            'application/msword',
            'application/pdf',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.ms-excel',
            'application/x-pdf',
            'application/vnd.pdf',
            'text/pdf',
            'image/jpg',
            'image/png',
            'image/jpeg',
        );

        $mimeType = mime_content_type($_FILES["auth_meeting_file"]["tmp_name"]);

        if (!in_array($mimeType, $allowedMimeTypes)) {
            die('3');
        } else {
            move_uploaded_file($_FILES["auth_meeting_file"]["tmp_name"], "../upload_authority_meeting_documents/" . $auth_meeting_file);
        }
    } else {
        $auth_meeting_file = $amrow["auth_meeting_file"];
    }

    $sql = "update fsez_auth_meetings set auth_meeting_title = '$auth_meeting_title' , date_of_issue = '$date_of_issue', auth_meeting_file = '$auth_meeting_file' where auth_id =" . $amrow["auth_id"];
    $res = mysqli_query($con, $sql);

    if ($res) {
        echo "0";
    } else {
        echo "1";
    }
} else {
    echo "Invalid Request...";
}
