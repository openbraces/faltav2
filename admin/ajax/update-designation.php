<?php
include("../application-top.php");
 
 if(isset($_POST['csrf_token']))
 {
     
	 $designation_name = mysqli_real_escape_string($con, check_sanity($_POST["designation_name"]));
	 $designation_id =  mysqli_real_escape_string($con, check_sanity($_POST["designation_id"]));
	 
	 $desql = "update fsez_designations set designation_name = '$designation_name' where designation_id= '$designation_id'";
	 $deres = mysqli_query($con, $desql); 
	 
	 if($deres)
	 {   
		 echo "0";
		 exit();
	 } else {
	     echo "1";
	 }
 }
 
 if(isset($_GET["designation_id"])) {
  $sql = "select * from  fsez_designations where designation_id=".$_GET["designation_id"];
  $res = mysqli_query($con, $sql);
  $row = mysqli_fetch_object($res);
?>
      <form method="post" id="eDegForm">
      <!-- Modal body -->
      <div class="modal-body">
			<div class="form-group row">
			  <label for="designation_name" class="col-sm-4 col-form-label">Designation Name</label>
			  <div class="col-sm-8">
				<input type="text" class="form-control" id="designation_name" required placeholder="Designation Name" name="designation_name" value="<?php echo $row->designation_name?>">
			  </div>  		
			  <input type="hidden" name="csrf_token" value="<?php echo $_GET["ctoken"]; ?>" />
			</div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">	   
	    <input type="submit" class="btn btn-primary" name="submit" value="Save">
      </div>
     </form>
<?php
}
?>
<script type="text/javascript">
 $("#eDegForm").on("submit", function(e) {
    e.preventDefault();
    
    var designation_id = '<?php echo $_GET["designation_id"]; ?>';
    var msg = "Designation updated successfully !!!";
    
    var formData = new FormData($('#eDegForm')[0]);
    formData.append("designation_id", designation_id);
    
		  $.ajax({
			 type : "POST",
             url  : "ajax/update-designation.php",
             data: formData,
             dataType: "html",
             processData: false,
             contentType: false,
             cache: false,
            success: function(response) {
                if (response == 0) {
                    eDegForm.reset();
                    window.location.href = "designation-list.php?msg="+msg;
                }  else {
                    eDegForm.reset();
                    alert("Something went wrong");
                }
            }				
		  });
	  });
</script>     