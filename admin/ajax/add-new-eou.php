<?php

include "../application-top.php";

if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) {

    $eou_meeting_title = mysqli_real_escape_string($con, check_sanity($_POST["eou_meeting_title"]));
    $date_of_issue = mysqli_real_escape_string($con, check_sanity($_POST["date_of_issue"]));

    if (!empty($_FILES["eou_meeting_file"]["name"])) {
        $eou_meeting_file = time() . $_FILES["eou_meeting_file"]["name"];

        $allowedMimeTypes = array(
            'application/msword',
            'application/pdf',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.ms-excel',
            'application/x-pdf',
            'application/vnd.pdf',
            'text/pdf',
            'image/jpg',
            'image/png',
            'image/jpeg',
        );

        $mimeType = mime_content_type($_FILES["eou_meeting_file"]["tmp_name"]);

        if (!in_array($mimeType, $allowedMimeTypes)) {
            die('3');
        } else {
            move_uploaded_file($_FILES["eou_meeting_file"]["tmp_name"], "../upload_eou_meeting_documents/" . $eou_meeting_file);
        }
    } else {
        $eou_meeting_file = "noimage.gif";
    }

    $sql = "insert into fsez_eou_meetings (eou_meeting_title, date_of_issue, eou_meeting_file, added_on) values ('$eou_meeting_title', '$date_of_issue', '$eou_meeting_file', NOW())";
    $res = mysqli_query($con, $sql);

    if ($res) {
        echo "0";
    } else {
        echo "1";
    }
} else {
    echo "Invalid Request...";
}
