<?php
include "../application-top.php";

if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) {

    $vacancy_id = $_GET["vacancy_id"];

    $vnsql = "select * from fsez_vacancies where vacancy_id =" . $_GET["vacancy_id"];
    $vnres = mysqli_query($con, $vnsql);

    if ($vnres) {
        $vnrow = mysqli_fetch_array($vnres);
    }

    $vacancy_name = mysqli_real_escape_string($con, check_sanity($_POST["vacancy_name"]));
    $date_of_issue = mysqli_real_escape_string($con, check_sanity($_POST["date_of_issue"]));

    if (!empty($_FILES["vacancy_file_name"]["name"])) {
        $vacancy_file_name = time() . $_FILES["vacancy_file_name"]["name"];

        $allowedMimeTypes = array(
            'application/msword',
            'application/pdf',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.ms-excel',
            'application/x-pdf',
            'application/vnd.pdf',
            'text/pdf',
            'image/jpg',
            'image/png',
            'image/jpeg',
        );

        $mimeType = mime_content_type($_FILES["vacancy_file_name"]["tmp_name"]);

        if (!in_array($mimeType, $allowedMimeTypes)) {
            die('3');
        } else {
            move_uploaded_file($_FILES["vacancy_file_name"]["tmp_name"], "../upload_vacancy_documents/" . $vacancy_file_name);
        }
    } else {
        $vacancy_file_name = $vnrow["vacancy_file_name"];
    }

    $sql = "update fsez_vacancies set vacancy_name = '$vacancy_name' , date_of_issue = '$date_of_issue' , vacancy_file_name = '$vacancy_file_name' where vacancy_id =" . $_GET["vacancy_id"];
    $res = mysqli_query($con, $sql);

    if ($res) {
        echo "0";
    } else {
        echo "1";
    }
} else {
    echo "Invalid Request...";
}
