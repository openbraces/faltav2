<?php
include "../application-top.php";

if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) {

    $tender_id = $_GET["tender_id"];

    $utsql = "select * from fsez_tenders where tender_id =" . $_GET["tender_id"];
    $utres = mysqli_query($con, $utsql);

    if ($utres) {
        $utrow = mysqli_fetch_array($utres);
    }

    $tender_name = mysqli_real_escape_string($con, check_sanity($_POST["tender_name"]));
    $date_of_issue = mysqli_real_escape_string($con, check_sanity($_POST["date_of_issue"]));

    if (!empty($_FILES["tender_file_name"]["name"])) {
        $tender_file_name = time() . $_FILES["tender_file_name"]["name"];

        $allowedMimeTypes = array(
            'application/msword',
            'application/pdf',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.ms-excel',
            'application/x-pdf',
            'application/vnd.pdf',
            'text/pdf',
            'image/jpg',
            'image/png',
            'image/jpeg',
        );

        $mimeType = mime_content_type($_FILES["tender_file_name"]["tmp_name"]);

        if (!in_array($mimeType, $allowedMimeTypes)) {
            die('3');
        } else {
            move_uploaded_file($_FILES["tender_file_name"]["tmp_name"], "../upload_tender_documents/" . $tender_file_name);
        }
    } else {
        $tender_file_name = $utrow["tender_file_name"];
    }

    $sql = "update fsez_tenders set tender_name = '$tender_name' , date_of_issue = '$date_of_issue' , tender_file_name = '$tender_file_name' where tender_id =" . $_GET["tender_id"];
    $res = mysqli_query($con, $sql);

    if ($res) {
        echo "0";
    } else {
        echo "1";
    }
} else {
    echo "Invalid Request...";
}
