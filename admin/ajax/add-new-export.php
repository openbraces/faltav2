<?php

include "../application-top.php";

if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) {

    $export_title = mysqli_real_escape_string($con, check_sanity($_POST["export_title"]));
    $date_of_issue = mysqli_real_escape_string($con, check_sanity($_POST["date_of_issue"]));

    if (!empty($_FILES["export_file_name"]["name"])) {
        $export_file_name = time() . $_FILES["export_file_name"]["name"];
        
        $allowedMimeTypes = array(
            'application/msword',
            'application/pdf',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.ms-excel',
            'application/x-pdf',
            'application/vnd.pdf',
            'text/pdf',
            'image/jpg',
            'image/png',
            'image/jpeg',
        );

        $mimeType = mime_content_type($_FILES["export_file_name"]["tmp_name"]);

        if (!in_array($mimeType, $allowedMimeTypes)) {
            die('3');
        } else {
            move_uploaded_file($_FILES["export_file_name"]["tmp_name"], "../upload_export_documents/" . $export_file_name);
        }
    } else {
        $export_file_name = "noimage.gif";
    }

    $sql = "insert into fsez_exports (export_title, date_of_issue, export_file_name, added_on) values ('$export_title', '$date_of_issue', '$export_file_name', NOW())";
    $res = mysqli_query($con, $sql);

    if ($res) {
        echo "0";
    } else {
        echo "1";
    }
} else {
    echo "Invalid Request...";
}
