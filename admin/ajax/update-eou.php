<?php
include "../application-top.php";

if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) {

    $eou_id = $_GET["eou_id"];

    $uasql = "select * from  fsez_eou_meetings where eou_id =" . $_GET["eou_id"];
    $uares = mysqli_query($con, $uasql);

    if ($uares) {
        $uarow = mysqli_fetch_array($uares);
    }

    $eou_meeting_title = mysqli_real_escape_string($con, check_sanity($_POST["eou_meeting_title"]));
    $date_of_issue = mysqli_real_escape_string($con, check_sanity($_POST["date_of_issue"]));

    if (!empty($_FILES["eou_meeting_file"]["name"])) {
        $eou_meeting_file = time() . $_FILES["eou_meeting_file"]["name"];

        $allowedMimeTypes = array(
            'application/msword',
            'application/pdf',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.ms-excel',
            'application/x-pdf',
            'application/vnd.pdf',
            'text/pdf',
            'image/jpg',
            'image/png',
            'image/jpeg',
        );

        $mimeType = mime_content_type($_FILES["eou_meeting_file"]["tmp_name"]);

        if (!in_array($mimeType, $allowedMimeTypes)) {
            die('3');
        } else {
            move_uploaded_file($_FILES["eou_meeting_file"]["tmp_name"], "../upload_eou_meeting_documents/" . $eou_meeting_file);
        }
    } else {
        $eou_meeting_file = $uarow["eou_meeting_file"];
    }

    $sql = "update fsez_eou_meetings set eou_meeting_title = '$eou_meeting_title' , date_of_issue = '$date_of_issue' , eou_meeting_file = '$eou_meeting_file' where eou_id =" . $uarow["eou_id"];
    $res = mysqli_query($con, $sql);

    if ($res) {
        echo "0";
    } else {
        echo "1";
    }
} else {
    echo "Invalid Request...";
}
