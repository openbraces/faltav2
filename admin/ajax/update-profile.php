<?php
include "../application-top.php";

if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) {

    $uid = $_GET["uid"];

    $pfsql = "select * from admin_user where id =" . $_GET["uid"];
    $pfres = mysqli_query($con, $pfsql);

    if ($pfres) {
        $pfrow = mysqli_fetch_array($pfres);
    }

    $username = mysqli_real_escape_string($con, check_sanity($_POST["username"]));
    $email = mysqli_real_escape_string($con, check_sanity($_POST["email"]));
    $contact_no = mysqli_real_escape_string($con, check_sanity($_POST["contact_no"]));

    if (!empty($_FILES["profile_image"]["name"])) {
        $profile_image = time() . $_FILES["profile_image"]["name"];

        $allowedMimeTypes = array(
            'image/jpg',
            'image/png',
            'image/jpeg',
        );

        $mimeType = mime_content_type($_FILES["profile_image"]["tmp_name"]);

        if (!in_array($mimeType, $allowedMimeTypes)) {
            die('3');
        } else {
            move_uploaded_file($_FILES["profile_image"]["tmp_name"], "../upload_images/" . $profile_image);
        }
    } else {
        $profile_image = $pfrow["profile_image"];
    }

    $sql = "update admin_user set username = '$username' , email = '$email' , contact_no = '$contact_no' , profile_image = '$profile_image'  where id =" . $pfrow["id"];
    $res = mysqli_query($con, $sql);

    if ($res) {
        echo "0";
    } else {
        echo "1";
    }
} else {
    echo "Invalid Request...";
}
