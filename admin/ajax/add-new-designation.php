<?php

include "../application-top.php";

if ($_SESSION["csrf_token"]) {

    $designation_name = mysqli_real_escape_string($con, check_sanity($_POST["designation_name"]));
    $sequence = mysqli_real_escape_string($con, check_sanity($_POST["sequence"]));

    $sql = "insert into fsez_designations (designation_name, sequence, added_on) values ('$designation_name', '$sequence', NOW())";
    $res = mysqli_query($con, $sql);

    if ($res) {
        echo "0";
    } else {
        echo "1";
    }
} else {
    echo "Invalid Request...";
}
