<?php
include "../application-top.php";

$ctid = $_GET["ctid"];

$ctsql = "select * from  fsez_gallery_categories where category_id = '$ctid'";
$ctres = mysqli_query($con, $ctsql);
$ctrow = mysqli_fetch_array($ctres);

if (isset($_POST["submit"])) {
    $category_name = mysqli_real_escape_string($con, check_sanity($_POST["category_name"]));

    $sql = "update fsez_gallery_categories set category_name = '$category_name' where category_id=" . $_GET["category_id"];
    $res = mysqli_query($con, $sql);
    if ($res) {
        $msg = "Category Updated Successfully !!!";
        header("location:../category-list.php?msg=" . $msg);
        exit();
    }
}
?>

<div class="modal-body">
    <form class="form-inline" method="post" action="ajax/update-category.php?category_id=<?=$ctrow["category_id"]?>">
        <div class="form-group mx-sm-3 mb-2 col-6">
            <label for="category_name" class="sr-only">Category Name:</label>
            <input type="text" required="" class="form-control" id="category_name" name="category_name"
                placeholder="Enter Category name..." value="<?php echo $ctrow["category_name"]; ?>">
        </div>
        <input type="submit" class="btn btn-info mb-2" name="submit" value="Update Category">
    </form>
</div>

