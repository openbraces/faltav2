<?php

include "../application-top.php";

if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) {
	
    $contact_name = mysqli_real_escape_string($con, check_sanity($_POST["contact_name"]));
    $designation = mysqli_real_escape_string($con, check_sanity($_POST["designation"]));
    $email_id = mysqli_real_escape_string($con, check_sanity($_POST["email_id"]));
    $contact_no = mysqli_real_escape_string($con, check_sanity($_POST["contact_no"]));  

    $sql = "insert into fsez_contacts (contact_name, designation, email_id, contact_no, added_on) values ('$contact_name', '$designation', '$email_id', '$contact_no', NOW())";
    $res = mysqli_query($con, $sql);

    if ($res) {
        echo "0";
    } else {
        echo "1";
    }
} else {
    echo "Invalid Request...";
}
