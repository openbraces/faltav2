<?php

include "../application-top.php";

if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) {

    $circular_name = mysqli_real_escape_string($con, check_sanity($_POST["circular_name"]));
    $date_of_issue = mysqli_real_escape_string($con, check_sanity($_POST["date_of_issue"]));
    
    if (!empty($_FILES["circular_file_name"]["name"])) {
        $circular_file_name = time() . $_FILES["circular_file_name"]["name"];

        $allowedMimeTypes = array(
            'application/msword',
            'application/pdf',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.ms-excel',
            'application/x-pdf',
            'application/vnd.pdf',
            'text/pdf',
            'image/jpg',
            'image/png',
            'image/jpeg',
        );

        $mimeType = mime_content_type($_FILES["circular_file_name"]["tmp_name"]);

        if (!in_array($mimeType, $allowedMimeTypes)) {
            die('3');
        } else {
            move_uploaded_file($_FILES["circular_file_name"]["tmp_name"], "../upload_circular_documents/" . $circular_file_name);
        }
    } else {
        $circular_file_name = "noimage.gif";
    }

    $sql = "insert into fsez_circulars (circular_name, circular_file_name, date_of_issue, added_on) values ('$circular_name', '$circular_file_name', '$date_of_issue', NOW())";
    $res = mysqli_query($con, $sql);

    if ($res) {
        echo "0";
    } else {
        echo "1";
    }
} else {
    echo "Invalid Request...";
}
