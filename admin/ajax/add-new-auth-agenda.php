<?php

include "../application-top.php";

if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) {

    $auth_agenda_title = mysqli_real_escape_string($con, check_sanity($_POST["auth_agenda_title"]));
    $date_of_issue = mysqli_real_escape_string($con, check_sanity($_POST["date_of_issue"]));

    if (!empty($_FILES["auth_agenda_file"]["name"])) {
        $auth_agenda_file = time() . $_FILES["auth_agenda_file"]["name"];

        $allowedMimeTypes = array(
            'application/msword',
            'application/pdf',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.ms-excel',
            'application/x-pdf',
            'application/vnd.pdf',
            'text/pdf',
            'image/jpg',
            'image/png',
            'image/jpeg',
        );

        $mimeType = mime_content_type($_FILES["auth_agenda_file"]["tmp_name"]);

        if (!in_array($mimeType, $allowedMimeTypes)) {
            die('3');
        } else {
            move_uploaded_file($_FILES["auth_agenda_file"]["tmp_name"], "../upload_authority_agenda_documents/" . $auth_agenda_file);
        }
    } else {
        $auth_agenda_file = "noimage.gif";
    }

    $sql = "insert into fsez_auth_agendas (auth_agenda_title, date_of_issue, auth_agenda_file, added_on) values ('$auth_agenda_title', '$date_of_issue', '$auth_agenda_file', NOW())";
    $res = mysqli_query($con, $sql);

    if ($res) {
        echo "0";
    } else {
        echo "1";
    }
} else {
    echo "Invalid Request...";
}
