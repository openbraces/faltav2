<?php

include "../application-top.php";

if ($_SESSION["csrf_token"]) {

    $category_name = mysqli_real_escape_string($con, check_sanity($_POST["category_name"]));

    $sql = "insert into fsez_gallery_categories (category_name, added_on) values ('$category_name', NOW())";
    $res = mysqli_query($con, $sql);

    if ($res) {
        echo "0";
    } else {
        echo "1";
    }
} else {
    echo "Invalid Request...";
}
