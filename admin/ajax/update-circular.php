<?php
include "../application-top.php";

if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) {

    $circular_id = $_GET["circular_id"];

    $ucsql = "select * from fsez_circulars where circular_id =" . $_GET["circular_id"];
    $ucres = mysqli_query($con, $ucsql);

    if ($ucres) {
        $ucrow = mysqli_fetch_array($ucres);
    }

    $circular_name = mysqli_real_escape_string($con, check_sanity($_POST["circular_name"]));
    $date_of_issue = mysqli_real_escape_string($con, check_sanity($_POST["date_of_issue"]));

    if (!empty($_FILES["circular_file_name"]["name"])) {
        $circular_file_name = time() . $_FILES["circular_file_name"]["name"];

        $allowedMimeTypes = array(
            'application/msword',
            'application/pdf',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.ms-excel',
            'application/x-pdf',
            'application/vnd.pdf',
            'text/pdf',
            'image/jpg',
            'image/png',
            'image/jpeg',
        );

        $mimeType = mime_content_type($_FILES["circular_file_name"]["tmp_name"]);

        if (!in_array($mimeType, $allowedMimeTypes)) {
            die('3');
        } else {
            move_uploaded_file($_FILES["circular_file_name"]["tmp_name"], "../upload_circular_documents/" . $circular_file_name);
        }
    } else {
        $circular_file_name = $ucrow["circular_file_name"];
    }

    $sql = "update fsez_circulars set circular_name = '$circular_name' , circular_file_name = '$circular_file_name' , date_of_issue = '$date_of_issue' where circular_id =" . $_GET["circular_id"];
    $res = mysqli_query($con, $sql);

    if ($res) {
        echo "0";
    } else {
        echo "1";
    }
} else {
    echo "Invalid Request...";
}
