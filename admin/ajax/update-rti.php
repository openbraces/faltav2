<?php
include "../application-top.php";

if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) {

    $urti_id = $_GET["rti_id"];

    $ursql = "select * from fsez_rti where rti_id =" . $_GET["rti_id"];
    $urres = mysqli_query($con, $ursql);

    if ($urres) {
        $urrow = mysqli_fetch_array($urres);
    }

    $rti_title = mysqli_real_escape_string($con, check_sanity($_POST["rti_title"]));

    if (!empty($_FILES["rti_file_name"]["name"])) {
        $rti_file_name = time() . $_FILES["rti_file_name"]["name"];

        $allowedMimeTypes = array(
            'application/msword',
            'application/pdf',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.ms-excel',
            'application/x-pdf',
            'application/vnd.pdf',
            'text/pdf',
            'image/jpg',
            'image/png',
            'image/jpeg',
        );

        $mimeType = mime_content_type($_FILES["rti_file_name"]["tmp_name"]);

        if (!in_array($mimeType, $allowedMimeTypes)) {
            die('3');
        } else {
            move_uploaded_file($_FILES["rti_file_name"]["tmp_name"], "../upload_rti_documents/" . $rti_file_name);
        }
    } else {
        $rti_file_name = $urrow["rti_file_name"];
    }

    $sql = "update fsez_rti set rti_title = '$rti_title' , rti_file_name = '$rti_file_name' where rti_id =" . $urrow["rti_id"];
    $res = mysqli_query($con, $sql);

    if ($res) {
        echo "0";
    } else {
        echo "1";
    }
} else {
    echo "Invalid Request...";
}
