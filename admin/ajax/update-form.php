<?php
include "../application-top.php";

if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) {

    $form_id = $_GET["form_id"];

    $ufsql = "select * from fsez_forms where form_id =" . $_GET["form_id"];
    $ufres = mysqli_query($con, $ufsql);

    if ($ufres) {
        $ufrow = mysqli_fetch_array($ufres);
    }

    $form_name = mysqli_real_escape_string($con, check_sanity($_POST["form_name"]));
    $date_of_issue = mysqli_real_escape_string($con, check_sanity($_POST["date_of_issue"]));

    if (!empty($_FILES["form_file_name"]["name"])) {
        $form_file_name = time() . $_FILES["form_file_name"]["name"];

        $allowedMimeTypes = array(
            'application/msword',
            'application/pdf',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.ms-excel',
            'application/x-pdf',
            'application/vnd.pdf',
            'text/pdf',
            'image/jpg',
            'image/png',
            'image/jpeg',
        );

        $mimeType = mime_content_type($_FILES["form_file_name"]["tmp_name"]);

        if (!in_array($mimeType, $allowedMimeTypes)) {
            die('3');
        } else {
            move_uploaded_file($_FILES["form_file_name"]["tmp_name"], "../upload_form_documents/" . $form_file_name);
        }
    } else {
        $form_file_name = $ufrow["form_file_name"];
    }

    $sql = "update fsez_forms set form_name = '$form_name' , date_of_issue = '$date_of_issue' , form_file_name = '$form_file_name' where form_id =" . $_GET["form_id"];
    $res = mysqli_query($con, $sql);

    if ($res) {
        echo "0";
    } else {
        echo "1";
    }
} else {
    echo "Invalid Request...";
}
