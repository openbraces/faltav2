<?php
include "../application-top.php";

if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) {

    $rent_status_id = $_GET["rent_status_id"];

    $ursql = "select * from fsez_rent_status where rent_status_id =" . $_GET["rent_status_id"];
    $urres = mysqli_query($con, $ursql);

    if ($urres) {
        $urrow = mysqli_fetch_array($urres);
    }
    
    $company = mysqli_real_escape_string($con, check_sanity($_POST["company"]));
    $rent_paid_of_year = mysqli_real_escape_string($con, check_sanity($_POST["rent_paid_of_year"]));
    $rent_paid_of_quarter = mysqli_real_escape_string($con, check_sanity($_POST["rent_paid_of_quarter"]));
    $rent_amount = mysqli_real_escape_string($con, check_sanity($_POST["rent_amount"]));

    if (!empty($_FILES["rent_status_file_name"]["name"])) {
        $rent_status_file_name = time() . $_FILES["rent_status_file_name"]["name"] . $rent_paid_of_year . 'Y' .$rent_paid_of_quarter . 'Q';

        $allowedMimeTypes = array(
            'application/msword',
            'application/pdf',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.ms-excel',
            'application/x-pdf',
            'application/vnd.pdf',
            'text/pdf'
        );

        $mimeType = mime_content_type($_FILES["rent_status_file_name"]["tmp_name"]);

        if (!in_array($mimeType, $allowedMimeTypes)) {
            die('3');
        } else {
            move_uploaded_file($_FILES["rent_status_file_name"]["tmp_name"], "../upload_rent_documents/" . $rent_status_file_name);
        }
    } else {
        $rent_status_file_name = $urrow["rent_status_file_name"];
    }

    $sql = "update fsez_rent_status set company = '$company' , rent_paid_of_year = '$rent_paid_of_year' , rent_paid_of_quarter = '$rent_paid_of_quarter' , rent_amount = '$rent_amount', rent_status_file_name = '$rent_status_file_name' where rent_status_id =" . $_GET["rent_status_id"];
    $res = mysqli_query($con, $sql);

    if ($res) {
        echo "0";
    } else {
        echo "1";
    }
} else {
    echo "Invalid Request...";
}
