<?php
include("../application-top.php");

if(isset($_POST['csrf_token']))
 {
	 $company_name = mysqli_real_escape_string($con, check_sanity($_POST["company_name"]));
	 $company_id =  mysqli_real_escape_string($con, check_sanity($_POST["company_id"]));
   $company_email =  mysqli_real_escape_string($con, check_sanity($_POST["company_email"]));
	 
	 $cesql = "update fsez_companies set company_name = '$company_name', company_email = '$company_email' where company_id= '$company_id'";
	 $ceres = mysqli_query($con, $cesql); 
	 
	 if($ceres)
	 {   
		 echo "0";
		 exit();
	 } else {
	     echo "1";
	 }
 }
 
 if(isset($_GET["company_id"])) {
   $sql = "select * from  fsez_companies where company_id=".$_GET["company_id"];
   $res = mysqli_query($con, $sql);
   $row = mysqli_fetch_object($res);
 ?>
      <form method="post" id="eComForm">
      <!-- Modal body -->
      <div class="modal-body">
			<div class="form-group row">
			  <label for="company_name" class="col-sm-4 col-form-label">Unit Name</label>
			    <div class="col-sm-8">
			    	<input type="text" class="form-control" id="company_name" required placeholder="Unit Name" name="company_name" value="<?php echo $row->company_name?>">
			    </div>  		
			   <input type="hidden" name="csrf_token" value="<?php echo $_GET["ctoken"]; ?>" />
			</div>
      <input type="hidden" name="company_id" value="<?php echo $_GET["company_id"]; ?>" />
      <div class="form-group row">
			  <label for="company_email" class="col-sm-4 col-form-label">Unit Email</label>
			   <div class="col-sm-8">
			    	<input type="text" class="form-control" id="company_email" required placeholder="Unit Email" name="company_email" value="<?php echo $row->company_email?>">
			   </div>  		
		  	</div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">	   
	    <input type="submit" class="btn btn-primary" name="submit" value="Save">
      </div>
     </form>
<?php
}
?>
<script>
 $("#eComForm").on("submit", function(e) {
    e.preventDefault();
    
    var msg = "Unit updated successfully !!!";
    
    var formData = new FormData($('#eComForm')[0]);

		  $.ajax({
			 type : "POST",
             url  : "ajax/update-company.php",
             data: formData,
             dataType: "html",
             processData: false,
             contentType: false,
             cache: false,
            success: function(response) {
                if (response == 0) {
                    window.location.href = "company-list.php?msg="+msg;
                }  else {
                    alert("Something went wrong");
                }
            }				
		  });
	  });
</script>     