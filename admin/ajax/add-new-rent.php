<?php

include "../application-top.php";

if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) {
    
    $company = mysqli_real_escape_string($con, check_sanity($_POST["company"]));
    $rent_paid_of_year = mysqli_real_escape_string($con, check_sanity($_POST["rent_paid_of_year"]));
    $rent_paid_of_quarter = mysqli_real_escape_string($con, check_sanity($_POST["rent_paid_of_quarter"]));
    $rent_amount = mysqli_real_escape_string($con, check_sanity($_POST["rent_amount"]));

    if (!empty($_FILES["rent_status_file_name"]["name"])) {
        $rent_status_file_name = time() . '-' . $rent_paid_of_year . 'Y-' .$rent_paid_of_quarter . 'Q-' . $_FILES["rent_status_file_name"]["name"];

        $allowedMimeTypes = array(
            'application/msword',
            'application/pdf',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.ms-excel',
            'application/x-pdf',
            'application/vnd.pdf',
            'text/pdf'
        );

        $mimeType = mime_content_type($_FILES["rent_status_file_name"]["tmp_name"]);

        if (!in_array($mimeType, $allowedMimeTypes)) {
            die('3');
        } else {
            move_uploaded_file($_FILES["rent_status_file_name"]["tmp_name"], "../upload_rent_documents/" . $rent_status_file_name);
        }
    } else {
        $rent_status_file_name = "noimage.gif";
    }
    
    $chksql = "select company, rent_paid_of_year, rent_paid_of_quarter from fsez_rent_status";
    $chkres = mysqli_query($con, $chksql);
    $chkrow = mysqli_fetch_array($chkres);
    
    if($chkrow["company"] == "$company" &&  $chkrow["rent_paid_of_year"] == "$rent_paid_of_year" && $chkrow["rent_paid_of_quarter"] == "$rent_paid_of_quarter") {
        die('4');
    }
    

    $sql = "insert into fsez_rent_status (company, rent_paid_of_year, rent_paid_of_quarter, rent_amount, rent_status_file_name, added_on) values ('$company', '$rent_paid_of_year', '$rent_paid_of_quarter', '$rent_amount', '$rent_status_file_name', NOW())";
    $res = mysqli_query($con, $sql);

    if ($res) {
        echo "0";
    } else {
        echo "1";
    }
} else {
    echo "Invalid Request...";
}
