<?php
include "../application-top.php";

if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) {

    $sid = $_GET["sid"];

    $gbsql = "select * from fsez_global_settings where setting_id =" . $_GET["sid"];
    $gbres = mysqli_query($con, $gbsql);

    if ($gbres) {
        $gbrow = mysqli_fetch_array($gbres);
    }

    $org_name = mysqli_real_escape_string($con, check_sanity($_POST["org_name"]));
    $org_info = mysqli_real_escape_string($con, check_sanity($_POST["org_info"]));
    $site_url = mysqli_real_escape_string($con, check_sanity($_POST["site_url"]));
    $admin_email = mysqli_real_escape_string($con, check_sanity($_POST["admin_email"]));
    $address = mysqli_real_escape_string($con, check_sanity($_POST["address"]));
    $contact_no = mysqli_real_escape_string($con, check_sanity($_POST["contact_no"]));
    $fb_link = mysqli_real_escape_string($con, check_sanity($_POST["fb_link"]));
    $tw_link = mysqli_real_escape_string($con, check_sanity($_POST["tw_link"]));
    $ln_link = mysqli_real_escape_string($con, check_sanity($_POST["ln_link"]));
    $is_maintenance = mysqli_real_escape_string($con, check_sanity($_POST["is_maintenance"]));

    if (!empty($_FILES["company_logo"]["name"])) {
        $company_logo = time() . $_FILES["company_logo"]["name"];

        $allowedMimeTypes = array(
            'image/jpg',
            'image/png',
            'image/jpeg',
        );

        $mimeType = mime_content_type($_FILES["company_logo"]["tmp_name"]);

        if (!in_array($mimeType, $allowedMimeTypes)) {
            die('3');
        } else {
            move_uploaded_file($_FILES["company_logo"]["tmp_name"], "../upload_images/" . $company_logo);
        }
    } else {
        $company_logo = $gbrow["company_logo"];
    }

    $sql = "update fsez_global_settings set org_name = '$org_name' , org_name = '$org_name' , org_info = '$org_info' , site_url = '$site_url' , admin_email = '$admin_email' , address = '$address' , contact_no = '$contact_no' , fb_link = '$fb_link' , tw_link = '$tw_link' , ln_link = '$ln_link' , company_logo = '$company_logo', is_maintenance = '$is_maintenance', updated_on = NOW() where setting_id =" . $gbrow["setting_id"];
    $res = mysqli_query($con, $sql);

    if ($res) {
        echo "0";
    } else {
        echo "1";
    }
} else {
    echo "Invalid Request...";
}
