<?php
include "../application-top.php";

if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) {

    $news_id = $_GET["news_id"];

    $unsql = "select * from fsez_news where news_id =" . $_GET["news_id"];
    $unres = mysqli_query($con, $unsql);

    if ($unres) {
        $unrow = mysqli_fetch_array($unres);
    }

    $news_description = mysqli_real_escape_string($con, check_sanity($_POST["news_description"]));
    $news_link = mysqli_real_escape_string($con, trim($_POST["news_link"]));

    if (!empty($_FILES["news_file_name"]["name"])) {
        $news_file_name = time() . $_FILES["news_file_name"]["name"];

        $allowedMimeTypes = array(
            'application/msword',
            'application/pdf',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.ms-excel',
            'application/x-pdf',
            'application/vnd.pdf',
            'text/pdf',
            'image/jpg',
            'image/png',
            'image/jpeg',
        );

        $mimeType = mime_content_type($_FILES["news_file_name"]["tmp_name"]);

        if (!in_array($mimeType, $allowedMimeTypes)) {
            die('3');
        } else {
            move_uploaded_file($_FILES["news_file_name"]["tmp_name"], "../upload_news_documents/" . $news_file_name);
        }
    } else {
        $news_file_name = $unrow["news_file_name"];
    }

    $sql = "update fsez_news set news_description = '$news_description' , news_link = '$news_link' , news_file_name = '$news_file_name' where news_id =" . $unrow["news_id"];
    $res = mysqli_query($con, $sql);

    if ($res) {
        echo "0";
    } else {
        echo "1";
    }
} else {
    echo "Invalid Request...";
}
