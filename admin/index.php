<?php
include "application-top.php";

/* Encryption key geneartion */

$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
$charactersLength = strlen($characters);
$randomString = '';
for ($i = 0; $i < 32; $i++) {
    $randomString .= $characters[rand(0, $charactersLength - 1)];
}
?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>FALTA SEZ - ADMIN</title>
      <!-- Custom fonts for this template-->
      <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
      <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> -->
      <!-- Custom styles for this template-->
      <link href="css/sb-admin-2.css" rel="stylesheet">
   </head>
   <body class="bg-gradient-primary">
      <div class="container">
         <!-- Outer Row -->
         <div class="row justify-content-center">
            <div class="col-xl-10 col-lg-12 col-md-9">
               <div class="card o-hidden border-0 shadow-lg my-5">
                  <div class="card-body p-0">
                     <!-- Nested Row within Card Body -->
                     <div class="row">
                        <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                        <div class="col-lg-6">
                           <div class="p-5">
                              <div class="text-center">
                                 <h1 class="h4 text-gray-900 mb-4">Welcome FSEZ Admin!</h1>
                              </div>
                              <form class="user" id="login-form">
                                	<input type="hidden" name="cryptoKey" id="cryptoKey" value="<?php echo $randomString; ?>">
                                 <div class="form-group">
                                    <input type="email" name="email" autocomplete="off" class="form-control form-control-user" required id="email" value="<?php if (isset($_COOKIE["email"])) {echo $_COOKIE["email"];}?>" placeholder="Enter Email Address...">
                                 </div>
                                 <div class="form-group">
                                    <input type="password" name="password" autocomplete="off" class="form-control form-control-user" required id="password" value="<?php if (isset($_COOKIE["password"])) {echo $_COOKIE["password"];}?>"  placeholder="Password">
                                 </div>

                                 <div class="row pt-1">
                                   <div class="col-md-6">
                                     <div class="form-group">
                                      <?php include 'includes/captcha.php';?>
                                     </div>
                                 </div>
                                 <div class="col-md-6">
                                     <div class="form-group">
                                     <input name="captcha" id="captcha" autocomplete="off" class="form-control form-control-user" required placeholder="Answer" onkeypress="return IsNumeric(event);"  type="number">
                                      </div>
                                 </div>
                                 </div>

                                 <div class="form-group">
                                    <div class="custom-control custom-checkbox small">
                                       <input type="checkbox" name="rememberMe" class="custom-control-input" value="1" <?php if (isset($_COOKIE["loggedin_user"])) {?> checked <?php }?> id="rememberMe">
                                       <label class="custom-control-label" for="rememberMe">Remember Me</label>
                                    </div>
                                 </div>
                                 <input type="submit" name="submit" class="btn btn-primary btn-user btn-block" value="Login">
                                 <hr>
                              </form>
                              <div class="alert alert-danger text-center" id="error"></div>
                              <hr>
                              <div class="text-center">
                                 <a class="small" href="forgot-password.php">Forgot Password?</a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php include "includes/common-js.php";?>
   </body>
</html>