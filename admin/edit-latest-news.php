<?php
include "application-top.php";

$sql = "select * from fsez_news where news_id=" . $_GET["news_id"];
$res = mysqli_query($con, $sql);

if ($res) {
    $row = mysqli_fetch_array($res);
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>FALTA SEZ - Latest News Update</title>
    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> -->
    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.css" rel="stylesheet">
</head>

<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <?php include "includes/sidebar.php";?>
        <!-- End of Sidebar -->
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
                <?php include "includes/header.php";?>
                <!-- End of Topbar -->
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Outer Row -->
                    <div class="nav-container">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i></a>
                            </li>
                            <li class="breadcrumb-item active">Edit Latest News</li>
                            <li class="ml-auto"><a href="latest-news.php" class="btn btn-sm btn-success add-btn"> <i
                                        class="fa fa-list" aria-hidden="true"></i> View Latest News</a></li>
                        </ol>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-xl-10 col-lg-12 col-md-9">
                            <div class="card o-hidden border-0 shadow-lg my-5">
                                <div class="card-body p-0">
                                    <!-- Nested Row within Card Body -->
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="p-5">
                                                <div class="text-center">
                                                    <h1 class="h4 text-gray-900 mb-4">Update Latest News</h1>
                                                </div>
                                                <form class="user" id="enewsForm" enctype="multipart/form-data">
                                                    <div class="form-group">
                                                        <textarea required name="news_description"
                                                            placeholder="What's in news..." id="news_description"
                                                            class="form-control form-control-user"><?php echo $row["news_description"]; ?></textarea>
                                                    </div>
                                                    <input type="hidden" name="csrf_token"
                                                        value="<?php echo $_SESSION['csrf_token']; ?>" />
                                                    <?php
if ($row["news_file_name"] !== "noimage.gif") {
    ?>
                                                    <div class="form-group">
                                                        <iframe
                                                            src="upload_news_documents/<?php echo $row["news_file_name"]; ?>"
                                                            width="100%" height="400px"
                                                            alt="News Image"></iframe>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="custom-file">
                                                            <input type="file" name="news_file_name"
                                                                class="custom-file-input form-control-user"
                                                                accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,.pdf,.doc,.docx,image/*" onchange="validateFile(id)"
                                                                id="news_file_name">
                                                            <label class="custom-file-label" for="customFile">Choose
                                                                file</label>
                                                        </div>
                                                    </div>
                                                       <?php
} else if ($row["news_link"] !== "") {
    ?>
                                                     <div class="form-group">
                                                            <input type="url" name="news_link"
                                                                class="form-control form-control-user" id="news_link" value=<?php echo $row["news_link"]; ?>
                                                                placeholder="Enter link Url">
                                                    </div>
                                                    <?php
} else {
    ?>
                                                    <div class="form-group row">
                                                      <div class="col-md-3">
                                                      <label>Add external link :</label>
                                                      </div>
                                                      <div class="col-md-8">
                                                        <input type="radio" name="is_link" id="is_link" value="Yes">
                                                        <label> YES </label>
                                                        &emsp;
                                                        <input type="radio" name="is_link" id="is_link" checked="true" value="No">
                                                        <label>	NO </label>
                                                        </div>
									            	</div>
                                                     <div class="form-group" id="elink">
                                                            <input type="url" name="news_link"
                                                                class="form-control form-control-user" id="news_link"
                                                                placeholder="Enter link Url">
                                                        </div>
                                                        <div class="form-group" id="efile">
                                                        <div class="custom-file">
                                                            <input type="file" name="news_file_name"
                                                                class="custom-file-input form-control-user"
                                                                accept="application/pdf, .pdf,.doc,.docx"
                                                                onchange="validateFile(id)" id="news_file_name">
                                                            <label class="custom-file-label" for="customFile">Choose
                                                                file</label>
                                                        </div>
                                                    </div>
                                                    <?php
}
?>
                                                    <input type="submit" class="btn btn-primary btn-user btn-block"
                                                        value="Update News">
                                                </form>
                                                <hr>
                                                <p>
                                                  <div class="errcode text-danger mb-2"></div>
                                                  <div class="errcopy text-danger mb-2"></div>
                                                </p>
                                                <div class="alert alert-success alert-dismissible" id="enSuccess">
                                                    <a href="#" class="close" data-dismiss="alert"
                                                        aria-label="close">&times;</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <!-- End of Main Content -->
            <!-- Footer -->
            <?php include "includes/footer.php";?>
            <!-- End of Footer -->
        </div>
        <!-- End of Content Wrapper -->
    </div>
    <!-- End of Page Wrapper -->
    <!-- Bootstrap and core JavaScript-->
    <?php include "includes/common-js.php";?>
    <script>

    /* Update News */

    jQuery(document).ready(function($){

    $("#enSuccess").hide();

    $("#enewsForm").on("submit", function(e) {
    e.preventDefault();

    var news_id    = <?php echo $_GET["news_id"]; ?>;
    var news_description  = $("#news_description").val();
    // var news_link = $("#news_link").val();

    var formData = new FormData($('#enewsForm')[0]);
    formData.append("news_file_name", $('input[type=file]')[0].files[0]);
    formData.append("news_description", news_description);
    // formData.append("news_link", news_link);
    formData.append("news_id", news_id);


    $.ajax({
    type : "POST",
    url  : "ajax/update-news.php?news_id="+news_id,
    data : formData,
    dataType: "html",
    cache: false,
    contentType: false,
    processData: false,
    success: function(response) {
    if(response == 0)
    {
    enewsForm.reset();
    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
    $("#enSuccess").append("News Updated Succesfully...").show();
    setTimeout(function(){location.href="latest-news.php"} , 3000);
    } else if (response == 3) {
    enewsForm.reset();
    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
    alert("Invalid file type, please choose another file.");
    } else {
    enewsForm.reset();
    $(".custom-file-label")[0].childNodes[0].data = "Choose File";
    alert("Something went wrong");
    }
    }
    });

    });
    });
</script>
</body>

</html>