<?php
include "application-top.php";

if (!isset($_SESSION["email"])) {
    header("location:index.php");
    exit();
}

$sql = "select * from fsez_contacts where contact_id=" . $_GET["contact_id"];
$res = mysqli_query($con, $sql);

if ($res) {
    $row = mysqli_fetch_array($res);
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>FALTA SEZ - Contact Update</title>
    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> -->
    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.css" rel="stylesheet">
</head>

<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <?php include "includes/sidebar.php";?>
        <!-- End of Sidebar -->
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
                <?php include "includes/header.php";?>
                <!-- End of Topbar -->
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Outer Row -->
                    <div class="nav-container">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i></a>
                            </li>
                            <li class="breadcrumb-item active">Edit Contact</li>
                            <li class="ml-auto"><a href="contact-list.php" class="btn btn-sm btn-success add-btn"> <i
                                        class="fa fa-list" aria-hidden="true"></i> View Contacts</a></li>
                        </ol>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-xl-10 col-lg-12 col-md-9">
                            <div class="card o-hidden border-0 shadow-lg my-5">
                                <div class="card-body p-0">
                                    <!-- Nested Row within Card Body -->
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="p-5">
                                                <div class="text-center">
                                                    <h1 class="h4 text-gray-900 mb-4">Update Contact</h1>
                                                </div>
                                                <form class="user" id="ecntForm" enctype="multipart/form-data">
                                                <div class="form-group">
                                                    <input type="text" required name="contact_name"
                                                            class="form-control form-control-user" id="contact_name"
                                                          value="<?php echo $row["contact_name"]; ?>" autocomplete="off" placeholder="Enter Contact Name">
                                                  </div>
												
                                                    <div class="form-group">
														<?php 
														$dgsql = "select * from fsez_designations where status = 1";
														$dgres = mysqli_query($con, $dgsql);
														?>
                                                        <select required name="designation"
                                                            class="form-control" id="designation">
                                                            <option selected="selected" disabled="true">Select Designation</option>
															<?php 
															if($dgres) 
															{
															 while ($dgrow = mysqli_fetch_array($dgres))
															 {
																 if($dgrow["designation_id"] == $row["designation"])
                                                                {
                                                                    $selected = "SELECTED";
                                                                }
                                                                else
                                                                {
                                                                    $selected = NULL;
                                                                }
															?>
                                                            <option value="<?php echo $dgrow["designation_id"]; ?>"   <?php echo $selected; ?>><?php echo $dgrow["designation_name"]; ?></option>
															<?php 
															 }
															}
															?>
                                                       </select>    
                                                    </div>
                                                   
												    <div class="form-group">
                                                    <input type="text" required name="email_id"
                                                            class="form-control form-control-user" id="email_id"
                                                        value="<?php echo $row["email_id"]; ?>" autocomplete="off" placeholder="Enter Email Address">
                                                  </div>

                                                  <div class="form-group">
                                                    <input type="text" name="contact_no"
                                                            class="form-control form-control-user" id="contact_no"
                                                            value="<?php echo $row["contact_no"]; ?>" autocomplete="off"  onkeypress="return IsNumeric(event);" minlength="8" placeholder="Enter Contact Number">
                                                  </div>

                                                    <input type="hidden" name="csrf_token"
                                                        value="<?php echo $_SESSION['csrf_token']; ?>" />
														
												<input type="submit" class="btn btn-primary btn-user btn-block"
                                                        value="Update Contact">
                                                   
                                                <hr>
                                                <p>
                                                  <div class="errcode text-danger mb-2"></div>
                                                  <div class="errcopy text-danger mb-2"></div>
                                                </p>
                                                <div class="alert alert-success alert-dismissible" id="ecSuccess">
                                                    <a href="#" class="close" data-dismiss="alert"
                                                        aria-label="close">&times;</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <!-- End of Main Content -->
            <!-- Footer -->
            <?php include "includes/footer.php";?>
            <!-- End of Footer -->
        </div>
        <!-- End of Content Wrapper -->
    </div>
    <!-- End of Page Wrapper -->
    <!-- Bootstrap and core JavaScript-->
    <?php include "includes/common-js.php";?>

    <script>
    /* Update Circular */

    jQuery(document).ready(function($) {

        $("#ecSuccess").hide();

        $("#ecntForm").on("submit", function(e) {
            e.preventDefault();

            var contact_id = <?php echo $_GET["contact_id"]; ?>;
            var contact_name = $("#contact_name").val();
            var designation = $("#designation").val();
            var email_id = $("#email_id").val();

            var formData = new FormData($('#ecntForm')[0]);
            formData.append("contact_name", contact_name);
            formData.append("designation", designation);
            formData.append("email_id", email_id);
            formData.append("contact_id", contact_id);


            $.ajax({
                type: "POST",
                url: "ajax/update-contact.php?contact_id=" + contact_id,
                data: formData,
                dataType: "html",
                cache: false,
                contentType: false,
                processData: false,
                success: function(response) {
                    if (response == 0) {
                        ecntForm.reset();
                        $("#ecSuccess").append("Contact Updated Succesfully...").show();
                        setTimeout(function() {
                            location.href = "contact-list.php"
                        }, 3000);
                    } else {
                        ecntForm.reset();
                        alert("Something went wrong");
                    }
                }
            });

        });
    });
    </script>
</body>

</html>