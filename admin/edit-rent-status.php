<?php
include "application-top.php";

if (!isset($_SESSION["email"])) {
    header("location:index.php");
    exit();
}

$sql = "select * from fsez_rent_status where rent_status_id=" . $_GET["rent_status_id"];
$res = mysqli_query($con, $sql);

if ($res) {
    $row = mysqli_fetch_array($res);
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>FALTA SEZ - Rent Status Update</title>
    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> -->
    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.css" rel="stylesheet">
</head>

<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <?php include "includes/sidebar.php";?>
        <!-- End of Sidebar -->
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
                <?php include "includes/header.php";?>
                <!-- End of Topbar -->
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Outer Row -->
                    <div class="nav-container">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i></a>
                            </li>
                            <li class="breadcrumb-item active">Edit Rent Status</li>
                            <li class="ml-auto"><a href="rent-status-list.php" class="btn btn-sm btn-eou add-btn"> <i
                                        class="fa fa-list" aria-hidden="true"></i> View Rent Status</a></li>
                        </ol>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-xl-10 col-lg-12 col-md-9">
                            <div class="card o-hidden border-0 shadow-lg my-5">
                                <div class="card-body p-0">
                                    <!-- Nested Row within Card Body -->
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="p-5">
                                                <div class="text-center">
                                                    <h1 class="h4 text-gray-900 mb-4">Update Existing Rent Status</h1>
                                                </div>
                                                <form class="user" id="erentForm" enctype="multipart/form-data">
												
												<div class="form-group">
												<?php 
												$cmsql = "select * from fsez_companies where status = 1";
												$cmres = mysqli_query($con, $cmsql);
												?>
                                                <select required name="company_name"
                                                            class="form-control" id="company">
													<option disabled="true">Select Company
                                                            </option>
                                                      <?php 
                                                        if($cmres)
                                                        {
                                                            while($cmrow = mysqli_fetch_array($cmres))
                                                            {
                                                                if($cmrow["company_id"] == $row["company"])
                                                                {
                                                                    $selected = "SELECTED";
                                                                }
                                                                else
                                                                {
                                                                    $selected = NULL;
                                                                }
                                                            ?>
                                                            <option value="<?php echo $cmrow["company_id"]; ?>"
                                                                <?php echo $selected; ?>>
                                                                <?php echo $cmrow["company_name"]; ?></option>
                                                            <?php
                                                            }
                                                        }
                                                        ?>
                                                       </select>    
                                                    </div>
                                                    <div class="form-group">
                                                    <select required name="rent_paid_of_year"
                                                            class="form-control" id="rent_paid_of_year">
                                                            <option disabled="true">Select Year
                                                            </option>
															  <option value="<?php echo $row["rent_paid_of_year"]; ?>" <?php echo $selected; ?>>
                                                                <?php echo $row["rent_paid_of_year"]; ?></option>
                                                                <option value="2021">2021</option>
                                                                <option value="2022">2022</option>
                                                                <option value="2023">2023</option>
                                                       </select>
                                                    </div>
                                                    <div class="form-group">
                                                    <select required name="rent_paid_of_quarter"
                                                            class="form-control" id="rent_paid_of_quarter">
                                                            <option disabled="true">Select Quarter</option>
															<option value="<?php echo $row["rent_paid_of_quarter"]; ?>" <?php echo $selected; ?>>
                                                                <?php echo $row["rent_paid_of_quarter"]; ?></option>
                                                            <option value="1">First</option>
                                                            <option value="2">Second</option>
                                                            <option value="3">Third</option>
                                                            <option value="4">Fourth</option>
                                                       </select>
                                                    </div>
                                                    
                                                  <div class="form-group">
                                                        <input type="text" required name="rent_amount"
                                                            class="form-control form-control-user" id="rent_amount" onkeypress="return IsNumeric(event);"
                                                            autocomplete="off" placeholder="Enter Rent Amount" value="<?php echo $row["rent_amount"]; ?>">
                                                    </div>    

                                                    <input type="hidden" name="csrf_token"
                                                        value="<?php echo $_SESSION['csrf_token']; ?>" />
                                                    <div class="form-group">
                                                        <iframe
                                                            src="upload_rent_documents/<?php echo $row["rent_status_file_name"]; ?>"
                                                            width="100%" height="400px"
                                                            alt="<?php echo $row["company"]; ?>"></iframe>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="custom-file">
                                                            <input type="file" name="rent_status_file_name"
                                                                class="custom-file-input form-control-user"
                                                                accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,.pdf,.doc,.docx"
                                                                onchange="validateFile(id)" id="rent_status_file_name">
                                                            <label class="custom-file-label" for="customFile">Choose
                                                                file</label>
                                                        </div>
                                                    </div>
                                                    <input type="submit" class="btn btn-primary btn-user btn-block"
                                                        value="Update Rent Status">
                                                </form>
                                                <hr>
                                                <p>
                                                  <div class="errcode text-danger mb-2"></div>
                                                  <div class="errcopy text-danger mb-2"></div>
                                                </p>
                                                <div class="alert alert-success alert-dismissible" id="erSuccess">
                                                    <a href="#" class="close" data-dismiss="alert"
                                                        aria-label="close">&times;</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <!-- End of Main Content -->
            <!-- Footer -->
            <?php include "includes/footer.php";?>
            <!-- End of Footer -->
        </div>
        <!-- End of Content Wrapper -->
    </div>
    <!-- End of Page Wrapper -->
    <!-- Bootstrap and core JavaScript-->
    <?php include "includes/common-js.php";?>

    <script>
    /* Update Circular */

    jQuery(document).ready(function($) {

        $("#erSuccess").hide();

        $("#erentForm").on("submit", function(e) {
            e.preventDefault();

            var rent_status_id = <?php echo $_GET["rent_status_id"]; ?>;
            var company = $("#company").val();
            var rent_paid_of_year = $("#rent_paid_of_year").val();
            var rent_paid_of_quarter = $("#rent_paid_of_quarter").val();
            var rent_amount = $("#rent_amount").val();
            
         if (rent_amount == "" || rent_amount == undefined || rent_amount == 0 || rent_amount == "0.00") {
              $('#rent_amount').focus();
              alert("Please enter a valid rent amount");
              return false;
             }

            var formData = new FormData($('#erentForm')[0]);
            formData.append("rent_status_file_name", $('input[type=file]')[0].files[0]);
            formData.append("company", company);
            formData.append("rent_paid_of_year", rent_paid_of_year);
            formData.append("rent_paid_of_quarter", rent_paid_of_quarter);
            formData.append("rent_amount", rent_amount);
            formData.append("rent_status_id", rent_status_id);


            $.ajax({
                type: "POST",
                url: "ajax/update-rent.php?rent_status_id=" + rent_status_id,
                data: formData,
                dataType: "html",
                cache: false,
                contentType: false,
                processData: false,
                success: function(response) {
                    if (response == 0) {
                        erentForm.reset();
                        $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                        $("#erSuccess").append("Rent Status Updated Succesfully...").show();
                        setTimeout(function() {
                            location.href = "rent-status-list.php"
                        }, 3000);
                    } else if (response == 3) {
                        erentForm.reset();
                       $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                       alert("Invalid file type, please choose another file.");
                    } else {
                        erentForm.reset();
                        $(".custom-file-label")[0].childNodes[0].data = "Choose File";
                        alert("Something went wrong");
                    }
                }
            });

        });
    });
    </script>
</body>

</html>