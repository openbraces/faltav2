<?php
if (!isset($_SESSION)) {
    ini_set("session.cookie_httponly", 1);
}

include "application-top.php";
include "includes/encryption.php";

if ($_POST["captcha"] && $_POST["captcha"] != "" && $_SESSION["vercode"] == $_POST["captcha"]) {
    
    if ($_POST["email"] != "" || $_POST["password"] != "") {

        $cipherValue = $_POST['cryptoKey'];
        $Encryption = new Encryption();
        $crypEmail = $Encryption->decrypt($_POST['email'], $cipherValue);
        $crypPass = $Encryption->decrypt($_POST['password'], $cipherValue);

        $email = mysqli_real_escape_string($con, $crypEmail);
        $password = mysqli_real_escape_string($con, sha1(sha1(sha1($crypPass))));

        $sql = "select * from admin_user where email = '$email' and password = '$password' and status = 1";
        $res = mysqli_query($con, $sql) or die("Query Fail");

        $numrow = mysqli_num_rows($res);

        if ($numrow > 0) {
            $row = mysqli_fetch_array($res);

            $_SESSION["email"] = $email;
            $_SESSION["id"] = $row["id"];
            $_SESSION["username"] = $row["username"];
            $_SESSION["profile_image"] = $row["profile_image"];

            // Create a new CSRF token.
            if (!isset($_SESSION['csrf_token'])) {
                $_SESSION['csrf_token'] = base64_encode(openssl_random_pseudo_bytes(32));
            }

            // if remember me clicked . Values will be stored in $_COOKIE  array

            if (!empty($_POST["rememberMe"]) && $_POST["rememberMe"] == 1) {
                //COOKIES for username

                setcookie("loggedin_user", $_POST["email"], time() + (1 * 365 * 24 * 60 * 60));

                //COOKIES for password

                setcookie("user_password", $password, time() + (1 * 365 * 24 * 60 * 60));
            } else {
                if (isset($_COOKIE["loggedin_user"])) {
                    setcookie("loggedin_user", "");

                    if (isset($_COOKIE["user_password"])) {
                        setcookie("user_password", "");
                    }
                }
            }

            $lg_sql = mysqli_query($con, "update admin_user set last_loggedin = NOW() where email='" . $_SESSION["email"] . "' and id = '" . $row['id'] . "'");

            echo 1;
        } else {
            echo "<span> Invalid Email or Password ! </span>";
        }

    }
} else {
    echo "<span> Invalid Answer! </span>";
}
