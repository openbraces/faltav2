<?php
   include("application-top.php");

   if(!isset($_SESSION["email"]))
   {
	header("location:index.php");
	exit();
   }
   
   $dn_sql = "select * from fsez_designations order by added_on desc";
   $dn_res = mysqli_query($con, $dn_sql);
   
   if(isset($_GET["mode"]))
   {
   if ($_GET["mode"] == "publish")
   {
   if($_GET["val"] == "pub")
   {
   $utsql = "update fsez_designations set status = 0 where designation_id=".$_GET["designation_id"];
   $uures = mysqli_query($con, $utsql);
   
   if($uures)
   {
   header("location:designation-list.php");
   exit();
   }
   }
   else 	
   {
   $utsql = "update fsez_designations set status = 1 where designation_id=".$_GET["designation_id"];
   $uures = mysqli_query($con, $utsql);
   
   if($uures)
   {
   header("location:designation-list.php");
   exit();
   }
   }
   }
   }
   
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>FALTA SEZ - Designation List</title>
    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> -->
    <!-- Custom styles for this template -->
    <link href="css/sb-admin-2.css" rel="stylesheet">
    <!-- Custom styles for this page -->
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
</head>

<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <?php include("includes/sidebar.php"); ?>
        <!-- End of Sidebar -->
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
                <?php include("includes/header.php"); ?>
                <!-- End of Topbar -->
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800"></h1>
                    <!-- DataTales Example -->
                    <div class="nav-container">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i></a>
                            </li>
                            <li class="breadcrumb-item active">Designations</li>
                            <li class="ml-auto"><a href="add-contact.php" class="btn btn-sm btn-success add-btn"> <i
                                        class="fa fa-plus" aria-hidden="true"></i> Add New Contact</a></li>
                        </ol>
                    </div>

                    <div class="col-md-12" id="confirm" style="display:none">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            Designation has been deleted successfully !!!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    
                    <?php 
            		if(isset($_GET["msg"]))
            		{
            		?>
            		<div class="alert alert-success alert-dismissible fade show" role="alert">
            			 <?php echo $_GET["msg"];?>
            			<button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="cngurlparam()">
            			<span aria-hidden="true">&times;</span>
            			</button>
                   </div>
            		<?php
            		}
            		?>
					
				   <div class="card shadow mb-4">
					<div class="card-header py-3">
						<h6 class="m-0 font-weight-bold text-primary">Add New Designation</h6>
					</div>
					<div class="card-body">
					  <form class="form-inline" id="dnForm">							
						 <div class="form-group mr-3 input-width-50">
							<input type="text" required name="designation_name"
							 class="form-control full-input-width" id="designation_name"
									placeholder="Enter Designation Name">
						  </div>
                          <div class="form-group mr-3 input-width-30">
                                <select name="sequence" id="sequence" required
                                    class="form-control full-input-width">
                                    <option selected="selected" disabled="true">Select Sequence
                                    </option>          
                                    <option value="1">1</option>
                                    <option value="2">2</option>    
                                    <option value="3">3</option>    
                                    <option value="4">4</option>    
                                    <option value="5">5</option>    
                                    <option value="6">6</option>    
                                    <option value="7">7</option>    
                                    <option value="8">8</option>    
                                    <option value="9">9</option>    
                                    <option value="10">10</option>
                                    <option value="11">11</option> 
                                    <option value="12">12</option> 
                                    <option value="13">13</option> 
                                    <option value="14">14</option> 
                                    <option value="15">15</option>    
                                    <option value="16">16</option>                          
                                </select>
                            </div>
																						  
						  <input type="hidden" name="csrf_token"
								value="<?php echo $_SESSION['csrf_token']; ?>" />
						   
							<input type="submit" class="btn btn-eou btn-right"
								value="Save Designation">
						</form>

                        <p>
                            <div class="errcode text-danger"></div>
                            <div class="errcopy text-danger"></div>
                        </p>

                       <div class="alert alert-success alert-dismissible mt-3" id="dsgSuccess">
							<a href="#" class="close" data-dismiss="alert"
								aria-label="close">&times;</a>
						</div>												
					</div>
					</div>
					
				   

                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Designation List</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered ttable" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>SL No.</th>
                                            <th>Designation Name</th>
                                            <th>Order</th>
                                            <th>Added On</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>SL No.</th>
                                            <th>Designation Name</th>
                                            <th>Order</th>
                                            <th>Added On</th>
                                            <th>Actions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php
                                    $i = 0;
                                    if($dn_res)
                                    {
                                    while($dn_row = mysqli_fetch_array($dn_res))
                                    {
                                    $i++;
                                    ?>
                                        <tr>
                                            <td><?php echo $i; ?>.</td>
                                            <td><?php echo $dn_row["designation_name"]; ?></td>
                                            <td><?php echo $dn_row["sequence"]; ?></td>
                                            <td><?php echo $dn_row["added_on"]; ?></td>
                                            <td>
                                                <a href="#" data-id="<?php echo $dn_row["designation_id"];?>"
                                                   data-toggle="modal" data-target="#edModal"  class="btn btn-info btn-sm edit">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                                <?php
                                          if(obtools::is_role_published($dn_row["designation_id"]))
                                          { 
                                          ?>
                                                <a href="designation-list.php?designation_id=<?php echo $dn_row["designation_id"];?>&mode=publish&val=pub"
                                                    class="btn btn-success btn-sm">
                                                    <i class="fas fa-check"></i>
                                                </a>
                                                <?php 
                                          }
                                          else
                                          {
                                          ?>
                                                <a href="designation-list.php?designation_id=<?php echo $dn_row["designation_id"];?>&mode=publish&val=unpub"
                                                    class="btn btn-warning btn-sm">
                                                    <i class="fas fa-times"></i>
                                                </a>
                                                <?php
                                          }
                                          ?>
                                                <a href="#" data-id="<?php echo $dn_row["designation_id"];?>"
                                                    data-toggle="modal" data-target="#dgModal"
                                                    class="btn btn-danger btn-sm delete">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- End of Main Content -->
            <!-- Footer -->
            <?php include("includes/footer.php"); ?>
            <!-- End of Footer -->
        </div>
        <!-- End of Content Wrapper -->
    </div>
    <!-- End of Page Wrapper -->

    <!-- Delete Modal -->
    <div class="modal fade" id="dgModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header bg-danger">
                    <h4 class="modal-title text-white">Delete Designation</h4>
                    <button type="button" class="close text-white" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body text-center mt-4">
                    Are you sure want to delete this record ???
                </div>
                <hr>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="trash" data-dismiss="modal">Yes</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                </div>

            </div>
        </div>
    </div>
    <!-- Delete Modal -->
    
    <!-- Edit Modal -->
    <div class="modal fade" id="edModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header bg-warning">
                    <h4 class="modal-title text-white">Update Designation</h4>
                    <button type="button" class="close text-white" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div id="edRes"></div>

            </div>
        </div>
    </div>
    <!-- Edit Modal -->

    <?php include("includes/common-js.php"); ?>

    <script>
    jQuery(document).ready(function($) {

        $("#confirm").hide();
        var dnid = '';
        var dgId = '';
        
        $(".ttable").on('click', '.delete', function() {
            dnid = $(this).attr("data-id");
        });
        
        $("#trash").on('click', function() {
            if (dnid) {
                $.ajax({
                    type: "GET",
                    url: "ajax/delete-designation.php?dnid=" + dnid,
                    cache: false,
                    success: function(response) {
                        if (response == 0) {
                            $('html, body').animate({
                                scrollTop: 0
                            }, 'slow');
                            $("#confirm").css("display", "block");
                            setTimeout(function() {
                                window.location = 'designation-list.php';
                            }, 3000);
                        }
                    }
                });
            }
        });
        
	$(function(){
    
    $(".ttable").on('click', '.edit', function(){
      var dgId = $(this).attr("data-id");
      var ctoken = '<?php echo $_SESSION['csrf_token']; ?>';
    	if (dgId) {
	      $.ajax({
        	type : "GET",
        	url  : "ajax/update-designation.php?ctoken="+ctoken+"&designation_id="+dgId,
        	cache: false,
        	success: function(data)
        	{
            	$("#edRes").html(data);
        	}
	      });
         }
        });
      });
  
    });
    
    function cngurlparam() {
        var cnurl = window.location.href;
        var newUrl = cnurl.split("?");
        location.href = location.href.replace(window.location.href, newUrl[0]); 
    }
    </script>
</body>

</html>