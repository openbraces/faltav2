-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 03, 2020 at 08:04 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `falta_sez`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_user`
--

CREATE TABLE `admin_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `contact_no` varchar(100) NOT NULL,
  `profile_image` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `added_on` datetime NOT NULL,
  `last_loggedin` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_user`
--

INSERT INTO `admin_user` (`id`, `username`, `contact_no`, `profile_image`, `email`, `password`, `status`, `added_on`, `last_loggedin`) VALUES
(1, 'John Doe', '9830011854', '1561883525profile2.jpg', 'admin@gmail.com', '7d7c47b2bed4820d77094001207cda38012cdf25', 1, '2019-06-23 00:51:27', '2020-03-01 16:04:11');

-- --------------------------------------------------------

--
-- Table structure for table `fsez_auth_meetings`
--

CREATE TABLE `fsez_auth_meetings` (
  `auth_id` int(11) NOT NULL,
  `auth_meeting_title` varchar(255) NOT NULL,
  `auth_meeting_file` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `added_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fsez_auth_meetings`
--

INSERT INTO `fsez_auth_meetings` (`auth_id`, `auth_meeting_title`, `auth_meeting_file`, `status`, `added_on`) VALUES
(1, 'Test Meeting edit', '1582967016e-PAN_881031368759220.pdf', 1, '2020-02-29 14:15:22');

-- --------------------------------------------------------

--
-- Table structure for table `fsez_circulars`
--

CREATE TABLE `fsez_circulars` (
  `circular_id` int(11) NOT NULL,
  `circular_name` mediumtext NOT NULL,
  `circular_file_name` mediumtext NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `added_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fsez_circulars`
--

INSERT INTO `fsez_circulars` (`circular_id`, `circular_name`, `circular_file_name`, `status`, `added_on`) VALUES
(1, 'Testing Document', '1580631472Resume_new_pdf_3,079KB.pdf', 0, '2020-01-26 12:01:28'),
(2, 'Test Document 1342', '1581188949wewous.png', 1, '2020-01-26 12:03:54'),
(3, 'Testing Doc', '1580021111Admit Card.pdf', 0, '2020-01-26 12:15:11'),
(4, 'Test Doc', '1580021527e-PAN_881031368759220.pdf', 1, '2020-01-26 12:22:07'),
(10, 'Test Doc', '1580022480Admit Card.pdf', 0, '2020-01-26 12:38:00'),
(12, 'Test new', '1580022803Resume_new_pdf_3,079KB.pdf', 0, '2020-01-26 12:43:23'),
(14, 'Test Doc', '1580023406Admit Card.pdf', 0, '2020-01-26 12:53:26'),
(15, 'Test Doc', '15800235398810313687592202019-12-28 19_05_58.387_signed.pdf', 0, '2020-01-26 12:55:39'),
(17, 'Testing Doc', '1581159576wewous.png', 0, '2020-02-08 16:29:36'),
(19, 'Test Doc', '1582142492careerconect.png', 0, '2020-02-20 01:31:32'),
(21, 'Test Doc 12345', '1582391761sample.pdf', 0, '2020-02-22 20:47:02'),
(22, 'Test Doc 22-3', '1582391689e-PAN_881031368759220.pdf', 1, '2020-02-22 22:44:49');

-- --------------------------------------------------------

--
-- Table structure for table `fsez_forms`
--

CREATE TABLE `fsez_forms` (
  `form_id` int(11) NOT NULL,
  `form_name` mediumtext NOT NULL,
  `form_file_name` mediumtext NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `added_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fsez_forms`
--

INSERT INTO `fsez_forms` (`form_id`, `form_name`, `form_file_name`, `status`, `added_on`) VALUES
(1, 'First Form', '1580635029Resume-Freshmen-Example-2017.pdf', 1, '2020-02-01 17:03:40'),
(4, 'Final form', '1580558076Resume_new_pdf_3,079KB.pdf', 0, '2020-02-01 17:24:36'),
(5, 'Form one tow', '1582969664e-PAN_881031368759220.pdf', 0, '2020-02-08 22:10:21'),
(6, 'Final form test one', '1581189471sample.pdf', 0, '2020-02-08 22:15:54');

-- --------------------------------------------------------

--
-- Table structure for table `fsez_gallery_categories`
--

CREATE TABLE `fsez_gallery_categories` (
  `category_id` int(10) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `added_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fsez_gallery_categories`
--

INSERT INTO `fsez_gallery_categories` (`category_id`, `category_name`, `status`, `added_on`) VALUES
(1, 'Events', 1, '2020-02-21 11:41:53'),
(2, 'Test', 0, '2020-02-21 12:29:16'),
(3, 'Test', 0, '2020-02-21 12:30:18'),
(4, 'Test 1', 0, '2020-02-21 12:32:41'),
(5, 'Test 2', 0, '2020-02-21 12:34:28'),
(6, 'Testing', 0, '2020-02-22 13:31:35'),
(7, 'Test 1', 0, '2020-02-22 13:33:04'),
(8, 'Test', 0, '2020-02-22 13:34:12'),
(9, 'Test', 0, '2020-02-22 13:34:16'),
(10, 'Test 123', 0, '2020-02-22 19:53:10'),
(11, 'Testing', 0, '2020-02-22 19:53:44'),
(12, 'Test 1234', 0, '2020-02-22 21:11:08');

-- --------------------------------------------------------

--
-- Table structure for table `fsez_gallery_images`
--

CREATE TABLE `fsez_gallery_images` (
  `id` int(10) NOT NULL,
  `category_id` int(10) NOT NULL,
  `gallery_image` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `added_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fsez_gallery_images`
--

INSERT INTO `fsez_gallery_images` (`id`, `category_id`, `gallery_image`, `status`, `added_on`) VALUES
(1, 1, '1582390821hoot.png', 1, '2020-02-22 14:13:12'),
(2, 1, '1582390902wewous.png', 1, '2020-02-22 14:13:51'),
(3, 1, '1582383833hoot.png', 1, '2020-02-22 20:33:53');

-- --------------------------------------------------------

--
-- Table structure for table `fsez_news`
--

CREATE TABLE `fsez_news` (
  `news_id` int(11) NOT NULL,
  `news_description` varchar(255) NOT NULL,
  `news_link` varchar(255) NOT NULL,
  `news_file_name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `added_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fsez_news`
--

INSERT INTO `fsez_news` (`news_id`, `news_description`, `news_link`, `news_file_name`, `status`, `added_on`) VALUES
(1, 'Test Update', 'http://openbraces.in/falta-sez/', '1582370527e-PAN_881031368759220.pdf', 1, '2020-02-22 15:56:13');

-- --------------------------------------------------------

--
-- Table structure for table `fsez_rti`
--

CREATE TABLE `fsez_rti` (
  `rti_id` int(10) NOT NULL,
  `rti_title` varchar(255) NOT NULL,
  `rti_file_name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `added_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fsez_rti`
--

INSERT INTO `fsez_rti` (`rti_id`, `rti_title`, `rti_file_name`, `status`, `added_on`) VALUES
(1, 'First rti', '1582973073careerconect.png', 1, '2020-02-29 15:56:56');

-- --------------------------------------------------------

--
-- Table structure for table `fsez_tenders`
--

CREATE TABLE `fsez_tenders` (
  `tender_id` int(11) NOT NULL,
  `tender_name` mediumtext NOT NULL,
  `tender_file_name` mediumtext NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `added_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fsez_tenders`
--

INSERT INTO `fsez_tenders` (`tender_id`, `tender_name`, `tender_file_name`, `status`, `added_on`) VALUES
(1, 'Tender One', '15805559598810313687592202019-12-28 19_05_58.387_signed.pdf', 0, '2019-07-10 16:49:19'),
(2, 'Tender Two', '1580556132Resume_new_pdf_3,079KB.pdf', 1, '2020-02-01 16:52:12'),
(4, 'Tender One', '1581182491wewous.png', 0, '2020-02-08 22:51:31'),
(5, 'Tender Three Three', '1582448063resume-test.docx', 0, '2020-02-23 14:24:23');

-- --------------------------------------------------------

--
-- Table structure for table `fsez_uac_meetings`
--

CREATE TABLE `fsez_uac_meetings` (
  `uac_id` int(11) NOT NULL,
  `uac_meeting_title` varchar(255) NOT NULL,
  `uac_meeting_file` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `added_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fsez_uac_meetings`
--

INSERT INTO `fsez_uac_meetings` (`uac_id`, `uac_meeting_title`, `uac_meeting_file`, `status`, `added_on`) VALUES
(1, 'Test One', '1582964046sample.pdf', 1, '2020-02-29 13:12:14');

-- --------------------------------------------------------

--
-- Table structure for table `fsez_vacancies`
--

CREATE TABLE `fsez_vacancies` (
  `vacancy_id` int(11) NOT NULL,
  `vacancy_name` mediumtext NOT NULL,
  `vacancy_file_name` mediumtext NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `added_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fsez_vacancies`
--

INSERT INTO `fsez_vacancies` (`vacancy_id`, `vacancy_name`, `vacancy_file_name`, `status`, `added_on`) VALUES
(1, 'Test Vacancy one', '1582144045sample.pdf', 1, '2020-02-20 01:35:59');

-- --------------------------------------------------------

--
-- Table structure for table `global_settings`
--

CREATE TABLE `global_settings` (
  `setting_id` int(11) NOT NULL,
  `org_name` varchar(255) NOT NULL,
  `org_info` varchar(255) NOT NULL,
  `site_url` varchar(255) NOT NULL,
  `address` varchar(200) NOT NULL,
  `contact_no` varchar(50) NOT NULL,
  `company_logo` varchar(255) NOT NULL,
  `fb_link` varchar(120) NOT NULL,
  `inst_link` varchar(150) NOT NULL,
  `in_link` varchar(150) NOT NULL,
  `admin_email` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `global_settings`
--

INSERT INTO `global_settings` (`setting_id`, `org_name`, `org_info`, `site_url`, `address`, `contact_no`, `company_logo`, `fb_link`, `inst_link`, `in_link`, `admin_email`) VALUES
(1, 'Scoreboard Technology', 'Scoreboard Technology is set-up with a vision of providing quality training in programs for students who wish to pursue their education overseas. Our education consultants work with more than 350 Universities across 15 different countries.', 'http://scoreboardtechnology.com', '756, Thevar Illam, New Housing Unit Main Road, Opposite New Bus Stand Thanjavur - 613005', '91 936 067 9797', '1568571505logo.png', 'https://www.facebook.com/scoreboard', 'https://www.instagram.com/scoreboard', 'https://www.linkedin.com/scoreboard', 'scoreboardtechltd@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_user`
--
ALTER TABLE `admin_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fsez_auth_meetings`
--
ALTER TABLE `fsez_auth_meetings`
  ADD PRIMARY KEY (`auth_id`);

--
-- Indexes for table `fsez_circulars`
--
ALTER TABLE `fsez_circulars`
  ADD PRIMARY KEY (`circular_id`);

--
-- Indexes for table `fsez_forms`
--
ALTER TABLE `fsez_forms`
  ADD PRIMARY KEY (`form_id`);

--
-- Indexes for table `fsez_gallery_categories`
--
ALTER TABLE `fsez_gallery_categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `fsez_gallery_images`
--
ALTER TABLE `fsez_gallery_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `fsez_news`
--
ALTER TABLE `fsez_news`
  ADD PRIMARY KEY (`news_id`);

--
-- Indexes for table `fsez_rti`
--
ALTER TABLE `fsez_rti`
  ADD PRIMARY KEY (`rti_id`);

--
-- Indexes for table `fsez_tenders`
--
ALTER TABLE `fsez_tenders`
  ADD PRIMARY KEY (`tender_id`);

--
-- Indexes for table `fsez_uac_meetings`
--
ALTER TABLE `fsez_uac_meetings`
  ADD PRIMARY KEY (`uac_id`);

--
-- Indexes for table `fsez_vacancies`
--
ALTER TABLE `fsez_vacancies`
  ADD PRIMARY KEY (`vacancy_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fsez_auth_meetings`
--
ALTER TABLE `fsez_auth_meetings`
  MODIFY `auth_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fsez_circulars`
--
ALTER TABLE `fsez_circulars`
  MODIFY `circular_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `fsez_forms`
--
ALTER TABLE `fsez_forms`
  MODIFY `form_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `fsez_gallery_categories`
--
ALTER TABLE `fsez_gallery_categories`
  MODIFY `category_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `fsez_gallery_images`
--
ALTER TABLE `fsez_gallery_images`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `fsez_news`
--
ALTER TABLE `fsez_news`
  MODIFY `news_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fsez_rti`
--
ALTER TABLE `fsez_rti`
  MODIFY `rti_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fsez_tenders`
--
ALTER TABLE `fsez_tenders`
  MODIFY `tender_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `fsez_uac_meetings`
--
ALTER TABLE `fsez_uac_meetings`
  MODIFY `uac_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fsez_vacancies`
--
ALTER TABLE `fsez_vacancies`
  MODIFY `vacancy_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `fsez_gallery_images`
--
ALTER TABLE `fsez_gallery_images`
  ADD CONSTRAINT `category` FOREIGN KEY (`category_id`) REFERENCES `fsez_gallery_categories` (`category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
